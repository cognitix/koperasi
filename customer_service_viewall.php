<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "customer_service_viewall";

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			<section class="style-default-bright">
				<div class="section-header">
					<h2 class="text-primary">Nasabah Data</h2>
				</div>
				<div class="section-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table id="datatable1" class="table table-striped table-hover">
									<thead>
										<tr>
											<th class="sort-numeric">Nasabah Id</th>
											<th>Jenis Nasabah</th>
											<th class="sort-alpha">Nama Nasabah</th>
											<th >Kode Cab</th>
											<th >Tempat & Tanggal Lahir</th>
											<th >Jenis / No.Id</th>
											<th >No.NPWP</th>
											<th >No.Passport</th>
											<th >Status Martial</th>
											<th >Ibu Kandung</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sqlNasabah = "SELECT 
														nasabah.* ,
														kodegroup3_nasabah.DESKRIPSI_GROUP3 as jenis_nasabah_name ,
														jenis_identitas.nama_identitas as jenis_id_name
														FROM nasabah 
														LEFT JOIN kodegroup3_nasabah ON 
															kodegroup3_nasabah.NASABAH_GROUP3 = nasabah.jenis_nasabah
														LEFT JOIN jenis_identitas ON 
															jenis_identitas.jenis_id = nasabah.jenis_id
														WHERE 1 ORDER BY nasabah_id ASC";
										$queryNasabah = mysql_query($sqlNasabah);
										while($rowNasabah = mysql_fetch_array($queryNasabah))
										{
										?>
										<tr>
											<td><a href="customer_service_edit.php?nasabah_id=<?php echo $rowNasabah['nasabah_id']; ?>" class="btn ink-reaction btn-flat btn-xs btn-primary"><?php echo $rowNasabah['nasabah_id']; ?></a></td>
											<td><?php echo $rowNasabah['jenis_nasabah_name']; ?></td>
											<td><?php echo $rowNasabah['nama_nasabah']; ?></td>
											<td><?php echo $rowNasabah['CAB']; ?></td>
											<td><?php echo $rowNasabah['tempatlahir']; ?> <?php echo (!empty($rowNasabah['tgllahir']) && $rowNasabah['tgllahir'] != "0000-00-00") ? " , " . date("d/m/Y", strtotime($rowNasabah['tgllahir'])) : ""; ?></td>
											<td><?php echo $rowNasabah['jenis_id_name']; ?> <?php echo (!empty($rowNasabah['no_id'])) ? ", " . $rowNasabah['no_id'] : ""; ?></td>
											<td><?php echo $rowNasabah['npwp']; ?></td>
											<td><?php echo $rowNasabah['NO_PASSPORT']; ?></td>
											<td><?php echo $rowNasabah['Status_Marital']; ?></td>
											<td><?php echo $rowNasabah['IBU_KANDUNG']; ?></td>
										</tr>
										<?php 
										}
										?>
									</tbody>
								</table>
							</div>
						</div>	
					</div>	
				</div>
			</section>
		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>	

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>