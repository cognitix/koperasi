<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$jmls = $_SESSION['jmls'];
$totals = $_SESSION['totals'];

$jml_debet = $_SESSION['jml_debet'];
$jml_kredit = $_SESSION['jml_kredit'];

$saldo_awal = $_SESSION['saldo_awal'];
$saldo_akhir = $_SESSION['saldo_akhir'];

$tgl_transaksi = date("d M Y", strtotime($_SESSION['tgl_transaksi']));
$tgl_transaksi_kemarin = date("d M Y", strtotime($_SESSION['tgl_transaksi'] . "-1 day"));
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
    font-size:11px;
    line-height: 1.2;
    width:700px;
}
.table-custom tbody tr td
{
    border:none;
    padding:4px;
    line-height: 1.5;
}
.table-custom2 tbody tr td
{
    border:none;
    padding:0 2px;
    line-height: 1.5;
}
.table-custom thead tr th{
    text-align: center;
    font-weight: bold;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
    padding:4px;
    line-height: 1.5;
    border:1px solid #000;
    border-top:1px solid #000 !important;
}
.table-custom tr th{
    text-align: center;
    border-top:1px solid #000 !important;
    border-bottom:1px solid #000 !important;
    padding:2px !important;
}
.table-custom-border tr td{
    line-height: 1.5;
    padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
    padding:10px !important;
}
.header-text h5, .header-text h3{
    margin-top:0;
    margin-bottom:0;
}
</style>

<div class="card body-print">
    <div class="card-body">

        <div class="header-text">
            <div class="pull-left">
                <h5>Koperasi Simpan Pinjam</h5>
                <h3 style="margin-bottom:8px;">KSP Adil Makmur Fajar</h3>
            </div>
            <div class="pull-right">
                <h5>Berita Acara Kas Teller</h5>
                <h5 style="margin-bottom:8px;"><?php echo $tgl_transaksi; ?></h5>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table">
            <tr>
                <td>
                    <div>Saldo Kas Per Tanggal</div>
                    <div>Jumlah Pendapatan</div>
                    <div>Jumlah Pengeluaran</div>
                    <div>Saldo Kas Per Tanggal</div>
                </td>
                <td>
                    <div><?php echo $tgl_transaksi_kemarin; ?></div>
                    <div><br /></div>
                    <div><br /></div>
                    <div><?php echo $tgl_transaksi; ?></div>
                </td>
                <td>
                    <div><?php echo number_format($saldo_awal,0,'.',','); ?></div>
                    <div><?php echo number_format($jml_debet,0,'.',','); ?></div>
                    <div><?php echo number_format($jml_kredit,0,'.',','); ?></div>
                    <div><?php echo number_format($saldo_akhir,0,'.',','); ?></div>
                </td>
            </tr>
        </table>

        <div class="clearfix"></div>

        <div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover table-custom">
                
                <?php 
                $sqlDominasi = "SELECT * FROM kodednominasi WHERE 1 ORDER BY Jenis_D_Nominasi ASC";
                $queryDominasi = mysql_query($sqlDominasi);
                $i = $total = 0;
                $subTotal = $subJml = 0;

                $headTitle = "";

                while($result = mysql_fetch_array($queryDominasi))
                {
                    $total += $totals[$i];

                    if ($headTitle != $result['Jenis_D_Nominasi'])
                    {

                        if ($headTitle != "")
                        {
                ?>

                <tr>
                    <th style="text-align:left;">Sub Total</th>
                    <th style="text-align:left;"></th>
                    <th class="text-right" style="text-align:right;"><?php echo number_format($subJml,0,'.',','); ?></th>
                    <th class="text-right" style="text-align:right;"><?php echo number_format($subTotal,0,'.',','); ?></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <?php
                        $subJml = $subTotal = 0;

                        }

                        $headTitle = $result['Jenis_D_Nominasi'];
                ?>

                <tr>
                    <td>Uang:</td>
                    <td><?php echo $headTitle; ?></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <th>Kode</th>
                    <th>Nilai Mata Uang</th>
                    <th class="text-right">Jumlah</th>
                    <th class="text-right">Total</th>
                </tr>

                <?php 
                    }
                ?>

                <tr>
                    <td><?php echo $result['Kode_D_Nominasi']; ?></td>
                    <td id="nilai" data-nilai="<?php echo $result['Nilai_D_Nominasi']; ?>"><?php echo $result['Nilai_D_Nominasi']; ?></td>
                    <td class="text-right"><?php echo number_format($jmls[$i],0,'.',','); ?></td>
                    <td class="text-right"><?php echo number_format($totals[$i],0,'.',','); ?></td>
                </tr>

                <?php
                    $subJml += $jmls[$i];
                    $subTotal += $totals[$i];

                    $i++;
                }

                if ($headTitle != "")
                {
                ?>

                <tr>
                    <th style="text-align:left;">Sub Total</th>
                    <th style="text-align:left;"></th>
                    <th class="text-right" style="text-align:right;"><?php echo number_format($subJml,0,'.',','); ?></th>
                    <th class="text-right" style="text-align:right;"><?php echo number_format($subTotal,0,'.',','); ?></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <?php
                }
                ?>

                <tr>
                    <th style="text-align:left;">Total</th>
                    <th style="text-align:left;"></th>
                    <th class="text-right" style="text-align:right;"></th>
                    <th class="text-right" style="text-align:right;"><?php echo number_format($total,0,'.',','); ?></th>
                </tr>

                <tr>
                    <th style="text-align:left;">Terbilang:</th>
                    <th style="text-align:left;" colspan="3">##<?php echo Terbilang($total); ?></th>
                </tr>

            </table>

            <p>Pada hari ini <?php echo date("d M Y"); ?>. Tanggal <?php echo $tgl_transaksi; ?>, tidak ada transaksi yang mencurigakan tidak ada transaksi sama dengan dan diatas Rp. 500.000.000,- ( Lima Ratus Juta Rupiah )</p>

            <div class="clearfix"></div>

            <div style="margin:0 auto;">
                <div class="pull-left" style="width:15%;margin-left:50px;">
                    <table id="datatable1" class="table table-custom">
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="text-align:center;">Mengetahui,<br />Pemeriksa</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                    <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                        Staff ............
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pull-right" style="width:15%;margin-right:50px;">
                    <table id="datatable1" class="table table-custom">
                        <tbody>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="text-align:center;">Teller</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                    <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                        ............
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>

    </div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>


<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>