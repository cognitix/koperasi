<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl_transaksi_from = $_GET['tgl_transaksi_from'];
$tgl_transaksi_to = $_GET['tgl_transaksi_to'];
$kode_perkiraan = $_GET['kode_perkiraan'];
$nama_perkiraan = $_GET['nama_perkiraan'];

$tgl_transaksi_from_string = date("d M Y", strtotime($tgl_transaksi_from));
$tgl_transaksi_to_string = date("d M Y", strtotime($tgl_transaksi_to));

// Perkiraan ======================================

$sqlPerkiraan = "
SELECT * FROM perkiraan WHERE 1 AND kode_perk = '".$kode_perkiraan."'
";
$queryPerkiraan = mysql_query($sqlPerkiraan);
$fetchPerkiraan = mysql_fetch_array($queryPerkiraan);

// =================================================

// Saldo Awal ======================================

$sqlBefore = "
SELECT 
*
FROM trans_detail
JOIN trans_master ON trans_master.trans_id = trans_detail.master_id
WHERE 1
AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl_transaksi_from))."'
AND trans_detail.kode_perk = '".$kode_perkiraan."'
ORDER BY trans_master.tgl_trans DESC
";
$queryBefore = mysql_query($sqlBefore);

$saldoAwal = 0;

while($resultBefore = mysql_fetch_array($queryBefore))
{

	$kredit = $resultBefore['kredit'];

	$debet = $resultBefore['debet'];

	$saldoAwal += $debet - $kredit;

}

// =================================================

$sqlTransDetail = "
SELECT 
*
FROM trans_detail
JOIN trans_master ON trans_master.trans_id = trans_detail.master_id
WHERE 1
AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl_transaksi_from))."' AND '".date('Y-m-d', strtotime($tgl_transaksi_to))."'
AND trans_detail.kode_perk = '".$kode_perkiraan."'
ORDER BY trans_master.tgl_trans DESC
";

$queryTransDetail = mysql_query($sqlTransDetail);

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:700px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
.header-text h5, .header-text h3, .header-text h4{
    margin-top:0;
    margin-bottom:0;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div class="header-text">
			<div class="pull-left">
				<h5>Koperasi Simpan Pinjam</h5>
                <h4 style="margin-bottom:8px;">KSP ADIL MAKMUR FAJAR</h4>
			</div>
			<div class="pull-right">
				<h4>Buku Besar</h4>
                <h5 style="margin-bottom:8px;">Periode from <?php echo $tgl_transaksi_from_string; ?> to <?php echo $tgl_transaksi_to_string; ?></h5>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover table-custom">
            	<tr>
                    <th style="text-align:left;">Tanggal</th>
                    <th style="text-align:left;">Keterangan</th>
                    <th class="text-right" style="text-align:right;">Debet</th>
                    <th class="text-right" style="text-align:right;">Kredit</th>
                    <th class="text-right" style="text-align:right;">Saldo</th>
                </tr>
                <tr>
                	<td><b><?php echo $fetchPerkiraan['kode_perk']; ?></b></td>
                	<td><b><?php echo trim($fetchPerkiraan['nama_perk']); ?></b></td>
                	<td></td>
                	<td class="text-right">Saldo Awal</td>
                	<td class="text-right"><?php echo number_format($saldoAwal,2,'.',','); ?></td>
                </tr>

                <?php 
                $saldo = $saldoAwal;
                $totalDebet = $totalKredit = 0;
                while($results = mysql_fetch_array($queryTransDetail))
				{
					$debet = $results['debet'];
					$kredit = $results['kredit'];
					$saldo += $debet - $kredit;

					$totalDebet += $debet;
					$totalKredit += $kredit;
				?>

				<tr>
					<td><?php echo date("d/m/Y", strtotime($results['tgl_trans'])); ?></td>
					<td>Total Mutasi</td>
					<td class="text-right"><?php echo number_format($debet,2,'.',','); ?></td>
					<td class="text-right"><?php echo number_format($kredit,2,'.',','); ?></td>
					<td class="text-right"><?php echo number_format($saldo,2,'.',','); ?></td>
				</tr>

				<?php
				}
                ?>

                <tr>
                	<th></th>
                	<th><b>Total Jumlah <?php echo trim($fetchPerkiraan['nama_perk']); ?>:</b></th>
                	<th class="text-right" style="text-align:right;"><?php echo number_format($totalDebet,2,'.',','); ?></th>
                	<th class="text-right" style="text-align:right;"><?php echo number_format($totalKredit,2,'.',','); ?></th>
                	<th class="text-right" style="text-align:right;"><?php echo number_format($saldo,2,'.',','); ?></th>
                </tr>

            </table>
        </div>

        <div class="clearfix"></div>

		<div style="margin:0 auto;">
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Menyetujui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:98px;text-align:center;">Agustina</div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    KBO
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Mengetahui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:98px;text-align:center;">Lily Njomin</div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Pengawas
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">
									<b>Jakarta Utara, <?php echo date("d M Y"); ?></b><br />
									KSP ADIL MAKMUR FAJAR<br />
									Dibuat
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Staff Akutansi
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>

	</div>
</div>	

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>