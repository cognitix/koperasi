<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "buku_besar_pembantu";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			
			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Buku Besar Pembantu</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="get" action="print_buku_besar_pembantu.php" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi_from" name="tgl_transaksi_from" value="" required>
														<label for="tgl_transaksi_from">Tanggal Transaksi From</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi_to" name="tgl_transaksi_to" value="" required>
														<label for="tgl_transaksi_to">Tanggal Transaksi To</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="kode_perkiraan" name="kode_perkiraan" data-source="autosuggest_gltrans.php">
												<label for="kode_perkiraan">Kode Perkiraan</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="nama_perkiraan" name="nama_perkiraan">
												<label for="nama_perkiraan">Nama Perkiraan</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

					</form>

				</div>	
			</section>

			<?php require_once "layouts/home/menus.php"; ?>

		</div>

	</div>
	
<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tgl_transaksi_from, #tgl_transaksi_to').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#kode_perkiraan').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#kode_perkiraan").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#kode_perkiraan').val(ui.item.kode_perk);
					$('#nama_perkiraan').val(ui.item.nama_perk);
					
					return false;
				},
				focus: function( event, ui ) {

					$('#kode_perkiraan').val(ui.item.kode_perk);
			        $('#nama_perkiraan').val(ui.item.nama_perk);
			        
			        return false;
			    },
			});
		}
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>		