<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl_transaksi1 = $_GET['tgl_transaksi1'];
$tgl_transaksi2 = $_GET['tgl_transaksi2'];
$userid = $_GET['userid'];

$sqlUser = "SELECT * FROM passwd WHERE 1 AND USERID = '".$userid."'";
$fetchUser = mysql_fetch_array(mysql_query($sqlUser));

$saldoAkhir = 0;

$tgl_transaksi1 = date('Y-m-d', strtotime($tgl_transaksi1));
$tgl_transaksi2 = date('Y-m-d', strtotime($tgl_transaksi2));

$sqlBefore = "
SELECT 
*
FROM tellertrans 
WHERE 1
AND tgl_trans < '".$tgl_transaksi1."'
AND userid = '".$userid."'
ORDER BY tgl_trans DESC
";
$queryBefore = mysql_query($sqlBefore);

$saldoAwal = 0;
$jml_obkredit_before = $jml_obdebet_before = $jml_kredit_before = $jml_debet_before = 0;
while($resultBefore = mysql_fetch_array($queryBefore))
{
    if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "300")
    {
        $jml_obkredit_before += $resultBefore['saldo_trans'];
    }
    if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "200")
    {
        $jml_obdebet_before += $resultBefore['saldo_trans'];
    }

    $kredit = 0;
    if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "300")
    {
        $jml_kredit_before += $resultBefore['saldo_trans'];
        //$saldoAwal -= $jml_kredit_before;

        $kredit = $resultBefore['saldo_trans'];
    }

    $debet = 0;
    if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "200")
    {
        $jml_debet_before += $resultBefore['saldo_trans'];
        //$saldoAwal += $jml_kredit_before;

        $debet = $resultBefore['saldo_trans'];
    }

    $saldoAwal += $debet - $kredit;
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
    font-size:11px;
    line-height: 1.2;
    /*width:650px;*/
}
.table-custom tbody tr td
{
    border:none;
    padding:4px;
    line-height: 1.5;
}
.table-custom2 tbody tr td
{
    border:none;
    padding:0 2px;
    line-height: 1.5;
}
.table-custom thead tr th{
    text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
    padding:4px;
    line-height: 1.5;
    border:1px solid #000;
    border-top:1px solid #000 !important;
}
.table-custom tr th{
    text-align: center;
    border-top:1px solid #000 !important;
    border-bottom:1px solid #000 !important;
    padding:2px !important;
}
.table-custom-border tr td{
    line-height: 1.5;
    padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
    padding:10px !important;
}
</style>

<div class="card body-print">
    <div class="card-body">

        <div>
            <div class="pull-left">
                <h4>Laporan Kas Umum</h4>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="table-responsive">
            <table id="datatable1" class="table table-custom">
                
                <tr>
                    <td colspan="4"><b>Tunai - Kas Teller <?php echo $fetchUser['USERNAME']; ?></b></td>
                    <td><b>Saldo Awal</b></td>
                    <td><b><?php echo number_format($saldoAwal,2,'.',','); ?></b></td>
                </tr>

                <tr>
                    <th>Tanggal</th>
                    <th>Jurnal</th>
                    <th>No.Bukti</th>
                    <th width="50%">Keterangan</th>
                    <th>Kredit</th>
                    <th>Debet</th>
                </tr>

                <?php 
                $sql = "
                SELECT 
                *
                FROM tellertrans 
                WHERE 1
                AND tgl_trans BETWEEN '".$tgl_transaksi1."' AND '".$tgl_transaksi2."'
                AND userid = '".$userid."'
                ";

                $query = mysql_query($sql);
                ?>

                <tbody>

                    <?php 
                    $saldo = $saldoAwal;
                    $jml_obkredit = $jml_obdebet = $jml_kredit = $jml_debet = 0;

                    while($result = mysql_fetch_array($query))
                    {
                    ?>

                    <tr>
                        <td><?php echo date("d M Y", strtotime($result['tgl_trans'])); ?></td>
                        <td><?php echo $result['modul']; ?></td>
                        <td><?php echo $result['NO_BUKTI']; ?></td>
                        <td><?php echo $result['uraian']; ?></td>
                        
                        <td>
                        <?php 
                        $saldoTrans = 0;
                        $kredit = 0;
                        if ($result['tob'] == "T" && $result['my_kode_trans'] == "300")
                        {
                            $jml_kredit += $result['saldo_trans'];
                            $saldoTrans = $result['saldo_trans'];

                            $kredit = $result['saldo_trans'];
                        }

                        echo number_format($saldoTrans,2,'.',',');
                        ?>
                        </td>

                        <td>
                        <?php 
                        $saldoTrans = 0;
                        $debet = 0;
                        if ($result['tob'] == "T" && $result['my_kode_trans'] == "200")
                        {
                            $jml_debet += $result['saldo_trans'];
                            $saldoTrans = $result['saldo_trans'];

                            $debet = $result['saldo_trans'];
                        }

                        echo number_format($saldoTrans,2,'.',',');
                        ?>
                        </td>
                    </td>

                    <?php

                        $saldo += $debet - $kredit;

                    }
                    ?>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th class="text-center"><b><?php echo number_format($jml_kredit,2,'.',','); ?></b></th>
                        <th class="text-center"><b><?php echo number_format($jml_debet,2,'.',','); ?></b></th>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-center"><b>Saldo Akhir Kas</b></td>
                        <th class="text-center"><b><?php echo number_format($saldo,2,'.',','); ?></b></th>
                    </tr>

                </tbody>    

            </table>
        </div>

        <div class="clearfix"></div>

        <div style="margin:0 auto;">
            <div class="pull-left" style="width:15%;margin-left:50px;">
                <table id="datatable1" class="table table-custom">
                    <tbody>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-align:center;">Mengetahui</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Kabag
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="pull-right" style="width:15%;margin-right:50px;">
                <table id="datatable1" class="table table-custom">
                    <tbody>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-align:center;">Membuat</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Teller
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>


<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>