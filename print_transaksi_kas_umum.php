<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tabTransId = $_GET['tabTransId'];

$sql = "
SELECT  
tellertrans.* ,
perkiraan.nama_perk
FROM tellertrans 
LEFT JOIN perkiraan ON
perkiraan.kode_perk = tellertrans.GL_TRANS
WHERE 1 
AND trans_id = '".$tabTransId."'";
$fetch = mysql_fetch_array(mysql_query($sql));

//echo '<pre>';print_r($fetch);echo '</pre>';

function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:650px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.2;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.2;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.2;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
</style>

<body class="menubar-hoverable header-fixed menubar-pin ">

<div class="card body-print">
	<div class="card-body">

		<div>
			<div class="pull-left">
				<table id="datatable1" class="table table-custom2">
					<tbody>
						<tr>
							<td>
								<div><b>Koperasi Simpan Pinjam</b></div>
							</td>
						</tr>
						<tr>
							<td>
								<div><b>KSP Adil Makmur Fajar</b></div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				<table id="datatable1" class="table table-custom2">
					<tbody>
						<tr>
							<td>
								<div>Validasi<br />No.Bukti</div>
							</td>
							<td>
								<div>:</div>
							</td>
							<td>
								<div>
									<?php echo $fetch['NO_BUKTI']; ?>
									<br />
									<?php echo $fetch['GL_TRANS']; ?>
									<br />
									<?php echo $fetch['nama_perk']; ?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<table id="datatable1" class="table table-custom">
			<tbody>
				<tr>
					<td colspan="2">
						<div style="text-align:center;font-size:15px;font-weight:bold;">
							<?php echo ($fetch['kode_jurnal'] == "KK") ? "KAS KELUAR" : "KAS MASUK"; ?>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div style="text-align:left;">
							Telah terima dari KSP Adil Makmur Fajar
						</div>
					</td>
				</tr>
				<tr>
					<th>Keterangan</th>
					<th>Nominal (Rp.)</th>
				</tr>
				<tr>
					<td class="text-center"><?php echo $fetch['uraian']; ?></td>
					<td class="text-center"><?php echo number_format($fetch['saldo_trans'],2,'.',','); ?></td>
				</tr>
				<tr>
					<th>Jumlah</th>
					<th><?php echo number_format($fetch['saldo_trans'],2,'.',','); ?></th>
				</tr>
				<tr>
					<td colspan="2">Terbilang # <?php echo ucwords(Terbilang($fetch['saldo_trans'])); ?></td>
				</tr>
			</tbody>
		</table>

		<div>
			<div class="pull-left">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;"></div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:50px;text-align:center;">
									Penerima
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-right" style="margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
								Jakarta, <?php echo date("d M Y"); ?>
								<?php /*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*/ ?>
							</td>
						</tr>
						<tr>
							<td>
								
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:50px;text-align:center;">
									Teller
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>



<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>


