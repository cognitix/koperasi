<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "laporan_transaksi_kredit";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi1 = (!empty($_GET['tgl_transaksi1'])) ? $_GET['tgl_transaksi1'] : date('Y-m-d');
$tgl_transaksi2 = (!empty($_GET['tgl_transaksi2'])) ? $_GET['tgl_transaksi2'] : date('Y-m-d', strtotime("+7 day"));
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Laporan Transaksi Kredit</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi1" name="tgl_transaksi1" value="<?php echo $tgl_transaksi1; ?>" required>
														<label for="tgl_transaksi1">Tanggal Transaksi Dari</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi2" name="tgl_transaksi2" value="<?php echo $tgl_transaksi2; ?>" required>
														<label for="tgl_transaksi2">Tanggal Transaksi Ke</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group">
												<select id="kode_cab" name="kode_cab" class="form-control">
													<option value="">Cari Semua Cabang</option>

													<?php 
													$sqlKodeCabang = mysql_query("SELECT * FROM kodecabang WHERE 1 ORDER BY kode_cab ASC");
													while($rowKodeCabang = mysql_fetch_array($sqlKodeCabang))
													{
													?>
													<option value="<?php echo $rowKodeCabang['kode_cab']; ?>"><?php echo $rowKodeCabang['kode_cab'] .' - '. $rowKodeCabang['nama_cab']; ?></option>
													<?php
													}
													?>

												</select>
												<label for="kode_cab">Kode Cabang</label>
											</div>


									</div>
								</div>

							</div>


						</div>
           				
           				<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Search</button>

					</form>
					
					<hr />

					<?php 
					$saldoAkhir = 0;

					if (!empty($_GET['tgl_transaksi1']) && !empty($_GET['tgl_transaksi2']))
					{

						$kode_cab = (!empty($_GET['kode_cab'])) ? $_GET['kode_cab'] : "";
					?>

					<section class="style-default-bright">

						<div class="section-header">
							<ol class="breadcrumb">
								<li class="active">Laporan Transaksi Kredit</li>
							</ol>
						</div>

						<div class="row">
							<div class="col-lg-12">
							
								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										
										<tr>
											<th>No</th>
											<th>Cabang</th>
								            <th>Nama Nasabah</th>
								            <th>No Passport</th>
								            <th>No Rekening</th>
								            <th>Tgl Transaksi</th>
								            <th>Pokok Trans</th>
										</tr>

										<?php 
										$sql = "
										SELECT  
										kredit.JML_PINJAMAN ,
										nasabah.nama_nasabah ,
										nasabah.NO_PASSPORT ,
										kredit.TGL_REALISASI,
										kredit.NO_REKENING,
										kodecabang.kode_cab,
										kodecabang.nama_cab
										FROM kredit 
										JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id 
										JOIN kodecabang ON kodecabang.kode_cab = kredit.CAB
										WHERE 1 
										AND TGL_REALISASI BETWEEN '".$tgl_transaksi1."' AND '".$tgl_transaksi2."'";

										if ($kode_cab)
										{
											$sql .= "AND kodecabang.kode_cab = '".$kode_cab."'";
										}

										$query = mysql_query($sql);

										$no = 1;
										$totalPinjaman = 0;

										while($result = mysql_fetch_array($query))
										{
											$totalPinjaman += $result['JML_PINJAMAN'];
										?>

										<tr>
											<td><?php echo $no++; ?></td>
											<td><?php echo $result['kode_cab'].' ('.$result['nama_cab'].')'; ?></td>
											<td><?php echo $result['nama_nasabah']; ?></td>
											<td><?php echo $result['NO_PASSPORT']; ?></td>
											<td><?php echo $result['NO_REKENING']; ?></td>
											<td><?php echo date("d/M/Y", strtotime($result['TGL_REALISASI'])); ?></td>
											<td><?php echo number_format($result['JML_PINJAMAN'],0,'',','); ?></td>
										</tr>

										<?php
										}
										?>

										<tr>
											<td colspan="6" align="right"><b>Total</b></td>
											<td><b><?php echo number_format($totalPinjaman,0,"",","); ?></b></td>
										</tr>

									</table>
								</div>

							</div>
						</div>

						<a href="print_laporan_transaksi_kredit.php?tgl_transaksi1=<?php echo $tgl_transaksi1; ?>&tgl_transaksi2=<?php echo $tgl_transaksi2; ?>&kode_cab=<?php echo $kode_cab; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print</a>

					</section>

					<?php
					}
					?>
	
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tgl_transaksi1, #tgl_transaksi2').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#userid').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#userid").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$("#userid").val(ui.item.USERID);

					return false;
				},
				focus: function( event, ui ) {

			        $("#userid").val(ui.item.USERID);
			        
			        return false;
			    },
			});
		}
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>