<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

define ("MAX_SIZE", "5000");

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "customer_service_entrydata";
$nasabah_id = $_GET['nasabah_id'];

function getExtension($str) 
{
	$i = strrpos($str,".");
	if (!$i) { return ""; } 
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}


function upload($field_name, $folder_name)
{
	$filenamePhoto = "";
	$errors = 0;

	if (!empty($_FILES[$field_name]["name"]))
	{
		$photo 			= $_FILES[$field_name]["name"];
		$uploadedFile 	= $_FILES[$field_name]['tmp_name'];

	  	$filename = stripslashes($photo);
	   	$extension = getExtension($filename);
	  	$extension = strtolower($extension);

	  	if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
	  	{
			echo ' Unknown Image extension ';
			$errors = 1;
	  	}
	  	else
		{
	   		$size = filesize($uploadedFile);
	   		if ($size > MAX_SIZE * 1024)
			{
				echo "You have exceeded the size limit";
			 	$errors = 1;
			}
	   	}

	   	if ($errors == 0)
	   	{
	   		if($extension == "jpg" || $extension == "jpeg")
			{
				$src = imagecreatefromjpeg($uploadedFile);
			}
			else if($extension=="png")
			{
				$src = imagecreatefrompng($uploadedFile);
			}
			else 
			{
				$src = imagecreatefromgif($uploadedFile);
			}

			list($width, $height) = getimagesize($uploadedFile);

			$newwidth = $width;
			$newheight = $height;

			if ($width >= 500)
			{
				$newwidth = 500;
				$newheight = ($height / $width) * $newwidth;
			}
			
			$tmp = imagecreatetruecolor($newwidth, $newheight);

			imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

			$filenamePhoto = "uploads/".$folder_name."/". sha1($photo . time()) . '.png';

			imagejpeg($tmp,$filenamePhoto,100);

			imagedestroy($src);
			imagedestroy($tmp);
	   	}
	}

	return array('filenamePhoto' => $filenamePhoto, 'errors' => $errors);
}

$message = 0;

if (isset($_POST['submit']))
{
	$NO_DIN = $_POST['NO_DIN'];
	//$nasabah_id = $_POST['nasabah_id'];
	$nama_nasabah = $_POST['nama_nasabah'];
	$ALAMAT_DOMISILI = $_POST['ALAMAT_DOMISILI'];
	$tempatlahir = $_POST['tempatlahir'];
	$tgllahir = $_POST['tgllahir'];
	$jenis_nasabah = $_POST['jenis_nasabah'];
	$TGL_BUKA = $_POST['TGL_BUKA'];
	$nama_alias = $_POST['nama_alias'];
	$jenis_kelamin = $_POST['jenis_kelamin'];
	$gelar_id = $_POST['gelar_id'];
	$jenis_id = $_POST['jenis_id'];
	$tglid = $_POST['tglid'];
	$kode_area = $_POST['kode_area'];
	$IBU_KANDUNG = $_POST['IBU_KANDUNG'];
	$alamat = $_POST['alamat'];
	$KET_GELAR = $_POST['KET_GELAR'];
	$no_id = $_POST['no_id'];
	$NO_NIP = $_POST['NO_NIP'];
	$npwp = $_POST['npwp'];
	$Status_Marital = $_POST['Status_Marital'];
	$kelurahan = $_POST['kelurahan'];
	$kecamatan = $_POST['kecamatan'];
	$KOTA = $_POST['KOTA'];
	$pekerjaan_id = $_POST['pekerjaan_id'];
	$TUJUAN_PEMBUKAAN_KYC = $_POST['TUJUAN_PEMBUKAAN_KYC'];
	$PENDAPATAN_KYC = $_POST['PENDAPATAN_KYC'];
	$Kode_Negara = $_POST['Kode_Negara'];
	$pekerjaan = $_POST['pekerjaan'];
	$SUMBER_DANA_KYC = $_POST['SUMBER_DANA_KYC'];
	$NO_HP = $_POST['NO_HP'];
	$NO_PASSPORT = $_POST['NO_PASSPORT'];
	$TGL_MULAI_PASSPORT = $_POST['TGL_MULAI_PASSPORT'];
	$TGL_AKHIR_PASSPORT = $_POST['TGL_AKHIR_PASSPORT'];
	$CAB = $_POST['CAB'];
	$telpon = $_POST['telpon'];

	$upload_PATH_FOTO = upload("PATH_FOTO", "photo");
	$upload_PATH_TTANGAN = upload("PATH_TTANGAN", "tandatangan");
	
	if ($upload_PATH_FOTO['errors'] == 0 || $upload_PATH_TTANGAN['errors'] == 0)
	{
		$filename_PATH_FOTO = $upload_PATH_FOTO['filenamePhoto'];
		$filename_PATH_TTANGAN = $upload_PATH_TTANGAN['filenamePhoto'];

		$sql = "UPDATE nasabah SET 
				NO_DIN = '$NO_DIN' ,
				nama_nasabah = '$nama_nasabah' ,
				ALAMAT_DOMISILI = '$ALAMAT_DOMISILI' ,
				tempatlahir = '$tempatlahir' ,
				tgllahir = '$tgllahir' ,
				jenis_nasabah = '$jenis_nasabah' ,
				TGL_BUKA = '$TGL_BUKA' ,
				nama_alias = '$nama_alias' ,
				jenis_kelamin = '$jenis_kelamin' ,
				gelar_id = '$gelar_id' ,
				jenis_id = '$jenis_id' ,
				tglid = '$tglid' ,
				kode_area = '$kode_area' ,
				IBU_KANDUNG = '$IBU_KANDUNG' ,
				alamat = '$alamat' ,
				KET_GELAR = '$KET_GELAR' ,
				no_id = '$no_id' ,
				NO_NIP = '$NO_NIP' ,
				npwp = '$npwp' ,
				Status_Marital = '$Status_Marital' ,
				kelurahan = '$kelurahan' ,
				kecamatan = '$kecamatan' ,
				KOTA = '$KOTA' ,
				pekerjaan_id = '$pekerjaan_id' ,
				TUJUAN_PEMBUKAAN_KYC = '$TUJUAN_PEMBUKAAN_KYC' ,
				PENDAPATAN_KYC = '$PENDAPATAN_KYC' ,
				Kode_Negara = '$Kode_Negara' ,
				pekerjaan = '$pekerjaan' ,
				SUMBER_DANA_KYC = '$SUMBER_DANA_KYC' ,
				NO_HP = '$NO_HP' ,
				NO_PASSPORT = '$NO_PASSPORT' ,
				TGL_MULAI_PASSPORT = '$TGL_MULAI_PASSPORT' ,
				TGL_AKHIR_PASSPORT = '$TGL_AKHIR_PASSPORT' ,
				CAB = '$CAB' ,
				telpon = '$telpon'
				WHERE 1
				AND nasabah_id = '$nasabah_id'
				";

		//echo $sql;

		$return = mysql_query($sql);

		if (!empty($filename_PATH_FOTO))
		{
			$sql = "UPDATE nasabah SET PATH_FOTO = '$filename_PATH_FOTO' WHERE 1 AND nasabah_id = '$nasabah_id'";
			$return = mysql_query($sql);
		}

		if (!empty($filename_PATH_TTANGAN))
		{
			$sql = "UPDATE nasabah SET PATH_TTANGAN = '$filename_PATH_TTANGAN' WHERE 1 AND nasabah_id = '$nasabah_id'";
			$return = mysql_query($sql);
		}

		//print_r($return);

		$message = 1;
	}
	else
	{
		$message = 2;
	}

}

$sqlCustomerService = "SELECT * FROM nasabah WHERE 1 AND nasabah_id = '$nasabah_id'";
$fetchCustomerService = mysql_fetch_array(mysql_query($sqlCustomerService));

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Nasabah Edit Data</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form form-validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">
										
											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_DIN" name="NO_DIN" value="<?php echo $fetchCustomerService['NO_DIN']; ?>">
												<label for="NO_DIN">D.I.N - BI</label>
											</div>

											<div class="form-group floating-label" style="display:none;">
												<input type="hidden" class="form-control" id="nasabah_id" name="nasabah_id" value="<?php echo $nasabah_id; ?>" required>
												<label for="nasabah_id">Nasabah ID</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" value="<?php echo $fetchCustomerService['nama_nasabah']; ?>" required>
												<label for="nama_nasabah">Nama Nasabah</label>
											</div>

											<div class="form-group floating-label">
												<textarea name="ALAMAT_DOMISILI" id="ALAMAT_DOMISILI" class="form-control" rows="3" placeholder="" required><?php echo $fetchCustomerService['ALAMAT_DOMISILI']; ?></textarea>
												<label for="ALAMAT_DOMISILI">Alamat Domisili</label>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tempatlahir" name="tempatlahir" value="<?php echo $fetchCustomerService['tempatlahir']; ?>" required>
														<label for="tempatlahir">Tempat Lahir</label>
													</div>	
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_lahir_container" name="tgllahir" value="<?php echo (!empty($fetchCustomerService['tgllahir']) && $fetchCustomerService['tgllahir'] != "0000-00-00") ? date("Y-m-d", strtotime($fetchCustomerService['tgllahir'])) : ""; ?>" required>
														<label for="tgllahir">Tanggal Lahir</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											
									</div>
								</div>

							</div>

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">
										
											<div class="form-group floating-label">
												<select id="jenis_nasabah" name="jenis_nasabah" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlJenisNasabah = "SELECT * FROM kodegroup3_nasabah WHERE 1";
													$fetchJenisNasabah = mysql_query($sqlJenisNasabah);
													while($rowJenisNasabah = mysql_fetch_array($fetchJenisNasabah))
													{
														$selected = ($fetchCustomerService['jenis_nasabah'] == $rowJenisNasabah['NASABAH_GROUP3']) ? 'selected' : "";
													?>
													<option value="<?php echo $rowJenisNasabah['NASABAH_GROUP3']; ?>" <?php echo $selected; ?>><?php echo $rowJenisNasabah['DESKRIPSI_GROUP3']; ?></option>
													<?php
													}
													?>
												</select>
												<label for="jenis_nasabah">Jenis Nasabah</label>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="tgl_buka_container">
													<div class="input-group-content">
														<?php 
														$TGL_BUKA = $fetchCustomerService['TGL_BUKA'];
														?>
														<input type="text" class="form-control" id="TGL_BUKA" name="TGL_BUKA" value="<?php echo $TGL_BUKA; ?>" required>
														<label for="TGL_BUKA">Open Date</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nama_alias" name="nama_alias" value="<?php echo $fetchCustomerService['nama_alias']; ?>">
												<label for="nama_alias">Nama Alias</label>
											</div>

											<div class="form-group floating-label">
												<select id="jenis_kelamin" name="jenis_kelamin" class="form-control" required>
													<option value="">&nbsp;</option>
													<option value="L" <?php echo ($fetchCustomerService['jenis_kelamin'] == "L") ? 'selected' : ''; ?>>Male</option>
													<option value="P" <?php echo ($fetchCustomerService['jenis_kelamin'] == "P") ? 'selected' : ''; ?>>Female</option>
												</select>
												<label for="jenis_kelamin">Gender</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="CAB" name="CAB" value="<?php echo $fetchCustomerService['CAB']; ?>" readonly required>
												<label for="CAB">Kode Cabang</label>
											</div>
										
									</div>
								</div>

							</div>

						</div>


						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<select id="gelar_id" name="gelar_id" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlGelar = "SELECT * FROM jenis_gelar WHERE 1";
													$fetchGelar = mysql_query($sqlGelar);
													while($rowGelar = mysql_fetch_array($fetchGelar))
													{
														$selected = ($fetchCustomerService['gelar_id'] == $rowGelar['Gelar_ID']) ? 'selected' : "";
													?>
													<option value="<?php echo $rowGelar['Gelar_ID']; ?>" <?php echo $selected; ?>><?php echo $rowGelar['Gelar_ID'] . ' - ' . $rowGelar['Deskripsi_Gelar']; ?></option>
													<?php
													}
													?>
												</select>
												<label for="gelar_id">Gelar</label>
											</div>

											<div class="form-group floating-label">
												<select id="jenis_id" name="jenis_id" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlGelar = "SELECT * FROM jenis_identitas WHERE 1";
													$fetchGelar = mysql_query($sqlGelar);
													while($rowGelar = mysql_fetch_array($fetchGelar))
													{
														$selected = ($fetchCustomerService['jenis_id'] == $rowGelar['jenis_id']) ? 'selected' : "";
													?>
													<option value="<?php echo $rowGelar['jenis_id']; ?>" <?php echo $selected; ?>><?php echo $rowGelar['jenis_id'] . ' - ' . $rowGelar['nama_identitas']; ?></option>
													<?php
													}
													?>
												</select>
												<label for="jenis_id">Identitas</label>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="tglid_container" required>
													<div class="input-group-content">
														<input type="text" class="form-control" id="tglid" name="tglid" value="<?php echo $fetchCustomerService['tglid']; ?>">
														<label for="tglid">Berlaku s/d Tgl</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group">
													<div class="input-group-content">
														<input type="text" class="form-control" id="kode_area" name="kode_area" value="<?php echo $fetchCustomerService['kode_area']; ?>" required>
														<label for="kode_area">Kode Area</label>
													</div>
													<div class="input-group-content">
														<input type="text" class="form-control" id="telpon" name="telpon" value="<?php echo $fetchCustomerService['telpon']; ?>" required>
														<label for="telpon">No. Telpon</label>
													</div>
												</div>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="IBU_KANDUNG" name="IBU_KANDUNG" value="<?php echo $fetchCustomerService['IBU_KANDUNG']; ?>" required>
												<label for="IBU_KANDUNG">Ibu Kandung</label>
											</div>

											<div class="form-group floating-label">
												<textarea name="alamat" id="alamat" class="form-control" rows="3" placeholder="" required><?php echo $fetchCustomerService['alamat']; ?></textarea>
												<label for="alamat">Alamat KTP</label>
											</div>

									</div>
								</div>	


							</div>
							
							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="KET_GELAR" name="KET_GELAR" value="<?php echo $fetchCustomerService['KET_GELAR']; ?>">
												<label for="KET_GELAR">Ket. Gelar</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="no_id" name="no_id" value="<?php echo $fetchCustomerService['no_id']; ?>" required>
												<label for="no_id">No. Identitas</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_NIP" name="NO_NIP" value="<?php echo $fetchCustomerService['NO_NIP']; ?>">
												<label for="NO_NIP">NIP/NPK</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="npwp" name="npwp" value="<?php echo $fetchCustomerService['npwp']; ?>">
												<label for="npwp">NPWP</label>
											</div>

											<div class="form-group floating-label">
												<select id="Status_Marital" name="Status_Marital" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlScrPerkawinan = "SELECT * FROM scr_perkawinan WHERE 1";
													$fetchScrPerkawinan = mysql_query($sqlScrPerkawinan);
													while($rowScrPerkawinan = mysql_fetch_array($fetchScrPerkawinan))
													{
														$selected = ($fetchCustomerService['Status_Marital'] == $rowScrPerkawinan['Deskripsi']) ? 'selected' : "";
													?>
													<option value="<?php echo $rowScrPerkawinan['Deskripsi']; ?>" <?php echo $selected; ?>><?php echo $rowScrPerkawinan['kode_perkawinan'] . ' - ' . $rowScrPerkawinan['Deskripsi']; ?></option>
													<?php
													}
													?>
												</select>
												<label for="Status_Marital">Status Martial</label>
											</div>

											<div class="form-group floating-label">
												<div class="input-group">
													<div class="input-group-content">
														<input type="text" class="form-control" id="kelurahan" name="kelurahan" value="<?php echo $fetchCustomerService['kelurahan']; ?>" required>
														<label for="kelurahan">Kelurahan</label>
													</div>
													<div class="input-group-content">
														<input type="text" class="form-control" id="kecamatan" name="kecamatan" value="<?php echo $fetchCustomerService['kecamatan']; ?>" required>
														<label for="kecamatan">Kecamatan</label>
													</div>
												</div>
											</div>

									</div>
								</div>	

							</div>

						</div>

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<select id="KOTA" name="KOTA" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlJenisKota = "SELECT * FROM jenis_kota WHERE 1";
													$fetchJenisKota = mysql_query($sqlJenisKota);
													while($rowJenisKota = mysql_fetch_array($fetchJenisKota))
													{
														$selected = ($fetchCustomerService['KOTA'] == $rowJenisKota['Kota_id_2013']) ? 'selected' : "";
													?>
													<option value="<?php echo $rowJenisKota['Kota_id_2013']; ?>" <?php echo $selected; ?>>
														<?php echo $rowJenisKota['Kota_id_2013']; ?> - 
														<?php echo ($rowJenisKota['Deskripsi_Kota_2013']) ? $rowJenisKota['Deskripsi_Kota_2013'] : $rowJenisKota['Deskripsi_Kota']; ?>
													</option>
													<?php
													}
													?>
												</select>
												<label for="KOTA">Kota</label>
											</div>

											<div class="form-group floating-label">
												<select id="pekerjaan_id" name="pekerjaan_id" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlJenisPekerjaan = "SELECT * FROM jenis_pekerjaan WHERE 1";
													$fetchJenisPekerjaan = mysql_query($sqlJenisPekerjaan);
													while($rowJenisPekerjaan = mysql_fetch_array($fetchJenisPekerjaan))
													{
														$selected = ($fetchCustomerService['pekerjaan_id'] == $rowJenisPekerjaan['Pekerjaan_id']) ? 'selected' : "";
													?>
													<option value="<?php echo $rowJenisPekerjaan['Pekerjaan_id']; ?>" <?php echo $selected; ?>>
														<?php echo $rowJenisPekerjaan['Pekerjaan_id']; ?> - 
														<?php echo $rowJenisPekerjaan['Desktripsi_Pekerjaan']; ?>
													</option>
													<?php
													}
													?>
												</select>
												<label for="pekerjaan_id">Pekerjaan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="TUJUAN_PEMBUKAAN_KYC" name="TUJUAN_PEMBUKAAN_KYC" value="<?php echo $fetchCustomerService['TUJUAN_PEMBUKAAN_KYC']; ?>" required>
												<label for="TUJUAN_PEMBUKAAN_KYC">Tujuan Pembukaan Rekening</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="PENDAPATAN_KYC" name="PENDAPATAN_KYC" value="<?php echo $fetchCustomerService['PENDAPATAN_KYC']; ?>" required>
												<label for="PENDAPATAN_KYC">Pendapatan</label>
											</div>


									</div>
								</div>	

							</div>

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<select id="Kode_Negara" name="Kode_Negara" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$sqlKodeNegara = "SELECT * FROM kodenegara WHERE 1";
													$fetchKodeNegara = mysql_query($sqlKodeNegara);
													while($rowKodeNegara = mysql_fetch_array($fetchKodeNegara))
													{
														$selected = ($rowKodeNegara['KODE_NEGARA'] == $fetchCustomerService['Kode_Negara']) ? "selected" : "";
													?>
													<option value="<?php echo $rowKodeNegara['KODE_NEGARA']; ?>" <?php echo $selected; ?>>
														<?php echo $rowKodeNegara['KODE_NEGARA'] . ' - ' . $rowKodeNegara['DESKRIPSI_NEGARA']; ?>
													</option>
													<?php
													}
													?>
												</select>
												<label for="Kode_Negara">Kode Negara</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="pekerjaan" name="pekerjaan" value="<?php echo $fetchCustomerService['pekerjaan']; ?>">
												<label for="pekerjaan">Ket. Pekerjaan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="SUMBER_DANA_KYC" name="SUMBER_DANA_KYC" value="<?php echo $fetchCustomerService['SUMBER_DANA_KYC']; ?>" required>
												<label for="SUMBER_DANA_KYC">Sumber Dana</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_HP" name="NO_HP" value="<?php echo $fetchCustomerService['NO_HP']; ?>">
												<label for="NO_HP">No. HP</label>
											</div>

									</div>
								</div>	

							</div>
							
						</div>	

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_PASSPORT" name="NO_PASSPORT" value="<?php echo $fetchCustomerService['NO_PASSPORT']; ?>">
												<label for="NO_PASSPORT">No. Passport</label>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="tglmulaipassport_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="TGL_MULAI_PASSPORT" name="TGL_MULAI_PASSPORT" value="<?php echo $fetchCustomerService['TGL_MULAI_PASSPORT']; ?>">
														<label for="TGL_MULAI_PASSPORT">Tgl Mulai Passport</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="tglakhirpassport_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="TGL_AKHIR_PASSPORT" name="TGL_AKHIR_PASSPORT" value="<?php echo $fetchCustomerService['TGL_AKHIR_PASSPORT']; ?>">
														<label for="TGL_AKHIR_PASSPORT">Tgl Akhir Passport</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

									</div>
								</div>

							</div>

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<input type="file" class="form-control" id="PATH_FOTO" name="PATH_FOTO" value="">
												<label for="PATH_FOTO">Foto</label>
												<div>
													<img src="<?php echo $fetchCustomerService['PATH_FOTO']; ?>" width="200" />
												</div>
											</div>

											<div class="form-group">
												<input type="file" class="form-control" id="PATH_TTANGAN" name="PATH_TTANGAN" value="">
												<label for="PATH_TTANGAN">Tanda Tangan</label>
												<div>
													<img src="<?php echo $fetchCustomerService['PATH_TTANGAN']; ?>" width="200" />
												</div>
											</div>

									</div>
								</div>

							</div>	

						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
			?>
			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully updated. <a href="customer_service_edit.php?nasabah_id=<?php echo $nasabah_id; ?>">See Again</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			}
			else if ($message == 2)
			{
			?>
			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-danger" role="alert">
								<strong>Oh snap!</strong> Change a few things up and try submitting again. <a href="customer_service_entrydata.php">Try Again</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			}
			?>

		</div><!--end #content-->
		<!-- END CONTENT -->

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	$('#tgl_buka_container , #tgl_lahir_container , #tglid_container , #tglmulaipassport_container , #tglakhirpassport_container').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>
