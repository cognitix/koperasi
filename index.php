<?php 
require_once "connect.php";

if (empty($_SESSION['user']))
{
	require_once "login.php";
}
else
{
	require_once "home.php";
}
?>