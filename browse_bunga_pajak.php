<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "browse_bunga_pajak";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Browse Bunga dan Pajak</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<?php 
											/*
											$nomor_rekening = (!empty($_GET['nomor_rekening'])) ? $_GET['nomor_rekening'] : "";
											$nasabah_id = $nama_nasabah = "";

											if ($nomor_rekening)
											{

												$sql = "SELECT 
														tabung.*,
														nasabah.nama_nasabah ,
														nasabah.alamat
														FROM tabung 
														JOIN nasabah ON tabung.NASABAH_ID = nasabah.nasabah_id
														WHERE 1
														AND NO_REKENING = '".$nomor_rekening."'
														";

												$fetch = mysql_fetch_array( mysql_query($sql) );

												$nasabah_id = $fetch['NASABAH_ID'];
												$nama_nasabah = $fetch['nama_nasabah'];
											}
											*/
											?>

											<div class="form-group">
												<div class="input-group date" >
													<div class="input-group-content">
														<input type="text" class="form-control" name="saldo-month" value="<?php echo date("t/m/Y"); ?>" readonly>
														<label>Process Saldo nominatif sukarela (simpanan sukarela) akhir bulan ini</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<?php /*

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" data-source="autosuggest_nomorrekening_all.php" value="<?php echo $nomor_rekening; ?>">
												<label for="nomor_rekening">Nomor Rekening</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nasabah_id" name="nasabah_id" readonly value="<?php echo $nasabah_id; ?>">
												<label for="nasabah_id">Anggota ID</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly value="<?php echo $nama_nasabah; ?>">
												<label for="nama_nasabah">Nama Anggota</label>
											</div> 

											*/ ?>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>

					<hr />

					<?php 

					if (!empty($_GET['saldo-month']))
					{

						//$nomor_rekening = $_GET['nomor_rekening'];

						//AND NO_REKENING = '".$nomor_rekening."'

						$sql = "SELECT 
								tabung.*,
								nasabah.nasabah_id,
								nasabah.nama_nasabah ,
								nasabah.alamat
								FROM tabung 
								JOIN nasabah ON tabung.NASABAH_ID = nasabah.nasabah_id 
								JOIN tabtrans ON tabung.NO_REKENING = tabtrans.NO_REKENING
								WHERE 1
								AND STATUS_AKTIF = 2
								AND DATE_FORMAT(tabtrans.TGL_TRANS,'%m') = '".date('m')."'
								GROUP BY nasabah.nasabah_id
								";

						$query = mysql_query($sql);
					?>

					<section class="style-default-bright">

						<form method="post" action="cetak_buku_simpanan.php">

							<div class="row">
								<div class="col-lg-12">

									<div class="table-responsive">
										<table id="datatable1" class="table table-striped table-hover">
											<thead>
												<tr>
													<th>No. Anggota</th>
													<th>Nama Anggota</th>
													<th>No. Rekening</th>
													<th>Total Bunga</th>
													<th>Total Pajak</th>
													<th>Total ADM</th>
													<th>Saldo Nomintatif Simpanan Sukarela</th>
												</tr>
											</thead>
											<tbody>

												<?php 
												while($fetch = mysql_fetch_array($query))
												{
												?>

												<tr>
													<td><?php echo $fetch['nasabah_id']; ?></td>
													<td><?php echo $fetch['nama_nasabah']; ?></td>
													<td><?php echo $fetch['NO_REKENING']; ?></td>
													<td><?php echo number_format($fetch['BUNGA_BLN_INI'],2,'.',','); ?></td>
													<td><?php echo number_format($fetch['PAJAK_BLN_INI'],2,'.',','); ?></td>
													<td><?php echo number_format($fetch['ADM_BLN_INI'],2,'.',','); ?></td>
													<td><?php echo number_format($fetch['simpanan_sukarela'],2,'.',','); ?></td>
												</tr>

												<?php
												}
												?>

											</tbody>
										</table>

									</div>
								</div>
							</div>

						</form>

						<a href="print_browse_bunga_pajak.php" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print</a>

					</section>

					<?php 
					}
					?>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "print_buku_simpanan.php";
				require_once "layouts/message_success.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#nasabah_id').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});
});
</script>

<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>