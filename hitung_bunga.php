<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "hitung_bunga";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	//$saldoMonth = $_POST['saldo-month'];

	// >= 7500000

	$days_of_month = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
	//echo "days_of_month : " . $days_of_month . ' <br />';

	$sql = "
	SELECT
	tabung.*
	FROM
	tabung 
	JOIN tabtrans ON tabung.NO_REKENING = tabtrans.NO_REKENING
	WHERE 1
	AND STATUS_AKTIF = 2
	AND DATE_FORMAT(tabtrans.TGL_TRANS,'%m') = '".date('m')."'
	GROUP BY tabung.NO_REKENING, tabung.NASABAH_ID
	";

	//echo $sql;die();

	$TGL_TRANS = date('Y-m-d');

	$query = mysql_query($sql);
	while($fetch = mysql_fetch_array($query))
	{
		$total_bunga = $fetch['simpanan_sukarela'] * ($fetch['SUKU_BUNGA'] / 100) * $days_of_month / 365;
			
		$total_pajak = 0;
			
		if ($fetch['simpanan_sukarela'] >= 7500000)
		{
			$total_pajak = $total_bunga * $fetch['PERSEN_PPH'] / 100;
		}

		$total_biaya_adm = ((int) $fetch['ADM_PER_BLN']) ? $fetch['ADM_PER_BLN'] : 0;

		//echo 'NasabahId: ' . $fetch['NASABAH_ID'] . '<br />';
		//echo 'NoRekening: ' . $fetch['NO_REKENING'] . '<br />';
		//echo 'Saldo Simpana Sukarela: ' . round($fetch['simpanan_sukarela'], 2) . '<br />';
		//echo 'Total Bunga ('.$fetch['SUKU_BUNGA'].'): ' . round($total_bunga, 2) . '<br />';
		//echo 'Total Pajak ('.$fetch['PERSEN_PPH'].'): ' . round($total_pajak, 2) . '<br />';

		$simpanan_sukarela = $fetch['simpanan_sukarela'] + $total_bunga - $total_pajak - $total_biaya_adm;
		//echo 'Saldo Simpana Sukarela: ' . round($simpanan_sukarela, 2) . '<br />';

		//echo 'Biaya ADM: ' . round($total_biaya_adm, 2) . '<br /><br /><br />';

		$userId = $user['USERID'];
		$cab = $kodecabang['kode_cab'];

		$sql = "
		INSERT INTO tabtrans SET 
		TGL_TRANS = '".$TGL_TRANS."' ,
		NO_REKENING = '".$fetch['NO_REKENING']."' ,
		MY_KODE_TRANS = '110' ,
		KODE_TRANS = '07' ,
		kuitansi = 'SYS-BUNGA' ,
		TOB = 'O' ,
		USERID = '".$userId."' ,
		NO_TELLER = '".$userId."' ,
		SALDO_TRANS = '".$total_bunga."' ,
		FLAG_CETAK = 'N' ,
		CAB = '".$cab."'
		";
		mysql_query($sql);

		$sql = "
		INSERT INTO tabtrans SET 
		TGL_TRANS = '".$TGL_TRANS."' ,
		NO_REKENING = '".$fetch['NO_REKENING']."' ,
		MY_KODE_TRANS = '210' ,
		KODE_TRANS = '09' ,
		kuitansi = 'SYS-PAJAK' ,
		TOB = 'O' ,
		USERID = '".$userId."' ,
		NO_TELLER = '".$userId."' ,
		SALDO_TRANS = '".$total_pajak."' ,
		FLAG_CETAK = 'N' ,
		CAB = '".$cab."'
		";
		mysql_query($sql);	

		$sql = "
		INSERT INTO tabtrans SET 
		TGL_TRANS = '".$TGL_TRANS."' ,
		NO_REKENING = '".$fetch['NO_REKENING']."' ,
		MY_KODE_TRANS = '275' ,
		KODE_TRANS = '08' ,
		kuitansi = 'SYS-ADM' ,
		TOB = 'O' ,
		USERID = '".$userId."' ,
		NO_TELLER = '".$userId."' ,
		SALDO_TRANS = '".$total_biaya_adm."' ,
		FLAG_CETAK = 'N' ,
		CAB = '".$cab."'
		";
		mysql_query($sql);		

		$sql = "
		UPDATE tabung SET 
		SALDO_NOMINATIF = '".$simpanan_sukarela."' ,
		simpanan_sukarela = '".$simpanan_sukarela."' ,
		BUNGA_BLN_INI = '".$total_bunga."' ,
		PAJAK_BLN_INI = '".$total_pajak."' ,
		ADM_BLN_INI = '".$total_biaya_adm."'
		WHERE 1
		AND NO_REKENING = '".$fetch['NO_REKENING']."'
		";
		mysql_query($sql);
	}

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Hitung Bunga</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data" action="hitung_bunga.php">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										
										<div class="form-group">
											<div class="input-group date">
												<div class="input-group-content">
													<input type="text" class="form-control" name="saldo-month" value="<?php echo date("t/m/Y"); ?>" readonly>
													<label>Process Saldo nominatif sukarela (simpanan sukarela) akhir bulan ini</label>
												</div>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

						<?php 
						if ($message == 1)
						{
							$linkBack = "hitung_bunga.php";
							require_once "layouts/message_success.php";
						}
						?>

					</form>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script>
$(function(){
	$('#saldo-this-month').datepicker({
		autoclose: true, 
		todayHighlight: true, 
		minViewMode: 1,
		format: "yyyy-mm"
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>