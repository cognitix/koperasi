<?php 
require_once "connect.php";

$sql = "SELECT 
		tabung.*,
		nasabah.nama_nasabah ,
		nasabah.alamat
		FROM tabung 
		JOIN nasabah ON tabung.NASABAH_ID = nasabah.nasabah_id
		WHERE 1
		";

$fetch = mysql_query($sql);

$datas = array();

while($row = mysql_fetch_array($fetch))
{
	$datas[] = array(
		'NO_REKENING' => $row['NO_REKENING'],
		'nasabah_id' => $row['NASABAH_ID'],
		'nama_nasabah' => $row['nama_nasabah'],
		'alamat' => $row['alamat'],

		'SUKU_BUNGA' => $row['SUKU_BUNGA'],
		'PERSEN_PPH' => $row['PERSEN_PPH'],
		'TGL_REGISTRASI' => $row['TGL_REGISTRASI'],
		'ADM_PER_BLN' => $row['ADM_PER_BLN'],
		'PERIODE_ADM' => $row['PERIODE_ADM'],

		'SALDO_AWAL' => $row['SALDO_AWAL'],
		'SALDO_SETORAN' => $row['SALDO_SETORAN'],
		'SALDO_NOMINATIF' => $row['SALDO_NOMINATIF'],
		'simpanan_pokok' => $row['simpanan_pokok'],
		'simpanan_sukarela' => $row['simpanan_sukarela'],
		'simpanan_wajib' => $row['simpanan_wajib'],

		'value' => $row['NO_REKENING'] . ' - ' . $row['NASABAH_ID'] . ' - ' . $row['nama_nasabah']
	);
}

echo json_encode($datas);

?>