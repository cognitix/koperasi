<?php 
require_once "connect.php";

$NO_REKENING = $_GET['NO_REKENING'];
$pelunasan_semua = $_GET['pelunasan_semua'];

$sql = "SELECT * FROM kretrans WHERE 1 AND NO_REKENING = '".$NO_REKENING."' AND MY_KODE_TRANS = 300 ORDER BY KRETRANS_ID DESC";
$result = mysql_fetch_array(mysql_query($sql));

$pelunasan = 1;

if (! empty($result['PELUNASAN']))
{
	$pelunasan = $result['PELUNASAN'] + 1;
}

if ($pelunasan_semua)
{

	$sql = "SELECT 
	SUM(POKOK_TRANS) as POKOK_TRANS, 
	SUM(BUNGA_TRANS) as BUNGA_TRANS, 
	SUM(ADMIN_TRANS) as ADMIN_TRANS, 
	MAX(ANGSURAN_KE) as ANGSURAN_KE
	FROM kretrans WHERE 1 AND NO_REKENING = '".$NO_REKENING."' AND ANGSURAN_KE >= ".$pelunasan." ";
	$result = mysql_fetch_array(mysql_query($sql));

	$tgl_tagihan = date('Y-m-d');
	$pelunasan = $result['ANGSURAN_KE'];
	$pokok_trans = $result['POKOK_TRANS'];
	$bunga_trans = $result['BUNGA_TRANS'];
	$admin_trans = $result['ADMIN_TRANS'];
	$jumlah = $pokok_trans + $bunga_trans + $admin_trans;

}
else
{

	$sql = "SELECT * FROM kretrans WHERE 1 AND NO_REKENING = '".$NO_REKENING."' AND ANGSURAN_KE = '".$pelunasan."'";
	$result = mysql_fetch_array(mysql_query($sql));

	$tgl_tagihan = $result['TGL_TRANS'];
	$pokok_trans = $result['POKOK_TRANS'];
	$bunga_trans = $result['BUNGA_TRANS'];
	$admin_trans = $result['ADMIN_TRANS'];
	$jumlah = $pokok_trans + $bunga_trans + $admin_trans;

}

echo json_encode(array(
	'pelunasan' => $pelunasan,
	'tgl_tagihan' => $tgl_tagihan,
	'pokok_trans' => $pokok_trans,
	'bunga_trans' => $bunga_trans,
	'admin_trans' => $admin_trans,
	'jumlah' => $jumlah,
));

?>