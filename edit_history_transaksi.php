<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "history_transaksi";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$trans_id = $_GET['trans_id'];

$message = 0;

if (isset($_POST['simpan']))
{
	$nomor_bukti = $_POST['nomor_bukti'];
	$tanggal_transaksi = $_POST['tanggal_transaksi'];
	//$jumlah = $_POST['jumlah'];
	$keterangan = $_POST['keterangan'];
	$kode_perkiraan = $_POST['kode_perkiraan'];
	$nama_perkiraan = $_POST['nama_perkiraan'];
	$debet = $_POST['debet'];
	$kredit = $_POST['kredit'];
	$kode_jurnal = "JU";
	$src = "GL";

	$sqlTransSimpan = "
	SELECT
	trans_master.tgl_trans ,
	trans_master.kode_jurnal ,
	trans_master.no_bukti ,
	trans_master.NOMINAL ,
	trans_detail.trans_id ,
	trans_detail.URAIAN ,
	trans_detail.kode_perk ,
	trans_detail.debet ,
	trans_detail.kredit ,
	trans_detail.master_id ,
	trans_detail.saldo_akhir ,
	perkiraan.nama_perk ,
	perkiraan.saldo_debet ,
	perkiraan.saldo_kredit
	FROM
	trans_detail
	JOIN trans_master ON trans_detail.master_id = trans_master.trans_id
	JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
	WHERE 1
	AND trans_detail.trans_id = '".$trans_id."'
	";

	$queryTransSimpan = mysql_query($sqlTransSimpan);
	$resultSimpan = mysql_fetch_array($queryTransSimpan);

	$selisih = 0;

	if ((int) $debet)
	{
		$selisih = $resultSimpan['NOMINAL'] - $debet;
		$posisi = "debet";
		$jumlah = $debet;		
	}
	else if ((int) $kredit)
	{
		$selisih = $resultSimpan['NOMINAL'] - $kredit;
		$posisi = "kredit";
		$jumlah = $kredit;
	}

	$nominal = $resultSimpan['NOMINAL'] - $selisih;
	$saldo_akhir = $resultSimpan['saldo_akhir'] - $selisih;

	$sqlInsertTransMaster = "
	UPDATE trans_master SET 
	tgl_trans = '".$tanggal_transaksi."' ,
	kode_jurnal = '".$kode_jurnal."' ,
	no_bukti = '".$nomor_bukti."' ,
	src = '".$src."' ,
	NOMINAL = '".$nominal."' ,
	KETERANGAN = '".$keterangan."'
	WHERE 1
	AND trans_id = '".$resultSimpan['master_id']."'
	";

	mysql_query($sqlInsertTransMaster);

	$master_id = $resultSimpan['master_id'];

	$sqlTransDetail = "
	UPDATE trans_detail SET 
	master_id = '".$master_id."' ,
	URAIAN = '".$keterangan."' ,
	kode_perk = '".$kode_perkiraan."' ,
	".$posisi." = '".$jumlah."' ,
	saldo_akhir = '".$saldo_akhir."'
	WHERE 1
	AND trans_id = '".$resultSimpan['trans_id']."' 
	";

	mysql_query($sqlTransDetail);

	$sqlTransDetail = "
	SELECT 
	trans_detail.* ,
	
	perkiraan.kode_perk ,
	perkiraan.nama_perk ,
	perkiraan.saldo_debet ,
	perkiraan.saldo_kredit

	FROM trans_detail 
	JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
	WHERE 1 
	AND master_id = '".$master_id."'
	AND trans_id = '".$resultSimpan['trans_id']."' 
	";

	$queryTransDetail = mysql_query($sqlTransDetail);

	while($fetchTransDetail = mysql_fetch_array($queryTransDetail))
	{
		$saldo_debet = $fetchTransDetail['saldo_debet'] + $fetchTransDetail['debet'] - $fetchTransDetail['kredit'];
		$saldo_kredit = $fetchTransDetail['saldo_kredit'] + $fetchTransDetail['debet'] - $fetchTransDetail['kredit'];
		$saldo_akhir = $saldo_debet - $saldo_kredit;

		$kode_perk = $fetchTransDetail['kode_perk'];

		$sql = "
		UPDATE perkiraan SET 
		saldo_debet = '".$saldo_debet."' ,
		saldo_kredit = '".$saldo_kredit."' ,
		saldo_akhir = '".$saldo_akhir."'
		WHERE 1
		AND kode_perk = '".$kode_perk."'
		";

		mysql_query($sql);
	}

	$message = 1;
}


$sqlTrans = "
SELECT
trans_master.tgl_trans ,
trans_master.kode_jurnal ,
trans_master.no_bukti ,
trans_detail.trans_id ,
trans_detail.URAIAN ,
trans_detail.kode_perk ,
trans_detail.debet ,
trans_detail.kredit ,
perkiraan.nama_perk
FROM
trans_detail
JOIN trans_master ON trans_detail.master_id = trans_master.trans_id
JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
WHERE 1
AND trans_detail.trans_id = '".$trans_id."'
";

$queryTrans = mysql_query($sqlTrans);
$result = mysql_fetch_array($queryTrans);

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
				$linkBack = "edit_history_transaksi.php";
				require_once "layouts/message_success.php";
			}
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Edit History Transaksi</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group">
											<input type="text" class="form-control" id="nomor_bukti" name="nomor_bukti" value="<?php echo $result['no_bukti']; ?>" >
											<label for="nomor_bukti">Nomor Bukti</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="tanggal_transaksi" name="tanggal_transaksi" value="<?php echo $result['tgl_trans']; ?>" />
											<label for="tanggal_transaksi">Tanggal Transaksi</label>
										</div>

										<div class="form-group ">
											<input type="text" class="form-control" id="keterangan" name="keterangan" value="<?php echo $result['URAIAN']; ?>">
											<label for="keterangan">Keterangan</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="kode_perkiraan" name="kode_perkiraan" value="<?php echo $result['kode_perk']; ?>" data-source="autosuggest_gltrans.php">
											<label for="kode_perkiraan">Kode Perkiraan</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="nama_perkiraan" name="nama_perkiraan" value="<?php echo trim($result['nama_perk']); ?>">
											<label for="nama_perkiraan">Nama Perkiraan</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="debet" name="debet" value="<?php echo $result['debet']; ?>">
											<label for="debet">Debet</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="kredit" name="kredit" value="<?php echo $result['kredit']; ?>">
											<label for="kredit">Kredit</label>
										</div>

									</div>
								</div>

								<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="simpan">Simpan</button>

							</div>

						</div>	

					</form>	

					<hr />

				</div>
			</section>

		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tanggal_transaksi').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#kode_perkiraan').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#kode_perkiraan").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#kode_perkiraan').val(ui.item.kode_perk);
					$('#nama_perkiraan').val(ui.item.nama_perk);
					
					return false;
				},
				focus: function( event, ui ) {

					$('#kode_perkiraan').val(ui.item.kode_perk);
			        $('#nama_perkiraan').val(ui.item.nama_perk);
			        
			        return false;
			    },
			});
		}
	});

});
</script>	

<?php require_once "layouts/footer.php"; ?>
	