<?php 

require_once "connect.php";

if (!empty($_SESSION['user'])) header('location:home.php');

$title = "Login";

require_once "layouts/head.php"; 

if (isset($_POST['login']))
{
	$username = $_POST['username'];
	$password = $_POST['password'];
	$password = sha1($password . $salt);
	$kodekantor = $_POST['kodekantor'];

	$sql = "SELECT * FROM kodecabang WHERE 1 AND kode_cab = '$kodekantor'";
	$fetchKodeCabang = mysql_fetch_array(mysql_query($sql));

	if (empty($fetchKodeCabang))
	{
		echo 'Login Error . <a href="login.php">Try Again</a>';
		die();
	}

	$sql = "SELECT * FROM passwd WHERE 1 AND USERNAME = '$username' AND PASSWORD = '$password'";
	$fetchUser = mysql_fetch_array(mysql_query($sql));

	if (empty($fetchUser))
	{
		echo 'Login Error . <a href="login.php">Try Again</a>';
		die();
	}

	$_SESSION['user'] = $fetchUser;
	$_SESSION['kodecabang'] = $fetchKodeCabang;
	$_SESSION['kodekantor'] = $kodekantor;
	$_SESSION['username'] = $username;

	header('location:home.php');
}

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

?>

	<body class="menubar-hoverable header-fixed ">

		<!-- BEGIN LOGIN SECTION -->
		<section class="section-account">
			<div class="img-backdrop" style="background-image: url('assets/img/img16.jpg')"></div>
			<div class="spacer"></div>
			<div class="card contain-sm style-transparent">
				<div class="card-body">
					<div class="row">
						<div class="col-sm-6">
							<br/>
							<span class="text-lg text-bold text-primary">Koperasi</span>
							<br/><br/>
							<form class="form floating-label" accept-charset="utf-8" method="post">
								<div class="form-group">
									<input type="text" class="form-control" id="username" name="username">
									<label for="username">Username</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="kodekantor" name="kodekantor">
									<label for="kodekantor">Kode Kantor</label>
									<p class="help-block" id="namakantor"></p>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="password" name="password">
									<label for="password">Password</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="tglsystem" name="tglsystem" disabled value="<?php echo $tglsystem; ?>">
									<label for="tglsystem">Tanggal System</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="konfirmasi_tglsystem" name="konfirmasi_tglsystem" disabled value="<?php echo $tglsystem; ?>">
									<label for="tglsystem">Konfirmasi Tanggal System</label>
								</div>
								<br/>
								<div class="row">
									<div class="col-xs-6 text-left">
										<button class="btn btn-primary btn-raised" type="submit" name="login">Login</button>
									</div><!--end .col -->
									<div class="col-xs-6 text-right">
										
									</div><!--end .col -->
								</div><!--end .row -->
							</form>
						</div><!--end .col -->
					</div><!--end .card -->
				</div>
			</div>
		</section>
		<!-- END LOGIN SECTION -->

<?php require_once "layouts/foot.php"; ?>

<script type="text/javascript">
$(function(){
	$('#kodekantor').bind('keyup', function(){
		var el = $(this);
		if (el.data('working')) return false;
		el.data('working', true);
		$.ajax({
			type : 'POST',
			url : 'autosuggest_nama_kantor.php',
			data : {
				kodekantor : el.val()
			},
			dataType: 'json',
			success : function(response){
				$('#namakantor').text('');

				if (response)
					$('#namakantor').text(response.nama_cab);

				el.data('working', false);
			}
		});
		return false;
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>