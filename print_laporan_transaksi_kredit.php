<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl_transaksi1 = $_GET['tgl_transaksi1'];
$tgl_transaksi2 = $_GET['tgl_transaksi2'];
$kode_cab = (!empty($_GET['kode_cab'])) ? $_GET['kode_cab'] : "";
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
    font-size:11px;
    line-height: 1.2;
    /*width:650px;*/
}
.table-custom tbody tr td
{
    border:none;
    padding:4px;
    line-height: 1.5;
}
.table-custom2 tbody tr td
{
    border:none;
    padding:0 2px;
    line-height: 1.5;
}
.table-custom thead tr th{
    text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
    padding:4px;
    line-height: 1.5;
    border:1px solid #000;
    border-top:1px solid #000 !important;
}
.table-custom tr th{
    text-align: center;
    border-top:1px solid #000 !important;
    border-bottom:1px solid #000 !important;
    padding:2px !important;
}
.table-custom-border tr td{
    line-height: 1.5;
    padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
    padding:10px !important;
}
</style>

<div class="card body-print">
    <div class="card-body">

        <div>
            <div class="pull-left">
                <h4>Laporan Transaksi Kredit</h4>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover">
                
                <tr>
                    <th>No</th>
                    <th>Cabang</th>
                    <th>Nama Nasabah</th>
                    <th>No Passport</th>
                    <th>No Rekening</th>
                    <th>Tgl Transaksi</th>
                    <th>Pokok Trans</th>
                </tr>

                <?php 
                $sql = "
                SELECT  
                kredit.JML_PINJAMAN ,
                nasabah.nama_nasabah ,
                nasabah.NO_PASSPORT ,
                kredit.TGL_REALISASI,
                kredit.NO_REKENING,
                kodecabang.kode_cab,
                kodecabang.nama_cab
                FROM kredit 
                JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id 
                JOIN kodecabang ON kodecabang.kode_cab = kredit.CAB
                WHERE 1 
                AND TGL_REALISASI BETWEEN '".$tgl_transaksi1."' AND '".$tgl_transaksi2."'";

                if ($kode_cab)
                {
                    $sql .= "AND kodecabang.kode_cab = '".$kode_cab."'";
                }

                $query = mysql_query($sql);

                $no = 1;
                $totalPinjaman = 0;

                while($result = mysql_fetch_array($query))
                {
                    $totalPinjaman += $result['JML_PINJAMAN'];
                ?>

                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $result['kode_cab'].' ('.$result['nama_cab'].')'; ?></td>
                    <td><?php echo $result['nama_nasabah']; ?></td>
                    <td><?php echo $result['NO_PASSPORT']; ?></td>
                    <td><?php echo $result['NO_REKENING']; ?></td>
                    <td><?php echo date("d/M/Y", strtotime($result['TGL_REALISASI'])); ?></td>
                    <td><?php echo number_format($result['JML_PINJAMAN'],0,'',','); ?></td>
                </tr>

                <?php
                }
                ?>

                <tr>
                    <td colspan="6" align="right"><b>Total</b></td>
                    <td><b><?php echo number_format($totalPinjaman,0,"",","); ?></b></td>
                </tr>

            </table>
        </div>

    </div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>


<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>