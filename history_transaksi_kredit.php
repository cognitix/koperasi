<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "history_transaksi_kredit";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl1 = date("Y-m-d", strtotime("-14 day"));
$tgl2 = date("Y-m-d");
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">History Transaksi Kredit</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl1" name="tgl1" value="<?php echo $tgl1; ?>" required>
														<label for="tgl1">From</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl2" name="tgl2" value="<?php echo $tgl2; ?>" required>
														<label for="tgl2">To</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Search</button>

					</form>

					<hr />

					<?php 
					if (isset($_POST['submit']))
					{
						$tgl1 = $_POST['tgl1'];
						$tgl2 = $_POST['tgl2'];	

						$sqlTellerTrans = "
						SELECT 
						tellertrans.*
						FROM tellertrans 
						WHERE 1 
						AND modul = 'KRE'
						AND tgl_trans BETWEEN '".$tgl1."' AND '".$tgl2."'
						ORDER BY tellertrans.trans_id
						";

						$queryTellerTrans = mysql_query($sqlTellerTrans);

					?>

					<section class="style-default-bright">

						<div class="section-header">
							<ol class="breadcrumb">
								<li class="active">History Transaksi Kredit</li>
								<?php 
								if ($tgl1 && $tgl2)
								{
								?>
								<li><?php echo 'Periode: ' . date("d/m/Y", strtotime($tgl1)) .' s/d '. date("d/m/Y", strtotime($tgl2)); ?></li>
								<?php 
								}
								?>
							</ol>
						</div>

						<div class="row">
							<div class="col-lg-12">

								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>No</th>
												<th>Tanggal Transaksi</th>
												<th>Modul</th>
												<th>Kode Jurnal</th>
												<th>No Bukti</th>
												<th>Uraian</th>
												<th>Saldo Trans</th>
											</tr>
										</thead>
										<tbody>

											<?php 
											$i = 1;
											while($result = mysql_fetch_array($queryTellerTrans))
											{
											?>

											<tr>
												<td><?php echo $i++; ?>
													<div class="btn-group" role="group" aria-label="Justified button group">
														<a href="delete_history_transaksi_kredit.php?trans_id=<?php echo $result['trans_id']; ?>" class="btn ink-reaction btn-default-bright btn-xs btn-primary" role="button" onclick="return confirm('Yakin mau mendelete ?')">Delete</a>
													</div>
												</td>
												<td><?php echo date("d M Y", strtotime($result['tgl_trans'])); ?></td>
												<td><?php echo $result['modul']; ?></td>
												<td><?php echo $result['kode_jurnal']; ?></td>
												<td><?php echo $result['NO_BUKTI']; ?></td>
												<td><?php echo $result['uraian']; ?></td>
												<td><?php echo number_format($result['saldo_trans'],0,'.',','); ?></td>
											</tr>	

											<?php
											}
											?>

										</tbody>
									</table>
								</div>

							</div>
						</div>

					</section>

					<?php 
					}
					?>

				</div>	
			</section>

			<?php require_once "layouts/home/menus.php"; ?>

		</div>

	</div>
	
<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<script type="text/javascript">
$(function(){
	
	$('#tgl1').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$('#tgl2').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>		