<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "print_rekening_koran";

$no_rekening = $_GET['no_rekening'];

$sql = "SELECT 
		tabtrans.*,

		nasabah.nama_nasabah ,
		nasabah.nasabah_id ,
				
		kodetranstabungan.KODE_TRANS,
		kodetranstabungan.DESKRIPSI_TRANS,
		kodetranstabungan.TYPE_TRANS,
		kodetranstabungan.GL_TRANS

		FROM 
		tabtrans 
		JOIN kodetranstabungan ON 
			kodetranstabungan.KODE_TRANS = tabtrans.KODE_TRANS
		JOIN tabung ON 
			tabung.NO_REKENING = tabtrans.NO_REKENING
		JOIN nasabah ON 
			nasabah.nasabah_id = tabung.NASABAH_ID
		WHERE 1 
		AND tabtrans.NO_REKENING = '".$no_rekening."'
		ORDER BY TABTRANS_ID ASC";

$queryInfo = mysql_query($sql);
$resultInfo = mysql_fetch_array($queryInfo);

$querySimpananPokok = mysql_query($sql);

$querySimpananWajib = mysql_query($sql);

$querySimpananSukarela = mysql_query($sql);

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	/*font-size:10px;
	font-face:courier new;*/
}
.table > tbody > tr > td{
	border-top:0;
	padding:0;
}
.body-print h3{
	margin-top:0;
}
.card-body{
	padding:0;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div class="col-lg-6">

			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="disabled15" class="col-sm-2 control-label">Nama Anggota</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo $resultInfo['nama_nasabah']; ?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="disabled15" class="col-sm-2 control-label">No. Anggota</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo $resultInfo['nasabah_id']; ?></p>
					</div>
				</div>
				<div class="form-group">
					<label for="disabled15" class="col-sm-2 control-label">No. Rekening</label>
					<div class="col-sm-10">
						<p class="form-control-static"><?php echo $resultInfo['NO_REKENING']; ?></p>
					</div>
				</div>
			</form>

		</div>

		<div class="clearfix"></div>

		<div class="col-lg-4">

			<table id="datatable1" class="table table-bordered">

				<thead>
					<tr>
						<th colspan="3" class="text-left"><b>Simpanan Pokok</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td class="text-center"><b>Debet</b></td>
						<td class="text-center"><b>Kredit</b></td>
						<td class="text-center"><b>Saldo</b></td>
					</tr>

					<?php 
					$saldo = 0;
					while($fetch1 = mysql_fetch_array($querySimpananPokok))
					{

						if ($fetch1['DESKRIPSI_TRANS'] == "Simpanan Pokok" || $fetch1['KODE_TRANS'] == 11)
						{
							$debet = ($fetch1['TYPE_TRANS'] == "D") ? $fetch1['SALDO_TRANS'] : 0;
							$kredit = ($fetch1['TYPE_TRANS'] == "K") ? $fetch1['SALDO_TRANS'] : 0;
							$saldo += $kredit - $debet;
					?>

					<tr>
						<td class="text-center"><?php echo number_format($debet,2,'.',','); ?></td>
						<td class="text-center"><?php echo number_format($kredit,2,'.',','); ?></td>
						<td class="text-center"><?php echo number_format($saldo,2,'.',','); ?></td>
					</tr>

					<?php
						}
					}
					?>

				</tbody>
			</table>

		</div>

		<div class="col-lg-4">

			<table id="datatable1" class="table table-bordered">

				<thead>
					<tr>
						<th colspan="3" class="text-left"><b>Simpanan Wajib</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td class="text-center"><b>Debet</b></td>
						<td class="text-center"><b>Kredit</b></td>
						<td class="text-center"><b>Saldo</b></td>
					</tr>

					<?php 
					$saldo = 0;
					while($fetch2 = mysql_fetch_array($querySimpananWajib))
					{

						if ($fetch2['DESKRIPSI_TRANS'] == "Simpanan Wajib" || $fetch2['KODE_TRANS'] == 12 ||
							$fetch2['DESKRIPSI_TRANS'] == "Penarikkan Simpanan Wajib" || $fetch2['KODE_TRANS'] == 14
							)
						{
							$debet = ($fetch2['TYPE_TRANS'] == "D") ? $fetch2['SALDO_TRANS'] : 0;
							$kredit = ($fetch2['TYPE_TRANS'] == "K") ? $fetch2['SALDO_TRANS'] : 0;
							$saldo += $kredit - $debet;
					?>

					<tr>
						<td class="text-center"><?php echo number_format($debet,2,'.',','); ?></td>
						<td class="text-center"><?php echo number_format($kredit,2,'.',','); ?></td>
						<td class="text-center"><?php echo number_format($saldo,2,'.',','); ?></td>
					</tr>

					<?php
						}
					}
					?>


				</tbody>
			</table>

		</div>

		<div class="col-lg-4">

			<table id="datatable1" class="table table-bordered">

				<thead>
					<tr>
						<th colspan="3" class="text-left"><b>Simpanan Sukarela</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td class="text-center"><b>Debet</b></td>
						<td class="text-center"><b>Kredit</b></td>
						<td class="text-center"><b>Saldo</b></td>
					</tr>

					<?php 
					$saldo = 0;
					while($fetch3 = mysql_fetch_array($querySimpananSukarela))
					{

						if (
							$fetch3['DESKRIPSI_TRANS'] == "Adm Penutupan" || $fetch3['KODE_TRANS'] == "10" ||
							$fetch3['DESKRIPSI_TRANS'] == "Pemindadah Buku Kredit" || $fetch3['KODE_TRANS'] == "03" ||
							$fetch3['DESKRIPSI_TRANS'] == "Pemindahan Buku Debet" || $fetch3['KODE_TRANS'] == "04" ||
							$fetch3['DESKRIPSI_TRANS'] == "Pajak" || $fetch3['KODE_TRANS'] == "09" ||
							$fetch3['DESKRIPSI_TRANS'] == "Adm" || $fetch3['KODE_TRANS'] == "08" ||
							$fetch3['DESKRIPSI_TRANS'] == "Bunga" || $fetch3['KODE_TRANS'] == "07" ||
							$fetch3['DESKRIPSI_TRANS'] == "Simpanan Sukarela" || $fetch3['KODE_TRANS'] == "01" ||
							$fetch3['DESKRIPSI_TRANS'] == "Penarikkan Simpanan Sukarela" || $fetch3['KODE_TRANS'] == "13"
						)
						{
							$debet = ($fetch3['TYPE_TRANS'] == "D") ? $fetch3['SALDO_TRANS'] : 0;
							$kredit = ($fetch3['TYPE_TRANS'] == "K") ? $fetch3['SALDO_TRANS'] : 0;
							$saldo += $kredit - $debet;
					?>

					<tr>
						<td class="text-center"><?php echo number_format($debet,2,'.',','); ?></td>
						<td class="text-center"><?php echo number_format($kredit,2,'.',','); ?></td>
						<td class="text-center"><?php echo number_format($saldo,2,'.',','); ?></td>
					</tr>

					<?php
						}
					}
					?>

				</tbody>
			</table>

		</div>

	</div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>