<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "cabang_viewall";

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			<section class="style-default-bright">
				<div class="section-header">
					<h2 class="text-primary">Cabang Data</h2>
				</div>
				<div class="section-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table id="datatable1" class="table table-striped table-hover">
									<thead>
										<tr>
											<th class="sort-numeric">Kode Cabang</th>
											<th class="sort-alpha">Nama Cabang</th>
											<th class="sort-numeric">Kode Cabang Induk</th>
											<th class="sort-alpha">Nama Cabang Induk</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sql = "SELECT * FROM kodecabang WHERE 1";
										$query = mysql_query($sql);
										while($row = mysql_fetch_array($query))
										{
										?>
										<tr>
											<td><a href="cabang_edit.php?kode_cab=<?php echo $row['kode_cab']; ?>" class="btn ink-reaction btn-flat btn-xs btn-primary"><?php echo $row['kode_cab']; ?></a></td>
											<td><?php echo $row['nama_cab']; ?></td>
											<td><?php echo $row['KODE_CAB_INDUK']; ?></td>
											<td><?php echo $row['NAMA_CAB_INDUK']; ?></td>
										</tr>
										<?php 
										}
										?>
									</tbody>
								</table>
							</div>
						</div>	
					</div>	
				</div>
			</section>
		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>	

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>