<?php 
require_once "connect.php";

$status_aktif = (!empty($_GET['status_aktif'])) ? $_GET['status_aktif'] : "";

$where_status_aktif = "";

if ($status_aktif)
{
	$where_status_aktif = "AND STATUS_AKTIF = '".$status_aktif."'";
}

$sql = "SELECT 
		kredit.* ,
		nasabah.NO_PASSPORT ,
		nasabah.alamat
		FROM kredit 
		JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id
		WHERE 1
		".$where_status_aktif."";

$fetch = mysql_query($sql);

$datas = array();

while($row = mysql_fetch_array($fetch))
{
	$datas[] = array(
		'NO_REKENING' => $row['NO_REKENING'],
		'NASABAH_ID' => $row['NASABAH_ID'],
		'NASABAH' => $row['NASABAH'],
		'NO_PASSPORT' => $row['NO_PASSPORT'],
		'alamat' => $row['alamat'],
		'JML_PINJAMAN' => (!empty($row['JML_PINJAMAN'])) ? $row['JML_PINJAMAN'] : 0,
		'JML_BUNGA_PINJAMAN' => (!empty($row['JML_BUNGA_PINJAMAN'])) ? $row['JML_BUNGA_PINJAMAN'] : 0,
		'TGL_REALISASI' => $row['TGL_REALISASI'],
		'JML_ANGSURAN' => (!empty($row['JML_ANGSURAN'])) ? $row['JML_ANGSURAN'] : 0,
		'SUKU_BUNGA_PER_TAHUN' => $row['SUKU_BUNGA_PER_TAHUN'],
		'JENIS_PINJAMAN' => $row['JENIS_PINJAMAN'],
		'TYPE_PINJAMAN' => $row['TYPE_PINJAMAN'],
		'SALDO_NOMINATIF' => (!empty($row['SALDO_NOMINATIF'])) ? $row['SALDO_NOMINATIF'] : 0,

		'angsuran_pokok' => (!empty($row['angsuran_pokok'])) ? $row['angsuran_pokok'] : 0,
		'angsuran_bunga' => (!empty($row['angsuran_bunga'])) ? $row['angsuran_bunga'] : 0,
		'ANGSURAN_ADMIN' => (!empty($row['ANGSURAN_ADMIN'])) ? $row['ANGSURAN_ADMIN'] : 0,
		'angsuran_total' => (!empty($row['angsuran_total'])) ? $row['angsuran_total'] : 0,

		'ADM' => (!empty($row['ADM'])) ? $row['ADM'] : 0,
		'PROVISI' => (!empty($row['PROVISI'])) ? $row['PROVISI'] : 0,

		'value' => $row['NO_REKENING'] . ' - ' . $row['NASABAH_ID'] . ' - ' . $row['NASABAH']
	);
}

echo json_encode($datas);

?>