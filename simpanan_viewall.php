<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "simpanan_viewall";

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			<section class="style-default-bright">
				<div class="section-header">
					<h2 class="text-primary">Simpanan Data</h2>
				</div>
				<div class="section-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table id="datatable1" class="table table-striped table-hover">
									<thead>
										<tr>
											<th class="sort-numeric">No Rekening</th>
											<th class="sort-numeric">No Anggota</th>
											<th class="sort-alpha">Nama Anggota</th>
											<th >Bunga</th>
											<th >PPH</th>
											<th >Tgl Transaksi</th>
											<th >Minimum</th>
											<th >Adm Per Bulan</th>
											<th >Periode Adm</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sqlTabung = "SELECT 
														tabung.* ,
														nasabah.nama_nasabah
														FROM tabung 
														JOIN nasabah ON nasabah.nasabah_id = tabung.NASABAH_ID
														WHERE 1";
										$queryTabung = mysql_query($sqlTabung);
										while($rowTabung = mysql_fetch_array($queryTabung))
										{
										?>
										<tr>
											<td><a href="simpanan_edit.php?no_rekening=<?php echo $rowTabung['NO_REKENING']; ?>" class="btn ink-reaction btn-flat btn-xs btn-primary"><?php echo $rowTabung['NO_REKENING']; ?></a></td>
											<td><?php echo $rowTabung['NASABAH_ID']; ?></td>
											<td><?php echo $rowTabung['nama_nasabah']; ?></td>
											<td><?php echo $rowTabung['SUKU_BUNGA']; ?></td>
											<td><?php echo $rowTabung['PERSEN_PPH']; ?></td>
											<td><?php echo date("d/m/Y", strtotime($rowTabung['TGL_REGISTRASI'])); ?></td>
											<td><?php echo $rowTabung['MINIMUM']; ?></td>
											<td><?php echo $rowTabung['ADM_PER_BLN']; ?></td>
											<td><?php echo $rowTabung['PERIODE_ADM']; ?></td>
										</tr>
										<?php 
										}
										?>
									</tbody>
								</table>
							</div>
						</div>	
					</div>	
				</div>
			</section>
		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>	

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>