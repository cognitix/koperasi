<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "mysysid";

$message = 0;

if (isset($_POST['submit']))
{
	$TANGGALHARIINI = $_POST['TANGGALHARIINI'];

	$sql = "UPDATE mysysid SET 
					value = '".$TANGGALHARIINI."'
					WHERE 1 AND KeyName = 'TANGGALHARIINI'
			";

	mysql_query($sql);

	$message = 1;
}

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem_string = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Mysysid</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data" action="">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										
										<div class="form-group">
											<div class="input-group date">
												<div class="input-group-content">
													<input type="text" class="form-control" id="TANGGALHARIINI" name="TANGGALHARIINI" value="<?php echo $tglsystem; ?>">
													<label>Tanggal mysysid (Tanggal Hari ini)</label>
												</div>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Simpan</button>

						<?php 
						if ($message == 1)
						{
							$linkBack = "mysysid.php";
							require_once "layouts/message_success.php";
						}
						?>

					</form>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script>
$(function(){
	$('#TANGGALHARIINI').datepicker({
		autoclose: true, 
		todayHighlight: true, 
		format: "dd/mm/yyyy"
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>