<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl_transaksi = $_GET['tgl_transaksi'];
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:700px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
.header-text h5, .header-text h3, .header-text h4{
    margin-top:0;
    margin-bottom:0;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div class="header-text">
			<div class="pull-left">
				<h5>Browse Bunga Pajak Deposito</h5>
                <h4 style="margin-bottom:8px;"><?php echo $tgl_transaksi; ?></h4>
			</div>
			<div class="pull-right">

			</div>
		</div>

		<div class="clearfix"></div>

		<?php 
		$tanggal = $_GET['tgl_transaksi'];

		$sql = "
		SELECT
		deposito.* ,
		nasabah.nama_nasabah
		FROM deposito 
		JOIN nasabah ON nasabah.nasabah_id = deposito.NASABAH_ID
		WHERE 1 
		AND DATE_FORMAT(deposito.TGL_REGISTRASI,'%m-%Y') = '".$tanggal."'
		";

		$query = mysql_query($sql);
		?>

		<div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover table-custom">
            	<tr>
                    <th style="text-align:left;">Tanggal Registrasi</th>
                    <th style="text-align:left;">No. Rekening</th>
                    <th style="text-align:left;">Nasabah Id/Nama</th>
                    <th style="text-align:left;">Suku Bunga</th>
                    <th style="text-align:left;">Persen PPh</th>
                    <th style="text-align:left;">Bunga Bulan Ini</th>
                    <th style="text-align:left;">Pajak Bulan Ini</th>
                    <th style="text-align:left;">Jumlah Deposito</th>
                    <th style="text-align:left;">JKW</th>
                    <th style="text-align:left;">Tanggal Jatuh Tempo</th>
                </tr>
                
                <?php 
				while($fetch = mysql_fetch_array($query))
				{
				?>

				<tr>
					<td><?php echo date('d M Y', strtotime($fetch['TGL_REGISTRASI'])); ?></td>
					<td><?php echo $fetch['NO_REKENING']; ?></td>
					<td><?php echo $fetch['NASABAH_ID']; ?> / <?php echo $fetch['nama_nasabah']; ?></td>
					<td><?php echo $fetch['SUKU_BUNGA']; ?></td>
					<td><?php echo $fetch['PERSEN_PPH']; ?></td>
					<td><?php echo number_format($fetch['BUNGA_BLN_INI'],0,'.',','); ?></td>
					<td><?php echo number_format($fetch['PAJAK_BLN_INI'],0,'.',','); ?></td>
					<td><?php echo number_format($fetch['JML_DEPOSITO'],0,'.',','); ?></td>
					<td><?php echo $fetch['JKW']; ?></td>
					<td><?php echo date('d M Y', strtotime($fetch['TGL_JT'])); ?></td>
				</tr>

				<?php
				}
				?>

            </table>
        </div>

        <div class="clearfix"></div>

	</div>
</div>	

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>