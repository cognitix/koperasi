<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "print_buku_simpanan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Print Buku Simpanan</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<?php 

											$nomor_rekening = (!empty($_GET['nomor_rekening'])) ? $_GET['nomor_rekening'] : "";
											$nasabah_id = $nama_nasabah = "";

											if ($nomor_rekening)
											{

												$sql = "SELECT 
														tabung.*,
														nasabah.nama_nasabah ,
														nasabah.alamat
														FROM tabung 
														JOIN nasabah ON tabung.NASABAH_ID = nasabah.nasabah_id
														WHERE 1
														AND NO_REKENING = '".$nomor_rekening."'
														";

												$fetch = mysql_fetch_array( mysql_query($sql) );

												$nasabah_id = $fetch['NASABAH_ID'];
												$nama_nasabah = $fetch['nama_nasabah'];
											}

											?>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" data-source="autosuggest_nomorrekening_all.php" value="<?php echo $nomor_rekening; ?>">
												<label for="nomor_rekening">Nomor Rekening</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nasabah_id" name="nasabah_id" readonly value="<?php echo $nasabah_id; ?>">
												<label for="nasabah_id">Anggota ID</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly value="<?php echo $nama_nasabah; ?>">
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>

					<hr />

					<?php 

					if (!empty($_GET['nomor_rekening']))
					{

						$nomor_rekening = $_GET['nomor_rekening'];

					$sqlTotal = "SELECT 
							tabtrans.*,
							kodetranstabungan.DESKRIPSI_TRANS,
							kodetranstabungan.TYPE_TRANS,
							kodetranstabungan.GL_TRANS
							FROM tabtrans 
							JOIN kodetranstabungan ON 
								tabtrans.KODE_TRANS = kodetranstabungan.KODE_TRANS
							WHERE 1 
							AND NO_REKENING = '".$nomor_rekening."' 
							ORDER BY TABTRANS_ID ASC";
					$queryTotal = mysql_query($sqlTotal);

					$num_rows = mysql_num_rows($queryTotal);


					$totalItems = $num_rows;
					$itemsPerPage = 27;
					$currentPage = (!empty($_GET['page'])) ? $_GET['page'] : 1;
					$urlPattern = '?page=(:num)&nomor_rekening=' . $nomor_rekening . '&nasabah_id=' . $nasabah_id . '&nama_nasabah=' . $nama_nasabah;

					$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);

					$limit1 = ($currentPage - 1) * $itemsPerPage;
					$limit2 = $itemsPerPage;

					$sql = "SELECT 
							tabtrans.*,
							kodetranstabungan.DESKRIPSI_TRANS,
							kodetranstabungan.TYPE_TRANS,
							kodetranstabungan.GL_TRANS
							FROM tabtrans 
							JOIN kodetranstabungan ON 
								tabtrans.KODE_TRANS = kodetranstabungan.KODE_TRANS
							WHERE 1 
							AND NO_REKENING = '".$nomor_rekening."' 
							ORDER BY TABTRANS_ID ASC
							LIMIT ".$limit1.",".$limit2."
							";
					$query = mysql_query($sql);

					?>

					<section class="style-default-bright">

						<form method="post" action="cetak_buku_simpanan.php">

						<div class="row">
							<div class="col-lg-12">

								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Tgl Transaksi</th>
												<th>Kode Transaksi</th>
												<th>Debit</th>
												<th>Kredit</th>
												<th>Saldo</th>
												<th>No. Transaksi</th>
												<th>UserId</th>
												<th>Teller</th>
											</tr>
										</thead>
										<tbody>

											<?php 
											$tabTransIds = array();
											$saldo = 0;
											while($row = mysql_fetch_array($query))
											{
												$debet = ($row['TYPE_TRANS'] == "D") ? $row['SALDO_TRANS'] : 0;
												$kredit = ($row['TYPE_TRANS'] == "K") ? $row['SALDO_TRANS'] : 0;
												$saldo += $kredit - $debet;

												$tabTransIds[] = $row['TABTRANS_ID'];
											?>

											<tr>
												<td>
													<input type="hidden" name="tabTransIds[]" value="<?php echo $row['TABTRANS_ID']; ?>" />
													<?php echo date('d M Y', strtotime($row['TGL_TRANS'])); ?></td>
												<td><?php echo $row['KODE_TRANS'] . ' - ' . $row['DESKRIPSI_TRANS']; ?></td>
												<td><?php echo number_format($debet,2,'.',','); ?></td>
												<td><?php echo number_format($kredit,2,'.',','); ?></td>
												<td><?php echo number_format($saldo,2,'.',','); ?></td>
												<td><?php echo $row['TABTRANS_ID']; ?></td>
												<td><?php echo $row['USERID']; ?></td>
												<td><?php echo $row['NO_TELLER']; ?></td>
											</tr>

											<?php
											}
											?>

										</tbody>
									</table>

									<?php echo $paginator; ?>
								</div>
							</div>
						</div>
						<div class="section-body contain-lg">

							
								<input type="hidden" name="nomor_rekening" value="<?php echo $nomor_rekening; ?>" />

								<input type="submit" name="submit" value="Cetak" />
							
						</div>

						</form>

					</section>

					<?php 
					}
					?>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "print_buku_simpanan.php";
				require_once "layouts/message_success.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#nasabah_id').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>