<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl1 = $_GET['tgl1'];
$tgl2 = $_GET['tgl2'];

$tgl_transaksi_from_string = date("d M Y", strtotime($tgl1));
$tgl_transaksi_to_string = date("d M Y", strtotime($tgl2));

function sisaHasilUsaha($tanggal1Query, $tanggal2Query)
{
	$year1 = date('Y', strtotime($tanggal1Query));
	$year2 = date('Y', strtotime($tanggal2Query));

	$month1 = date('m', strtotime($tanggal1Query));

	if ($month1 == "01")
	{
		$tanggal1Query = ($year1 - 1) .'-12-01';
	}
	else
	{
		$tanggal1Query = ($year1) .'-01-01';
	}

	$periode = $tanggal1Query . ' - ' . $tanggal2Query;

	$sisaHasilUsaha = 0;

	$total=mysql_fetch_array(mysql_query("SELECT 
										sum(debet) as debet,
										sum(kredit) as kredit
										FROM trans_detail 
										JOIN trans_master ON 
								 			trans_detail.master_id = trans_master.trans_id
								 		JOIN perkiraan ON
								 			perkiraan.kode_perk = trans_detail.kode_perk
										where 1 
										
										AND (LEFT(perkiraan.kode_perk, 2) = '40' OR 
											LEFT(perkiraan.kode_perk, 1) = '4' OR 
											LEFT(perkiraan.kode_perk, 2) = '50' OR 
											LEFT(perkiraan.kode_perk, 1) = '5')

										AND DATE_FORMAT(trans_master.tgl_trans, '%Y-%m')  
										BETWEEN '".$tanggal1Query."' AND '".$tanggal2Query."'
										GROUP BY perkiraan.kode_perk
										"));

	$sisaTotalPendapatan = $total['debet'] - $total['kredit'];

	$total=mysql_fetch_array(mysql_query("SELECT 
										sum(debet) as debet,
										sum(kredit) as kredit
										FROM trans_detail 
										JOIN trans_master ON 
								 			trans_detail.master_id = trans_master.trans_id
								 		JOIN perkiraan ON
								 			perkiraan.kode_perk = trans_detail.kode_perk
										where 1 
										
										AND (LEFT(perkiraan.kode_perk, 2) = '50' OR 
											LEFT(perkiraan.kode_perk, 1) = '5')

										AND DATE_FORMAT(trans_master.tgl_trans, '%Y-%m')  
										BETWEEN '".$tanggal1Query."' AND '".$tanggal2Query."'
										GROUP BY perkiraan.kode_perk
										"));

	$sisaTotalBiaya = $total['debet'] - $total['kredit'];

	$sisaHasilUsaha += $sisaTotalPendapatan - $sisaTotalBiaya;

	return array('saldo' => $sisaHasilUsaha, 'periode' => $periode);
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:1100px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
.header-text h5, .header-text h3, .header-text h4{
    margin-top:0;
    margin-bottom:0;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div class="header-text">
			<div class="pull-left">
				<h5>Koperasi Simpan Pinjam</h5>
                <h4 style="margin-bottom:8px;">KSP ADIL MAKMUR FAJAR</h4>
			</div>
			<div class="pull-right">
				<h4>Trial Balance</h4>
                <h5 style="margin-bottom:8px;">Periode from <?php echo $tgl_transaksi_from_string; ?> to <?php echo $tgl_transaksi_to_string; ?></h5>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="table-responsive">
			<div style="float:left;width:500px;">
	            <table id="datatable1" class="table table-striped table-hover table-bordered">
	            	<tr>
	                    <th style="text-align:left;">Aktiva</th>
	                    <th style="text-align:left;">Rupiah</th>
	                </tr>

	                <?php 
	                	$tgl1 = $_GET['tgl1'];
	                	$totalAktiva = 0;
	                ?>

	                <tr>
						<td style="font-weight:bold;">1. Harta</td>
						<td>
						</td>
					</tr>

	                <?php

	                	// ======================== HARTA

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 2) = '10'
						AND LEFT(perkiraan.kode_perk, 1) = '1'
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$total = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$sqlBefore = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								 trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";
							$queryBefore = mysql_query($sqlBefore);

							$saldoAwal = 0;
							while($resultBefore = mysql_fetch_array($queryBefore))
							{
								$debet = $resultBefore['debet'];

								$kredit = $resultBefore['kredit'];

								$saldoAwal += $debet - $kredit;
							}

							$sql = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";

							$query = mysql_query($sql);

							$saldo = $saldoAwal;
							while($result1 = mysql_fetch_array($query))
							{
								$debet = $result1['debet'];

								$kredit = $result1['kredit'];

								$saldo += $debet - $kredit;
							}

							if (! $saldo && $results['level'] >= 3) continue;

							$bold = ($results['level'] <= 2) ? 'font-weight:bold;' : '';

							$total += $saldo;
						?>

						<tr>
							<td style="<?php echo $bold; ?>">
								<?php echo $results['nama_perk']; ?></td>
							<td>
								<?php 
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo ((int)$saldo != 0) ? number_format($saldo,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

						$totalAktiva += $total;
	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Harta</td>
							<td>
								<?php 
									if ($total < 0)
									{
										echo "( ".number_format(abs($total),2,'.',',')." )";
									}
									else
									{
										echo ((int)$total != 0) ? number_format($total,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== HARTA : END
						?>

						<tr>
							<td style="font-weight:bold;"></td>
							<td>
							</td>
						</tr>

						<?php

						// ======================== PIUTANG

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 3) = '120'
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$total = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$sqlBefore = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								 trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";
							$queryBefore = mysql_query($sqlBefore);

							$saldoAwal = 0;
							while($resultBefore = mysql_fetch_array($queryBefore))
							{
								$debet = $resultBefore['debet'];

								$kredit = $resultBefore['kredit'];

								$saldoAwal += $debet - $kredit;
							}

							$sql = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";

							$query = mysql_query($sql);

							$saldo = $saldoAwal;
							while($result1 = mysql_fetch_array($query))
							{
								$debet = $result1['debet'];

								$kredit = $result1['kredit'];

								$saldo += $debet - $kredit;
							}

							if (! $saldo && $results['level'] >= 3) continue;

							$bold = ($results['level'] <= 2) ? 'font-weight:bold;' : '';

							$total += $saldo;
						?>

						<tr>
							<td style="<?php echo $bold; ?>">
								<?php echo ($c == 0) ? '2. ' : ''; ?>
								<?php echo $results['nama_perk']; ?>
							</td>
							<td>
								<?php 
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo ((int)$saldo != 0) ? number_format($saldo,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

						$totalAktiva += $total;
	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Piutang</td>
							<td>
								<?php 
									if ($total < 0)
									{
										echo "( ".number_format(abs($total),2,'.',',')." )";
									}
									else
									{
										echo ((int)$total != 0) ? number_format($total,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== PIUTANG : END
						?>

						<tr>
							<td style="font-weight:bold;">Total Aktiva</td>
							<td>
								<?php 
									if ($totalAktiva < 0)
									{
										echo "( ".number_format(abs($totalAktiva),2,'.',',')." )";
									}
									else
									{
										echo ((int)$totalAktiva != 0) ? number_format($totalAktiva,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>


	            </table>
	        </div>


	        <?php 
			// ================================================================================================
			?>


	        <div style="float:right;width:500px;">

	        	<table id="datatable1" class="table table-striped table-hover table-bordered">
	            	<tr>
	                    <th style="text-align:left;">Pasiva</th>
	                    <th style="text-align:left;">Rupiah</th>
	                </tr>
	                <tr>
						<td style="font-weight:bold;">4. Kewajiban</td>
						<td>
						</td>
					</tr>

					<?php

						$totalPasiva = 0;

	                	// ======================== KEWAJIBAN

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 2) = '20' AND LEFT(perkiraan.kode_perk, 1) = '2'
						OR (perkiraan.kode_perk = '3011' OR perkiraan.kode_perk = '3012')
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$total = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$sqlBefore = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								 trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";
							$queryBefore = mysql_query($sqlBefore);

							$saldoAwal = 0;
							while($resultBefore = mysql_fetch_array($queryBefore))
							{
								$debet = $resultBefore['debet'];

								$kredit = $resultBefore['kredit'];

								$saldoAwal += $debet - $kredit;
							}

							$sql = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";

							$query = mysql_query($sql);

							$saldo = $saldoAwal;
							while($result1 = mysql_fetch_array($query))
							{
								$debet = $result1['debet'];

								$kredit = $result1['kredit'];

								$saldo += $debet - $kredit;
							}

							if (! $saldo && $results['level'] >= 3) continue;

							$bold = ($results['level'] <= 2) ? 'font-weight:bold;' : '';

							$total += $saldo;
						?>

						<tr>
							<td style="<?php echo $bold; ?>">
								<?php echo $results['nama_perk']; ?></td>
							<td>
								<?php 
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo ((int)$saldo != 0) ? number_format($saldo,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

						$totalPasiva += $total;
	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Kewajiban</td>
							<td>
								<?php 
									if ($total < 0)
									{
										echo "( ".number_format(abs($total),2,'.',',')." )";
									}
									else
									{
										echo ((int)$total != 0) ? number_format($total,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== Kewajiban : END
						?>

						<tr>
							<td style="font-weight:bold;"></td>
							<td>
							</td>
						</tr>

						<tr>
							<td style="font-weight:bold;">5. Modal</td>
							<td>
							</td>
						</tr>

						<?php

	                	// ======================== MODAL

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 2) = '30'
						AND LEFT(perkiraan.kode_perk, 1) = '3'
						AND (perkiraan.kode_perk != '3011' AND perkiraan.kode_perk != '3012')
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$total = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$sqlBefore = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								 trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";
							$queryBefore = mysql_query($sqlBefore);

							$saldoAwal = 0;
							while($resultBefore = mysql_fetch_array($queryBefore))
							{
								$debet = $resultBefore['debet'];

								$kredit = $resultBefore['kredit'];

								$saldoAwal += $debet - $kredit;
							}

							$sql = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";

							$query = mysql_query($sql);

							$saldo = $saldoAwal;
							while($result1 = mysql_fetch_array($query))
							{
								$debet = $result1['debet'];

								$kredit = $result1['kredit'];

								$saldo += $debet - $kredit;
							}

							if (! $saldo && $results['level'] >= 3) continue;

							$bold = ($results['level'] <= 2) ? 'font-weight:bold;' : '';

							$total += $saldo;
						?>

						<tr>
							<td style="<?php echo $bold; ?>">
								<?php echo $results['nama_perk']; ?></td>
							<td>
								<?php 
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo ((int)$saldo != 0) ? number_format($saldo,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

						$totalPasiva += $total;
	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Modal</td>
							<td>
								<?php 
									if ($total < 0)
									{
										echo "( ".number_format(abs($total),2,'.',',')." )";
									}
									else
									{
										echo ((int)$total != 0) ? number_format($total,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== MODAL : END
						?>

						<tr>
							<td style="font-weight:bold;"></td>
							<td>
							</td>
						</tr>

						<?php

	                	// ======================== LABA/RUGI

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 3) = '310'
						AND level != 4
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$total = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$tanggal1Query1 = date("Y-m-d", strtotime($tgl1));
							$tanggal2Query1 = date("Y-m-d", strtotime($tgl2));

							//$tanggal2Query1 = date("Y-m-d", strtotime($tgl1));
							$tanggal3Query1 = date("Y", strtotime($tgl1)) - 1;
							$tanggal3Query1_m = 12;

							$saldo = 0;
							$periode = "";

							if ($results['kode_perk'] == "31001")
							{

								$a = sisaHasilUsaha($tanggal3Query1."-02", $tanggal3Query1."-".$tanggal3Query1_m."-31");
								$saldo = $a['saldo'];
								$periode = $a['periode'];

							}
							else if ($results['kode_perk'] == "31002")
							{

								$a = sisaHasilUsaha($tanggal1Query1, $tanggal2Query1);
								$saldo = $a['saldo'];
								$periode = $a['periode'];

							}

							$total += $saldo;
						?>

						<tr>
							<td style="<?php echo $bold; ?>">
								<?php echo ($c == 0) ? '6. ' : ''; ?>
								<?php echo $results['nama_perk']; ?><br />
								<?php if ($periode) { ?>Periode: <?php echo $periode; ?><?php } ?>
							</td>
							<td>
								<?php 
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo ((int)$saldo != 0) ? number_format($saldo,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

						$totalPasiva += $total;
	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Laba/Rugi</td>
							<td>
								<?php 
									if ($total < 0)
									{
										echo "( ".number_format(abs($total),2,'.',',')." )";
									}
									else
									{
										echo ((int)$total != 0) ? number_format($total,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== LABA/RUGI : END
						?>

						<tr>
							<td style="font-weight:bold;">Total Pasiva</td>
							<td>
								<?php 
									if ($totalPasiva < 0)
									{
										echo "( ".number_format(abs($totalPasiva),2,'.',',')." )";
									}
									else
									{
										echo ((int)$totalPasiva != 0) ? number_format($totalPasiva,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

	        	</table>

	        </div>
        </div>

        <div class="clearfix"></div>

		<div style="margin:0 auto;">
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Menyetujui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:98px;text-align:center;">Agustina</div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    KBO
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Mengetahui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:98px;text-align:center;">Lily Njomin</div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Pengawas
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">
									<b>Jakarta Utara, <?php echo date("d M Y"); ?></b><br />
									KSP ADIL MAKMUR FAJAR<br />
									Dibuat
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Staff Akutansi
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>

	</div>
</div>	

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>