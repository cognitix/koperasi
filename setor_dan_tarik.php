<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

define ("MAX_SIZE", "5000");

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "setor_dan_tarik";

//print_r($user);

$sqlUser = "SELECT * FROM passwd WHERE 1 AND USERID = '".$user['USERID']."'";
$fetchUser = mysql_fetch_array(mysql_query($sqlUser));


/*
function getExtension($str) 
{
	$i = strrpos($str,".");
	if (!$i) { return ""; } 
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}


function upload($field_name, $folder_name)
{
	$filenamePhoto = "";
	$errors = 0;

	if (!empty($_FILES[$field_name]["name"]))
	{
		$photo 			= $_FILES[$field_name]["name"];
		$uploadedFile 	= $_FILES[$field_name]['tmp_name'];

	  	$filename = stripslashes($photo);
	   	$extension = getExtension($filename);
	  	$extension = strtolower($extension);

	  	if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
	  	{
			echo ' Unknown Image extension ';
			$errors = 1;
	  	}
	  	else
		{
	   		$size = filesize($uploadedFile);
	   		if ($size > MAX_SIZE * 1024)
			{
				echo "You have exceeded the size limit";
			 	$errors = 1;
			}
	   	}

	   	if ($errors == 0)
	   	{
	   		if($extension == "jpg" || $extension == "jpeg")
			{
				$src = imagecreatefromjpeg($uploadedFile);
			}
			else if($extension=="png")
			{
				$src = imagecreatefrompng($uploadedFile);
			}
			else 
			{
				$src = imagecreatefromgif($uploadedFile);
			}

			list($width, $height) = getimagesize($uploadedFile);

			$newwidth = $width;
			$newheight = $height;

			if ($width >= 500)
			{
				$newwidth = 500;
				$newheight = ($height / $width) * $newwidth;
			}
			
			$tmp = imagecreatetruecolor($newwidth, $newheight);

			imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

			$filenamePhoto = "uploads/".$folder_name."/". sha1($photo . time()) . '.png';

			imagejpeg($tmp,$filenamePhoto,100);

			imagedestroy($src);
			imagedestroy($tmp);
	   	}
	}

	return array('filenamePhoto' => $filenamePhoto, 'errors' => $errors);
}
*/

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{

	$tanggal_input = $_POST['tanggal_input'];
	$tanggal_valuta = $_POST['tanggal_valuta'];
	$nomor_rekening = $_POST['nomor_rekening'];
	$nasabah_id = $_POST['nasabah_id'];
	$nama_nasabah = $_POST['nama_nasabah'];
	$alamat = $_POST['alamat'];
	$kode_trans = $_POST['kode_trans'];
	$jumlah = $_POST['jumlah'];
	$kuitansi = $_POST['kuitansi'];
	$keterangan = $_POST['keterangan'];
	$SALDO_NOMINATIF = $_POST['SALDO_NOMINATIF'];

	$limittarik = $fetchUser['LIMIT_TARIK'];
	$limitsetor = $fetchUser['LIMIT_SETOR'];

	$username = $_POST['username'];
	$password = $_POST['password'];
	$password = sha1($password . $salt);

	$isLimitSetor = $isLimitTarik = false;

	if ($kode_trans == "01" || $kode_trans == "11" || $kode_trans == "12")
	{
		$isLimitSetor = true;
	}
	else if ($kode_trans == "02" || $kode_trans == "13" || $kode_trans == "14")
	{
		$isLimitTarik = true;
	}

	if ( ($isLimitSetor && $jumlah > $limitsetor) || ($isLimitTarik && $jumlah > $limittarik) )
	{
		$sql = "SELECT * FROM passwd WHERE 1 AND USERNAME = '$username' AND PASSWORD = '$password'";
		$fetchUser = mysql_fetch_array(mysql_query($sql));

		if (empty($fetchUser))
		{
			echo 'Approval Error . <a href="setor_dan_tarik.php">Try Again</a>';
			die();
		}

	}

	$tarik = array("02", "13", "14");

	$MY_KODE_TRANS = (in_array($kode_trans, $tarik)) ? '300' : '200';
	//echo $MY_KODE_TRANS;die();

	//$upload_PATH_TTANGAN = upload("PATH_TTANGAN", "tandatangan");

	$tgl_trans = $tanggal_input; //$tglsystem; //date('Y-m-d');
	$userId = $user['USERID'];
	$cab = $kodecabang['kode_cab'];

	$sqlTabung = "SELECT * FROM tabung WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
	$fetchTabung = mysql_fetch_array(mysql_query($sqlTabung));

	if (!empty($fetchTabung['NO_REKENING']))
	{
		/*$filename_PATH_TTANGAN = NULL;

		if ($upload_PATH_TTANGAN['errors'] == 0)
		{
			$filename_PATH_TTANGAN = $upload_PATH_TTANGAN['filenamePhoto'];
		}*/

		$sqlKodeTrans = "SELECT * FROM kodetranstabungan WHERE 1 AND KODE_TRANS = '".$kode_trans."'";
		$fetchKodeTrans = mysql_fetch_array(mysql_query($sqlKodeTrans));
		$uraian = $fetchKodeTrans['DESKRIPSI_TRANS'] . " : " . $nomor_rekening . " - " . $nama_nasabah;

		// insert tabtrans
		$sql = "INSERT INTO tabtrans SET 
				TGL_TRANS = '".$tgl_trans."' ,
				NO_REKENING = '".$nomor_rekening."' ,
				KODE_TRANS = '".$kode_trans."' ,
				SALDO_TRANS = '".$jumlah."' ,
				kuitansi = '".$kuitansi."' ,
				USERID = '".$userId."' ,
				NO_TELLER = '".$userId."' ,
				keterangan = '".$keterangan."' ,
				FLAG_CETAK = 'N' ,
				TGL_INPUT = '".$tanggal_input."' ,
				MY_KODE_TRANS = '".$MY_KODE_TRANS."' ,
				KODE_PERK_GL = '".$fetchKodeTrans['GL_TRANS']."' ,
				TOB = '".$fetchKodeTrans['TOB']."' ,
				tob_RAK = '".$fetchKodeTrans['TOB']."' ,
				CAB = '".$cab."'
		";

		mysql_query($sql);

		$tabTransId = mysql_insert_id();

		// insert tellertrans
		$sql = "INSERT INTO tellertrans SET 
				modul = 'TAB' ,
				tgl_trans = '".$tgl_trans."' ,
				NO_BUKTI = '".$kuitansi."' ,
				uraian = '".$uraian."' ,
				saldo_trans = '".$jumlah."' ,
				userid = '".$userId."' ,
				my_kode_trans = '".$MY_KODE_TRANS."' ,
				GL_TRANS = '".$fetchKodeTrans['GL_TRANS']."' ,
				TOB = '".$fetchKodeTrans['TOB']."' ,
				tob_RAK = '".$fetchKodeTrans['TOB']."' ,
				cab = '".$cab."'
		";

		mysql_query($sql);

		if ((int) $fetchTabung['SALDO_AWAL'] == 0 && $fetchKodeTrans['KODE_TRANS'] == "01")
		{
			$sql = "UPDATE tabung SET 
					SALDO_AWAL = '".$jumlah."' ,
					SALDO_SETORAN = '".$jumlah."' ,
					SALDO_NOMINATIF = '".$SALDO_NOMINATIF."' ,
					simpanan_sukarela = '".$jumlah."' ,
					STATUS_AKTIF = 2
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			"; // PATH_TTANGAN = '".$filename_PATH_TTANGAN."'
		}
		else if ($fetchKodeTrans['KODE_TRANS'] == "01")
		{
			$SALDO_SETORAN = $fetchTabung['SALDO_SETORAN'] + $jumlah;
			$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] + $jumlah;
			$simpanan_sukarela = $fetchTabung['simpanan_sukarela'] + $jumlah;

			$sql = "UPDATE tabung SET 
					SALDO_SETORAN = '".$SALDO_SETORAN."' ,
					SALDO_NOMINATIF = '".$SALDO_NOMINATIF."' ,
					simpanan_sukarela = '".$simpanan_sukarela."' ,
					STATUS_AKTIF = 2 
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			"; // PATH_TTANGAN = '".$filename_PATH_TTANGAN."'
		}
		else if ($fetchKodeTrans['KODE_TRANS'] == "11")
		{
			//$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] + $jumlah;
			$simpanan_pokok = $fetchTabung['simpanan_pokok'] + $jumlah;

			$sql = "UPDATE tabung SET 
					simpanan_pokok = '".$simpanan_pokok."' ,
					STATUS_AKTIF = 2
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			"; // PATH_TTANGAN = '".$filename_PATH_TTANGAN."'
		}
		else if ($fetchKodeTrans['KODE_TRANS'] == "12")
		{
			//$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] + $jumlah;
			$simpanan_wajib = $fetchTabung['simpanan_wajib'] + $jumlah;

			$sql = "UPDATE tabung SET 
					simpanan_wajib = '".$simpanan_wajib."' ,
					STATUS_AKTIF = 2
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			"; // PATH_TTANGAN = '".$filename_PATH_TTANGAN."'
		}
		else if ($fetchKodeTrans['KODE_TRANS'] == "13")
		{
			$SALDO_SETORAN = $fetchTabung['SALDO_SETORAN'] - $jumlah;
			$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] - $jumlah;
			$simpanan_sukarela = $fetchTabung['simpanan_sukarela'] - $jumlah;

			$sql = "UPDATE tabung SET 
					SALDO_SETORAN = '".$SALDO_SETORAN."' ,
					SALDO_NOMINATIF = '".$SALDO_NOMINATIF."' ,
					simpanan_sukarela = '".$simpanan_sukarela."' ,
					STATUS_AKTIF = 2 
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			"; // PATH_TTANGAN = '".$filename_PATH_TTANGAN."'
		}
		else if ($fetchKodeTrans['KODE_TRANS'] == "14")
		{
			//$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] - $jumlah;
			$simpanan_wajib = $fetchTabung['simpanan_wajib'] - $jumlah;

			$sql = "UPDATE tabung SET 
					simpanan_wajib = '".$simpanan_wajib."' ,
					STATUS_AKTIF = 2
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			"; // PATH_TTANGAN = '".$filename_PATH_TTANGAN."'
		}

		mysql_query($sql);

		$message = 1;

	}
	else
	{
		$message = 2;
	}

}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Setor dan Tarik</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="tanggal_input" name="tanggal_input" value="<?php echo $tglsystem; ?>" 
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?>
												/>
												<label for="tanggal_input">Tanggal Input</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="tanggal_valuta" name="tanggal_valuta" value="<?php echo $tglsystem; ?>" 
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?> 
												/>
												<label for="tanggal_valuta">Tanggal Valuta</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" data-source="autosuggest_nomorrekening.php">
												<label for="nomor_rekening">Nomor Rekening</label>
											</div>

											<div class="form-group floating-label">
												<select id="kode_trans" name="kode_trans" class="form-control">
													<option value="">&nbsp;</option>
													<?php 
													$sqlKodeTrans = "SELECT * FROM kodetranstabungan WHERE 1 ORDER BY KODE_TRANS ASC";
													$fetchKodeTrans = mysql_query($sqlKodeTrans);
													while($rowKodeTrans = mysql_fetch_array($fetchKodeTrans))
													{
													?>
													<option value="<?php echo $rowKodeTrans['KODE_TRANS']; ?>"><?php echo $rowKodeTrans['KODE_TRANS']; ?> - <?php echo $rowKodeTrans['DESKRIPSI_TRANS']; ?></option>
													<?php
													}
													?>
												</select>
												<label for="jenis_nasabah">Kode Transaksi</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="jumlah" name="jumlah">
												<label for="jumlah">Jumlah / Saldo Transfer</label>
											</div>

											<div class="form-group floating-label">
												<?php 
												$sqlTabTrans = "SELECT TABTRANS_ID FROM tabtrans WHERE 1 ORDER by TABTRANS_ID desc limit 1";
												$fetchTabTrans = mysql_fetch_array(mysql_query($sqlTabTrans));
												$idMax = (int) $fetchTabTrans['TABTRANS_ID'];
												$idMax++;
												$kuitansi = "T-" . sprintf("%06s", $idMax);
												?>
												<input type="text" class="form-control" id="kuitansi" name="kuitansi" value="<?php echo $kuitansi; ?>" readonly>
												<label for="kuitansi">Kuitansi</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="keterangan" name="keterangan">
												<label for="keterangan">Keterangan</label>
											</div>

									</div>
								</div>

							</div>

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group ">
												<input type="text" class="form-control" id="nasabah_id" name="nasabah_id" readonly>
												<label for="nasabah_id">Anggota ID</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly>
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="alamat" name="alamat" readonly>
												<label for="alamat">Alamat</label>
											</div>

											<div class="form-group" id="PATH_TTANGAN">
												<label for="PATH_TTANGAN">Tanda Tangan</label>
												<div style="margin-top:10px;">-</div>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="SALDO_NOMINATIF" name="SALDO_NOMINATIF" readonly>
												<label for="SALDO_NOMINATIF">Total Saldo Nominatif</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="simpanan_pokok" name="simpanan_pokok" readonly>
												<label for="simpanan_pokok">Simpanan Pokok</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="simpanan_wajib" name="simpanan_wajib" readonly>
												<label for="simpanan_wajib">Simpanan Wajib</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="simpanan_sukarela" name="simpanan_sukarela" readonly>
												<label for="simpanan_sukarela">Simpanan Sukarela</label>
											</div>

									</div>
								</div>

							</div>	

						</div>

						<div class="row" id="formLogin" style="display:none;">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group ">
											<input type="text" class="form-control" id="username" name="username" />
											<label for="username">Username</label>
										</div>

										<div class="form-group ">
											<input type="password" class="form-control" id="password" name="password" />
											<label for="password">Password</label>
										</div>

									</div>
								</div>

							</div>
						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit" id="submit"
						data-limittarik="<?php echo $fetchUser['LIMIT_TARIK']; ?>"
						data-limitsetor="<?php echo $fetchUser['LIMIT_SETOR']; ?>"
						>Submit</button>

					</form>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "setor_dan_tarik.php";
				require_once "layouts/message_success.php";
			?>
				<section>
					<div class="section-body contain-lg">
						<a href="print_validasi.php?tabTransId=<?php echo $tabTransId; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print Validasi</a>
						<a href="cetak_validasi.php?tabTransId=<?php echo $tabTransId; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Cetak Validasi</a>
					</div>
				</section>
			<?php
			}
			else if ($message == 2)
			{
				$linkBack = "setor_dan_tarik.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){

	var approval = false;

	$('#submit').click(function(){

		var kode_trans = $('#kode_trans').val();

		var isLimitSetor = isLimitTarik = false;

		if (kode_trans == "01" || kode_trans == "11" || kode_trans == "12")
		{
			isLimitSetor = true;
		}
		else if (kode_trans == "02" || kode_trans == "13" || kode_trans == "14")
		{
			isLimitTarik = true;
		}

		var jumlah = parseInt($('#jumlah').val());
		var limitsetor = parseInt($(this).attr('data-limitsetor'));
		var limittarik = parseInt($(this).attr('data-limittarik'));

		if (! approval)
		{

			if ( (isLimitSetor && jumlah > limitsetor) || (isLimitTarik && jumlah > limittarik) )
			{
				$('#formLogin').show();
				approval = true;
				return false;
			}
		
		}

		//return false;
	});

	$('#tanggal_input, #tanggal_valuta').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#SALDO_NOMINATIF').val(ui.item.SALDO_NOMINATIF);
					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#nasabah_id').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);
					$('#alamat').val(ui.item.alamat);

					$('#simpanan_pokok').val(ui.item.simpanan_pokok);
					$('#simpanan_sukarela').val(ui.item.simpanan_sukarela);
					$('#simpanan_wajib').val(ui.item.simpanan_wajib);

					var img = $('<img />', {
						src : ui.item.PATH_TTANGAN ,
						width : 200 ,
					});
					$('#PATH_TTANGAN').find('div').html(img);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>