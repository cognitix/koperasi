<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "akutansi_jurnal_umum";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['simpan']))
{
	$nomor_bukti = $_POST['nomor_bukti'];
	$tanggal_transaksi = $_POST['tanggal_transaksi'];
	$jumlah = $_POST['jumlah'];
	$keterangan = $_POST['keterangan'];
	$kode_perkiraan = $_POST['kode_perkiraan'];
	$nama_perkiraan = $_POST['nama_perkiraan'];
	$posisi = $_POST['posisi'];
	$kode_jurnal = "JU";
	$src = "GL";

	$master_id = (!empty($_SESSION['master_id'])) ? $_SESSION['master_id'] : "";

	$_SESSION['keterangan'] = $keterangan;
	$_SESSION['nomor_bukti'] = $nomor_bukti;

	// insert ke table trans master
	if ($posisi == "debet")
	{

		$sqlTransMaster = "
		SELECT * FROM trans_master WHERE 1 AND no_bukti = '".$nomor_bukti."'
		";
		$queryTransMaster = mysql_query($sqlTransMaster);
		$fetchTransMaster = mysql_fetch_array($queryTransMaster);

		if (! $fetchTransMaster)
		{
			$sqlInsertTransMaster = "
			INSERT INTO trans_master SET 
			tgl_trans = '".$tanggal_transaksi."' ,
			kode_jurnal = '".$kode_jurnal."' ,
			no_bukti = '".$nomor_bukti."' ,
			src = '".$src."' ,
			NOMINAL = '".$jumlah."' ,
			KETERANGAN = '".$keterangan."'
			";

			mysql_query($sqlInsertTransMaster);

			$master_id = mysql_insert_id();
		}
		else
		{
			$nominal = $fetchTransMaster['NOMINAL'] + $jumlah;

			$sqlInsertTransMaster = "
			UPDATE trans_master SET 
			tgl_trans = '".$tanggal_transaksi."' ,
			kode_jurnal = '".$kode_jurnal."' ,
			no_bukti = '".$nomor_bukti."' ,
			src = '".$src."' ,
			NOMINAL = '".$nominal."' ,
			KETERANGAN = '".$keterangan."'
			WHERE 1
			AND trans_id = '".$fetchTransMaster['trans_id']."'
			";

			mysql_query($sqlInsertTransMaster);

			$master_id = $fetchTransMaster['trans_id'];
		}

		$_SESSION['master_id'] = $master_id;

	}

	if ($master_id)
	{
		$saldo_akhir = 0;

		// perkiraan
		$sqlPerkiraan = "
		SELECT * FROM perkiraan WHERE 1 AND kode_perk = '".$kode_perkiraan."'
		";
		$queryPerkiraan = mysql_query($sqlPerkiraan);
		$fetchPerkiraan = mysql_fetch_array($queryPerkiraan);

		if ($posisi == "debet")
		{
			$debet = $jumlah;
			$kredit = 0;
		}
		else
		{
			$debet = 0;
			$kredit = $jumlah;
		}

		$saldo_akhir = $fetchPerkiraan['saldo_akhir'] + $debet - $kredit;

		$sqlTransDetail = "
		INSERT INTO trans_detail SET 
		master_id = '".$master_id."' ,
		URAIAN = '".$keterangan."' ,
		kode_perk = '".$kode_perkiraan."' ,
		".$posisi." = '".$jumlah."' ,
		saldo_akhir = '".$saldo_akhir."'
		";

		//echo $sqlTransDetail;

		mysql_query($sqlTransDetail);
	}
}

$master_id_temp = null;

if (isset($_POST['selesai']))
{
	$nomor_bukti = (!empty($_SESSION['nomor_bukti'])) ? $_SESSION['nomor_bukti'] : "";
	$keterangan = (!empty($_SESSION['keterangan'])) ? $_SESSION['keterangan'] : "";
	$master_id = (!empty($_SESSION['master_id'])) ? $_SESSION['master_id'] : "";

	$sqlTransDetail = "
	SELECT 
	trans_detail.* ,
	
	perkiraan.kode_perk ,
	perkiraan.nama_perk ,
	perkiraan.saldo_debet ,
	perkiraan.saldo_kredit

	FROM trans_detail 
	JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
	WHERE 1 
	AND master_id = '".$master_id."'
	";

	$queryTransDetail = mysql_query($sqlTransDetail);

	while($fetchTransDetail = mysql_fetch_array($queryTransDetail))
	{
		$saldo_debet = $fetchTransDetail['saldo_debet'] + $fetchTransDetail['debet'] - $fetchTransDetail['kredit'];
		$saldo_kredit = $fetchTransDetail['saldo_kredit'] + $fetchTransDetail['debet'] - $fetchTransDetail['kredit'];
		$saldo_akhir = $saldo_debet - $saldo_kredit;

		$kode_perk = $fetchTransDetail['kode_perk'];

		$sql = "
		UPDATE perkiraan SET 
		saldo_debet = '".$saldo_debet."' ,
		saldo_kredit = '".$saldo_kredit."' ,
		saldo_akhir = '".$saldo_akhir."'
		WHERE 1
		AND kode_perk = '".$kode_perk."'
		";

		//echo $sql;

		mysql_query($sql);
	}

	$master_id_temp = $master_id;

	unset($_SESSION['nomor_bukti']);
	unset($_SESSION['keterangan']);
	unset($_SESSION['master_id']);

	$message = 1;
}

$nomor_bukti = (!empty($_SESSION['nomor_bukti'])) ? $_SESSION['nomor_bukti'] : "";
$keterangan = (!empty($_SESSION['keterangan'])) ? $_SESSION['keterangan'] : "";
$master_id = (!empty($_SESSION['master_id'])) ? $_SESSION['master_id'] : "";
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
				$linkBack = "akutansi_jurnal_umum.php";
				require_once "layouts/message_success.php";
			?>

			<section>
				<div class="section-body contain-lg">
					<a href="print_akutansi_jurnal_umum.php?master_id=<?php echo $master_id_temp; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print Akutansi Jurnal Umum</a>
				</div>
			</section>

			<?php
			}
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Jurnal Umum</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group">
											<?php 
											if (! $nomor_bukti)
											{
												$sqlTransMaster = "SELECT no_bukti FROM trans_master WHERE 1 AND kode_jurnal = 'JU' ORDER by trans_id desc limit 1";
												$fetchTabTrans = mysql_fetch_array(mysql_query($sqlTransMaster));
												//$idMax = (int) str_replace("JU-", "", $fetchTabTrans['no_bukti']);
												$idMax = explode("-", $fetchTabTrans['no_bukti']);
												$idMax = end($idMax);
												$idMax++;
												$nomor_bukti = "JU-" . date("Ymd") . "-" . sprintf("%07s", $idMax);
											}
											?>
											<input type="text" class="form-control" id="nomor_bukti" name="nomor_bukti" value="<?php echo $nomor_bukti; ?>">
											<label for="nomor_bukti">Nomor Bukti</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="tanggal_transaksi" name="tanggal_transaksi" value="<?php echo $tglsystem; ?>" />
											<label for="tanggal_transaksi">Tanggal Transaksi</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="jumlah" name="jumlah">
											<label for="jumlah">Jumlah</label>
										</div>

										<div class="form-group ">
											<input type="text" class="form-control" id="keterangan" name="keterangan" value="<?php echo $keterangan; ?>">
											<label for="keterangan">Keterangan</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="kode_perkiraan" name="kode_perkiraan" data-source="autosuggest_gltrans.php">
											<label for="kode_perkiraan">Kode Perkiraan</label>
										</div>

										<div class="form-group">
											<input type="text" class="form-control" id="nama_perkiraan" name="nama_perkiraan">
											<label for="nama_perkiraan">Nama Perkiraan</label>
										</div>

										<div class="form-group">
											<select id="posisi" name="posisi" class="form-control">
												<option value="debet">Debet</option>
												<option value="kredit">Kredit</option>
											</select>
											<label for="Posisi">Posisi</label>
										</div>

									</div>
								</div>

								<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="simpan">Simpan</button>

							</div>

						</div>	

					</form>	

					<hr />

					<?php 
					if ($master_id)
					{
					?>

						<section class="style-default-bright">

							<form class="form" method="post" enctype="multipart/form-data">

							<div class="row">
								<div class="col-lg-12">
								
									<div class="table-responsive">
										<table id="datatable1" class="table table-striped table-hover">
											
											<tr>
												<th>Kode/Nama Perkiraan</th>
									            <th>Keterangan</th>
									            <th>Debet</th>
									            <th>Kredit</th>
									            <th>Action</th>
											</tr>

											<?php 
											$sqlTransDetail = "
											SELECT 
											trans_detail.* ,
											perkiraan.kode_perk ,
											perkiraan.nama_perk
											FROM trans_detail 
											JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
											WHERE 1 
											AND master_id = '".$master_id."'
											";

											$queryTransDetail = mysql_query($sqlTransDetail);

											$totalDebet = $totalKredit = 0;

											while($fetchTransDetail = mysql_fetch_array($queryTransDetail))
											{
												$totalDebet += $fetchTransDetail['debet'];
												$totalKredit += $fetchTransDetail['kredit'];
											?>

											<tr>
												<td><?php echo $fetchTransDetail['kode_perk'] .' - '. $fetchTransDetail['nama_perk']; ?></td>
												<td><?php echo $fetchTransDetail['URAIAN']; ?></td>
												<td><?php echo $fetchTransDetail['debet']; ?></td>
												<td><?php echo $fetchTransDetail['kredit']; ?></td>
												<td></td>
											</tr>

											<?php
											}
											?>

											<tr>
												<td><b>Total</b></td>
												<td></td>
												<td><?php echo $totalDebet; ?></td>
												<td><?php echo $totalKredit; ?></td>
												<td><?php echo ($totalDebet != $totalKredit) ? 'Not Balance' : 'Balance'; ?></td>
											</tr>


										</table>
									</div>

								</div>
							</div>

							<?php
							if ($totalDebet != $totalKredit)
							{
							?>
							<button type="button" class="btn ink-reaction btn-raised btn-primary" name="selesai" onclick="alert('Data Tidak Balance :(');">Selesai</button>
							<?php
							}
							else
							{
							?>
							<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="selesai">Selesai</button>
							<?php 
							}
							?>

							</form>

						</section>

						<hr />

					<?php 
					}
					?>

				</div>
			</section>

		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tanggal_transaksi').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#kode_perkiraan').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#kode_perkiraan").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#kode_perkiraan').val(ui.item.kode_perk);
					$('#nama_perkiraan').val(ui.item.nama_perk);
					
					return false;
				},
				focus: function( event, ui ) {

					$('#kode_perkiraan').val(ui.item.kode_perk);
			        $('#nama_perkiraan').val(ui.item.nama_perk);
			        
			        return false;
			    },
			});
		}
	});

});
</script>	

<?php require_once "layouts/footer.php"; ?>
	