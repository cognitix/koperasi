<section>
	<div class="section-body contain-lg">
		<div class="card">
			<div class="card-body">
				<div class="alert alert-callout alert-success" role="alert">
					<strong>Well done!</strong> You successfully submit. <a href="<?php $linkBack; ?>">Submit Again</a>
				</div>
			</div>
		</div>
	</div>
</section>