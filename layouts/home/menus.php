<?php 
$user = $_SESSION['user'];

$sqlMenuRoles = "SELECT * FROM user_role_maps WHERE 1 AND user_id = '".$user['USERID']."'";
$queryMenuRoles = mysql_query($sqlMenuRoles);
$menuRoles = array();
while($rowMenuRoles = mysql_fetch_array($queryMenuRoles))
{
	$menuRoles[] = $rowMenuRoles['menu_id'];
}


$sqlMenus = "SELECT * FROM menus";
$queryMenus = mysql_query($sqlMenus);

$menus = array();

while($row = mysql_fetch_array($queryMenus))
{
	$menus[] = array(
		'id' => $row['id'] ,
		'name' => $row['name'] ,
		'parent_id' => $row['parent_id'] ,
		'url' => $row['url']
	);
}

function has_children($rows,$id,$menuRoles) {
  	foreach ($rows as $row) {
    	if ($row['parent_id'] == $id)
    	{
    		//if (in_array($row['id'], $menuRoles))
    		//{
    			return true;
    		//}
    	}
  	}
  	return false;
}

function build_menu($rows,$parent = 0, $main = 0, $menu, $menuRoles)
{  
	if ($main == 1)
	{
		$result = '<ul id="main-menu" class="gui-controls">';
	}
	else
	{
		$result = "<ul>";
	}

  	foreach ($rows as $row)
  	{
    	if ($row['parent_id'] == $parent)
    	{
    		//if (! has_children($rows, $row['id'], $menuRoles)) continue;

    		if ($parent == 0)
    		{
	      		$result .= '<li class="gui-folder gui-main-folder" data-name="'.$row['name'].'" data-available="'.(has_children($rows, $row['id'], $menuRoles)).'">';

	      		$result .= '<a>
								<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
								<span class="title">'.$row['name'].'</span>
							</a>';
			}
			else
			{
				if ($row['url'])
				{
					if (in_array($row['id'], $menuRoles))
					{

						$class = (!empty($menu) && $menu == str_replace(".php", "", $row['url'])) ? 'active' : '';

						$result .= '<li data-available="'.(has_children($rows, $row['id'], $menuRoles)).'"><a href="'.$row['url'].'" class="'.$class.'"><span class="title">'.$row['name'].'</span></a>';
					
					}
				}
				else
				{
					$result .= '<li class="gui-folder" data-available="'.(has_children($rows, $row['id'], $menuRoles)).'">
									<a href="#">
										<span class="title">'.$row['name'].'</span>
									</a>';
				}
			}
      		
      		if (has_children($rows, $row['id'], $menuRoles))
        		$result.= build_menu($rows, $row['id'], 0, $menu, $menuRoles);
      		
      		if (in_array($row['url'], $menuRoles))
      			$result.= "</li>";
    	}
  	}
 
  	$result.= "</ul>";

  	return $result;
}
?>




	<!-- BEGIN MENUBAR-->
	<div id="menubar" class="menubar-inverse ">
		<div class="menubar-fixed-panel">
			<div>
				<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
					<i class="fa fa-bars"></i>
				</a>
			</div>
			<div class="expanded">
				<a href="index.php">
					<span class="text-lg text-bold text-primary ">Koperasi</span>
				</a>
			</div>
		</div>
		<div class="menubar-scroll-panel">

			<!-- BEGIN MAIN MENU -->

			<?php /*
			<ul id="main-menu" class="gui-controls">

				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
						<span class="title">Cabang</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="cabang_entrydata.php" class="<?php echo (!empty($menu) && $menu == "cabang_entrydata") ? 'active' : ''; ?>"><span class="title">Entry Data</span></a></li>
						<li><a href="cabang_viewall.php" class="<?php echo (!empty($menu) && $menu == "cabang_viewall") ? 'active' : ''; ?>"><span class="title">View All Data</span></a></li>
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->

				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
						<span class="title">Users</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="user_entrydata.php" class="<?php echo (!empty($menu) && $menu == "user_entrydata") ? 'active' : ''; ?>"><span class="title">Entry Data</span></a></li>
						<li><a href="user_viewall.php" class="<?php echo (!empty($menu) && $menu == "user_viewall") ? 'active' : ''; ?>"><span class="title">View All Data</span></a></li>
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->

				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
						<span class="title">Nasabah</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="customer_service_entrydata.php" class="<?php echo (!empty($menu) && $menu == "customer_service_entrydata") ? 'active' : ''; ?>"><span class="title">Entry Data</span></a></li>
						<li><a href="customer_service_viewall.php" class="<?php echo (!empty($menu) && $menu == "customer_service_viewall") ? 'active' : ''; ?>"><span class="title">View All Data</span></a></li>
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->

				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
						<span class="title">Simpanan</span>
					</a>
					<!--start submenu -->
					<ul>
						<li><a href="simpanan_entrydata.php" class="<?php echo (!empty($menu) && $menu == "simpanan_entrydata") ? 'active' : ''; ?>"><span class="title">Entry Data</span></a></li>
						<li><a href="simpanan_viewall.php" class="<?php echo (!empty($menu) && $menu == "simpanan_viewall") ? 'active' : ''; ?>"><span class="title">View All Data</span></a></li>
					</ul><!--end /submenu -->
				</li><!--end /menu-li -->

				<li class="gui-folder">
					<a>
						<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
						<span class="title">Transaksi Teller</span>
					</a>
					<!--start submenu -->
					<ul>
						<li class="gui-folder">
							<a href="javascript:void(0);">
								<span class="title">Simpanan</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="setor_dan_tarik.php" class="<?php echo (!empty($menu) && $menu == "setor_dan_tarik") ? 'active' : ''; ?>"><span class="title">Setor dan Tarik</span></a></li>
								<li><a href="penutupan_simpanan.php" class="<?php echo (!empty($menu) && $menu == "penutupan_simpanan") ? 'active' : ''; ?>"><span class="title">Penutupan Simpanan</span></a></li>
								<li><a href="print_buku_simpanan.php" class="<?php echo (!empty($menu) && $menu == "print_buku_simpanan") ? 'active' : ''; ?>"><span class="title">Print Buku Simpanan</span></a></li>
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
					</ul>
				</li><!--end /menu-li -->

			</ul>
			*/ 


			echo build_menu($menus, 0, 1, (!empty($menu)) ? $menu : null, $menuRoles);

			?>

			<div class="menubar-foot-panel">
				<small class="no-linebreak hidden-folded">
					<span class="opacity-75">Copyright &copy; <?php echo date('Y'); ?></span> <strong>Koperasi</strong>
				</small>
			</div>

		</div>
	</div>


<style type="text/css">

</style>


<script type="text/javascript">
$(function(){
	$('#main-menu').find('.gui-main-folder').each(function(){

		//console.log($(this).);
		var ul = $(this).find('ul li');

		//console.log("A.length " + $(this).attr('data-name') + " = " + $(this).find('a').length);
		
		var tot = 0;
		$(this).find('a').each(function(){
			//console.log($(this).attr('href'));
			var href = $(this).attr('href');
			if (typeof(href) !== 'undefined' && href.indexOf(".php") != -1)
			{
				tot++;
			}
		});

		//console.log(tot);

		if (tot)
		{
			$(this).show();
		}
		else
		{
			$(this).hide();
		}


		//var dataAvailable = $(this).attr('data-available');
		//if (! dataAvailable)
		//	$(this).remove();
		//else
		//	$(this).show();
	});
});
</script>
