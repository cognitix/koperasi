<section>
	<div class="section-body contain-lg">
		<div class="card">
			<div class="card-body">
				<div class="alert alert-callout alert-danger" role="alert">
					<strong>Oh snap!</strong> Change a few things up and try submitting again. <a href="<?php $linkBack; ?>">Try Again</a>
				</div>
			</div>
		</div>
	</div>
</section>