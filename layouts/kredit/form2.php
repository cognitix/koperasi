<div class="row">

	<div class="col-lg-12">

		<br/><br/>

		<?php 
		$sql = "SELECT * FROM kredit WHERE 1 AND NO_REKENING = '".$norekening."'";
		$result = mysql_fetch_array(mysql_query($sql));

		$JML_PINJAMAN = $result['JML_PINJAMAN'];
		$JML_BUNGA_PINJAMAN = $result['JML_BUNGA_PINJAMAN'];
		$JML_ANGSURAN = $result['JML_ANGSURAN'];
		$PERIODE_ANGSURAN = $result['PERIODE_ANGSURAN'];
		$ADM = $result['ADM'];
		$TGL_REALISASI = $result['TGL_REALISASI'];		
		?>

		<div class="table-responsive">
			<table id="datatable1" class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Tgl</th>
						<th>Pokok</th>
						<th>Bunga</th>
						<th>ADM</th>
						<th>Angsuran</th>
						<th>Sisa Pinjaman</th>
					</tr>
				</thead>
				<tbody>

					<?php 
					$sisaPinjaman = $totalPokok = $totalBunga = $totalAdmin = $totalAngsuran = 0;

					for($i=0;$i<=$JML_ANGSURAN;$i++)
					{

						if ($i == 0)
						{
					?>
					<tr>
						<td>0</td>
						<td>0</td>
						<td>0</td>

						<td>0</td>
						<td>0</td>
						<td><?php 
							$sisaPinjaman += $JML_PINJAMAN;
							echo number_format($sisaPinjaman,0,'.',','); ?></td>
					</tr>
					<?php
						}
						else
						{
							$tgl = $i;

							if ($PERIODE_ANGSURAN == "3")
							{
								//$tgl = date('d/m/Y', strtotime($TGL_REALISASI . " +". (30*$i) ." days"));
								//$tgl = date('d') .'/'. date('m', strtotime('+' . ($i+1) . " month")) .'/'. date('Y');
								$tgl = date('d/m/Y', strtotime($TGL_REALISASI . " +". ($i) ." month"));
							}
							else if ($PERIODE_ANGSURAN == "6")
							{
								//$tgl = date('d/m/Y', strtotime($TGL_REALISASI . " + " . (30*12*$i) ." days"));
								//$tgl = date('d') .'/'. date('m') .'/'. date('Y', strtotime('+' . ($i+1) . " year"));
								$tgl = date('d/m/Y', strtotime($TGL_REALISASI . " +". ($i) ." year"));
							}

							$pokok = ($JML_PINJAMAN / $JML_ANGSURAN);
							$bunga = ($JML_BUNGA_PINJAMAN / $JML_ANGSURAN);//($SUKU_BUNGA_PER_ANGSURAN/100) * $sisaPinjaman;
							$admin = ($ADM / $JML_ANGSURAN);
							$angsuran = $pokok + $bunga + $admin;

							$sisaPinjaman -= $angsuran;

							$totalPokok += $pokok;
							$totalBunga += $bunga;
							$totalAdmin += $admin;
							$totalAngsuran += $angsuran;
					?>
					<tr>
						<td><?php echo $tgl; ?></td>
						<td><?php echo number_format($pokok,0,'.',','); ?></td>
						<td><?php echo number_format($bunga,0,'.',','); ?></td>

						<td><?php echo number_format($admin,0,'.',','); ?></td>
						<td><?php echo number_format($angsuran,0,'.',','); ?></td>
						<td><?php echo ($sisaPinjaman > 0) ? number_format($sisaPinjaman,0,'.',',') : 0; ?></td>
					</tr>
					<?php
						}

					}
					?>

					<tr>
						<td><b>Total</b></td>
						<td><?php echo number_format($totalPokok,0,'.',','); ?></td>
						<td><?php echo number_format($totalBunga,0,'.',','); ?></td>

						<td><?php echo number_format($totalAdmin,0,'.',','); ?></td>
						<td><?php echo number_format($totalAngsuran,0,'.',','); ?></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>

	</div>
	
</div>	

<br /><br />

<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit_form2">Submit</button>