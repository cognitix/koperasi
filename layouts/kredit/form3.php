<div class="row">

	<div class="col-lg-6">

		<?php 
		$sql = "SELECT * FROM kredit WHERE 1 AND NO_REKENING = '".$norekening."'";
		$result = mysql_fetch_array(mysql_query($sql));
		?>

		<br/><br/>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="PENJAMIN" name="PENJAMIN" value="<?php echo $result['PENJAMIN']; ?>" />
			<label for="PENJAMIN">Nama Penjamin</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="PEKERJAAN_PENJAMIN" name="PEKERJAAN_PENJAMIN" value="<?php echo $result['PEKERJAAN_PENJAMIN']; ?>" />
			<label for="PEKERJAAN_PENJAMIN">Pekerjaan Penjamin</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="ALAMAT_PENJAMIN" name="ALAMAT_PENJAMIN" value="<?php echo $result['ALAMAT_PENJAMIN']; ?>" />
			<label for="ALAMAT_PENJAMIN">Alamat Penjamin</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="ID_PENJAMIN" name="ID_PENJAMIN" value="<?php echo $result['ID_PENJAMIN']; ?>" />
			<label for="ID_PENJAMIN">ID Penjamin</label>
		</div>

		<div class="form-group floating-label">
			<div class="input-group date" id="TGL_ANALISA_container">
				<div class="input-group-content">
					<?php 
					$TGL_ANALISA = (empty($result['TGL_ANALISA'])) ? date('Y-m-d') : $result['TGL_ANALISA'];
					?>
					<input type="text" class="form-control" id="TGL_ANALISA" name="TGL_ANALISA" value="<?php echo $TGL_ANALISA; ?>" required>
					<label for="TGL_ANALISA">Tgl Analisa Kredit</label>
				</div>
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>

	</div>

	<div class="col-lg-6">

		<br /><br />

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="NAMA_PASANGAN" name="NAMA_PASANGAN" value="<?php echo $result['NAMA_PASANGAN']; ?>" />
			<label for="NAMA_PASANGAN">Nama Pasangan/Ortu</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="ALAMAT_PASANGAN" name="ALAMAT_PASANGAN" value="<?php echo $result['ALAMAT_PASANGAN']; ?>" />
			<label for="ALAMAT_PASANGAN">Alamat Pasangan/Ortu</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="PEKERJAAN_PASANGAN" name="PEKERJAAN_PASANGAN" value="<?php echo $result['PEKERJAAN_PASANGAN']; ?>" />
			<label for="PEKERJAAN_PASANGAN">Pekerjaan Pasangan/Ortu</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="STATUS_PASANGAN" name="STATUS_PASANGAN" value="<?php echo $result['STATUS_PASANGAN']; ?>" />
			<label for="STATUS_PASANGAN">Status Pasangan/Ortu</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="TUJUAN_PENGGUNAAN" name="TUJUAN_PENGGUNAAN" value="<?php echo $result['TUJUAN_PENGGUNAAN']; ?>" />
			<label for="TUJUAN_PENGGUNAAN">Tujuan Penggunaan</label>
		</div>

	</div>	

</div>

<br /><br />

<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit_form3">Submit</button>