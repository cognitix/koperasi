<div class="row">

	<div class="col-lg-6">

		<br/><br/>

		<div class="form-group floating-label">
			<select id="JENIS_PINJAMAN" name="JENIS_PINJAMAN" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqlJenisPinjaman = mysql_query("SELECT * FROM kodejeniskredit WHERE 1 ORDER BY KODE_JENIS_KREDIT ASC");
				while($rowJenisPinjaman = mysql_fetch_array($sqlJenisPinjaman))
				{
				?>
				<option value="<?php echo $rowJenisPinjaman['KODE_JENIS_KREDIT']; ?>"><?php echo $rowJenisPinjaman['KODE_JENIS_KREDIT'] .' - '. $rowJenisPinjaman['DESKRIPSI_JENIS_KREDIT']; ?></option>
				<?php
				}
				?>

			</select>
			<label for="JENIS_PINJAMAN">Jenis Pinjaman</label>
		</div>

		<?php /*
		<div class="form-group floating-label">
			<select id="KOLEKTIBILITAS" name="KOLEKTIBILITAS" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqlKolektibilitas = mysql_query("SELECT * FROM kodekolektibilitaskredit WHERE 1");
				while($rowKolektibilitas = mysql_fetch_array($sqlKolektibilitas))
				{
				?>
				<option value="<?php echo $rowKolektibilitas['KODE_KOLEKTIBILITAS']; ?>"><?php echo $rowKolektibilitas['KODE_KOLEKTIBILITAS'] .' - '. $rowKolektibilitas['DESKRIPSI_KOLEKTIBILITAS']; ?></option>
				<?php
				}
				?>

			</select>
			<label for="KOLEKTIBILITAS">Kode Kolektibilitas</label>
		</div> */ ?>

		<div class="form-group">
			<?php 
			/*
			$sqlNoRekening = "SELECT NO_REKENING FROM kredit WHERE 1 AND LEFT(NO_REKENING, 3) = '130' ORDER by NO_REKENING DESC LIMIT 1;";
			$fetchNoRekening = mysql_fetch_array(mysql_query($sqlNoRekening));
			$idMax = (int) str_replace(".", "", $fetchNoRekening['NO_REKENING']);
			$idMax++;
			$norekening = sprintf("%09s", $idMax);
			$norekening1 = substr($norekening, 0, 3);
			$norekening2 = substr($norekening, 3);
			*/
			?>
			<input type="text" class="form-control" id="NO_REKENING" name="NO_REKENING" value="<?php //echo $norekening1 .'.'. $norekening2; ?>" required readonly>
			<label for="NO_REKENING">Nomor Rekening Kredit</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="NASABAH_ID" name="NASABAH_ID" data-source="autosuggest_anggota.php">
			<label for="NASABAH_ID">Anggota ID</label>
		</div>

		<div class="form-group">
			<input type="text" class="form-control" id="NASABAH" name="NASABAH" readonly>
			<label for="NASABAH">Nama Anggota</label>
		</div>

		<div class="form-group">
			<input type="text" class="form-control" id="NO_PASSPORT" name="NO_PASSPORT" readonly>
			<label for="NO_PASSPORT">No Passport</label>
		</div>

		<div class="form-group ">
			<input type="text" class="form-control" id="alamat" name="alamat" readonly>
			<label for="alamat">Alamat</label>
		</div>

		<div class="form-group floating-label">
			<select id="KODE_GROUP5" name="KODE_GROUP5" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqlkodegroup5kredit = mysql_query("SELECT * FROM kodegroup5kredit WHERE 1");
				while($rowkodegroup5kredit = mysql_fetch_array($sqlkodegroup5kredit))
				{
				?>
				<option value="<?php echo $rowkodegroup5kredit['KODE_GROUP5']; ?>"><?php echo $rowkodegroup5kredit['KODE_GROUP5'] .' - '. $rowkodegroup5kredit['DESKRIPSI_GROUP5']; ?></option>
				<?php
				}
				?>

			</select>
			<label for="KODE_GROUP5">PPTKIS</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="slip_payment" name="slip_payment"/>
			<label for="slip_payment">Slip Payment</label>
		</div>

		<div class="form-group floating-label">
			<select id="KODE_TYPE_KREDIT" name="KODE_TYPE_KREDIT" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqlkodetypekredit = mysql_query("SELECT * FROM kodetypekredit WHERE 1");
				while($rowkodetypekredit = mysql_fetch_array($sqlkodetypekredit))
				{
				?>
				<option value="<?php echo $rowkodetypekredit['KODE_TYPE_KREDIT']; ?>"><?php echo $rowkodetypekredit['KODE_TYPE_KREDIT'] .' - '. $rowkodetypekredit['DESKRIPSI_TYPE_KREDIT']; ?></option>
				<?php
				}
				?>

			</select>
			<label for="KODE_TYPE_KREDIT">Kode Type Kredit</label>
		</div>

		<div class="form-group floating-label">
			<div class="input-group date" id="TGL_REALISASI_container">
				<div class="input-group-content">
					<?php 
					$TGL_REALISASI = date('Y-m-d');
					?>
					<input type="text" class="form-control" id="TGL_REALISASI" name="TGL_REALISASI" value="<?php echo $TGL_REALISASI; ?>" required>
					<label for="TGL_REALISASI">Tanggal Realisasi</label>
				</div>
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>

	</div>

	<div class="col-lg-6">

		<br/><br/>

		<div class="form-group floating-label">
			<?php /*<select id="lnfagctw" name="lnfagctw" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqllnfagctw = mysql_query("SELECT * FROM lnfagctw WHERE 1");
				while($rowlnfagctw = mysql_fetch_array($sqllnfagctw))
				{
				?>
				<option value="<?php echo $rowlnfagctw['IDAGCTW']; ?>"><?php echo $rowlnfagctw['IDAGCTW'] .' - '. $rowlnfagctw['NMAGCTW']; ?></option>
				<?php
				}
				?>

			</select>*/ ?>
			<input type="text" class="form-control" id="agency" name="agency" value="">
			<label for="agency">Agency</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="nohp_agency" name="nohp_agency" value="">
			<label for="nohp_agency">No HP Agency</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="nama_majikan" name="nama_majikan"/>
			<label for="nama_majikan">Nama Majikan</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="no_telepon" name="no_telepon"/>
			<label for="no_telepon">No. Telp/HP</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="JML_PINJAMAN" name="JML_PINJAMAN" value="0" />
			<label for="JML_PINJAMAN">Jumlah Pinjaman</label>
		</div>

		<div class="form-group">

			<div class="input-group">
				<div class="input-group-content" style="width:40%;">
					<input type="text" class="form-control" id="bunga_pinjaman" name="bunga_pinjaman" required>
					<label for="bunga_pinjaman">Bunga Pinjaman (%)</label>
				</div>
				<div class="input-group-content" style="width:60%;">
					<input type="text" class="form-control" id="JML_BUNGA_PINJAMAN" name="JML_BUNGA_PINJAMAN" required>
					<label for="JML_BUNGA_PINJAMAN">Jumlah Bunga Pinjaman</label>
				</div>
			</div>
		</div>

		<div class="form-group floating-label">

			<div class="input-group">
				<div class="input-group-content" style="width:40%;">
					<input type="text" class="form-control" id="JML_ANGSURAN" name="JML_ANGSURAN" required>
					<label for="JML_ANGSURAN">Jumlah Angsuran</label>
				</div>
				<div class="input-group-content" style="width:60%;">
					<select id="PERIODE_ANGSURAN" name="PERIODE_ANGSURAN" class="form-control" required>
						<option value="">&nbsp;</option>

						<?php 
						$sqlkodeperiodepembayaran = mysql_query("SELECT * FROM kodeperiodepembayaran WHERE 1 AND kode_periode_pembayaran IN (3,6)");
						while($rowkodeperiodepembayaran = mysql_fetch_array($sqlkodeperiodepembayaran))
						{
						?>
						<option value="<?php echo $rowkodeperiodepembayaran['kode_periode_pembayaran']; ?>"><?php echo $rowkodeperiodepembayaran['deskripsi_periode_pembayaran']; ?></option>
						<?php
						}
						?>

					</select>
					<label for="PERIODE_ANGSURAN">Periode Angsuran</label>
				</div>
			</div>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="PROVISI" name="PROVISI"/>
			<label for="PROVISI">Biaya Provisi</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="ADM" name="ADM"/>
			<label for="ADM">Biaya ADM</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="GRACE_PERIODE" name="GRACE_PERIODE" value="0"/>
			<label for="GRACE_PERIODE">Grace Periode</label>
		</div>

	</div>

</div>

<br /><br />

<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit_form1">Submit</button>