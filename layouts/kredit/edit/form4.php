<div class="row">

	<div class="col-lg-6">

		<?php 
		$nomor_rekening = (!empty($_GET['nomor_rekening'])) ? $_GET['nomor_rekening'] : "";
		$sql = "SELECT * FROM kredit WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
		$result = mysql_fetch_array(mysql_query($sql));
		?>

		<br /><br />

		<div class="form-group floating-label">
			<select id="AGUNAN_JENIS" name="AGUNAN_JENIS" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqlKodeJenisAgunan = mysql_query("SELECT * FROM kodejenisagunan WHERE 1 ORDER BY KODE_JENIS_AGUNAN ASC");
				while($rowKodeJenisAgunan = mysql_fetch_array($sqlKodeJenisAgunan))
				{
					$selected = ($rowKodeJenisAgunan['KODE_JENIS_AGUNAN'] == $result['AGUNAN_JENIS']) ? 'selected' : '';
				?>
				<option value="<?php echo $rowKodeJenisAgunan['KODE_JENIS_AGUNAN']; ?>" <?php echo $selected; ?>><?php echo $rowKodeJenisAgunan['KODE_JENIS_AGUNAN'] .' - '. $rowKodeJenisAgunan['DESKRIPSI_JENIS_AGUNAN']; ?></option>
				<?php
				}
				?>

			</select>
			<label for="AGUNAN_JENIS">Jenis Agunan</label>
		</div>

		<div class="form-group floating-label">
			<select id="AGUNAN_IKATAN_HUKUM" name="AGUNAN_IKATAN_HUKUM" class="form-control" required>
				<option value="">&nbsp;</option>

				<?php 
				$sqlKodeIkatanHukum = mysql_query("SELECT * FROM kodeikatanhukumkredit WHERE 1 ORDER BY KODE_IKATAN_HUKUM ASC");
				while($rowKodeIkatanHukum = mysql_fetch_array($sqlKodeIkatanHukum))
				{
					$selected = ($rowKodeIkatanHukum['KODE_IKATAN_HUKUM'] == $result['AGUNAN_IKATAN_HUKUM']) ? 'selected' : '';
				?>
				<option value="<?php echo $rowKodeIkatanHukum['KODE_IKATAN_HUKUM']; ?>" <?php echo $selected; ?>><?php echo $rowKodeIkatanHukum['KODE_IKATAN_HUKUM'] .' - '. $rowKodeIkatanHukum['DESKRIPSI_IKATAN_HUKUM']; ?></option>
				<?php
				}
				?>

			</select>
			<label for="AGUNAN_IKATAN_HUKUM">Agunan Ikatan Hukum</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="AGUNAN_NILAI" name="AGUNAN_NILAI" value="<?php echo $result['AGUNAN_NILAI']; ?>" />
			<label for="AGUNAN_NILAI">Nilai Agunan</label>
		</div>

		<div class="form-group">

			<div class="input-group">

				<?php 
				$NILAI_LIKUIDASI = ($result['NILAI_LIKUIDASI']) ? $result['NILAI_LIKUIDASI'] : 0;
				$bunga_likuidasi = 0;
				?>

				<div class="input-group-content" style="width:40%;">
					<input type="text" class="form-control" id="bunga_likuidasi" name="bunga_likuidasi" required value="<?php echo $bunga_likuidasi; ?>" />
					<label for="bunga_likuidasi">Bunga Likuidasi (%)</label>
				</div>
				<div class="input-group-content" style="width:60%;">
					<input type="text" class="form-control" id="NILAI_LIKUIDASI" name="NILAI_LIKUIDASI" required readonly value="<?php echo $NILAI_LIKUIDASI; ?>">
					<label for="NILAI_LIKUIDASI">Nilai Likuidasi</label>
				</div>
			</div>
		</div>

	</div>

	<div class="col-lg-6">

		<br /><br />

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="PEMILIK_AGUNAN" name="PEMILIK_AGUNAN" value="<?php echo $result['PEMILIK_AGUNAN']; ?>" />
			<label for="PEMILIK_AGUNAN">Pemilik Agunan</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="alamat_agunan" name="alamat_agunan" value="<?php echo $result['alamat_agunan']; ?>" />
			<label for="alamat_agunan">Alamat Agunan</label>
		</div>

		<div class="form-group floating-label">
			<div class="input-group date" id="TGL_JT_AGUNAN_container">
				<div class="input-group-content">
					<?php 
					$TGL_JT_AGUNAN = ($result['TGL_JT_AGUNAN']) ? $result['TGL_JT_AGUNAN'] : date('Y-m-d');
					?>
					<input type="text" class="form-control" id="TGL_JT_AGUNAN" name="TGL_JT_AGUNAN" value="<?php echo $TGL_JT_AGUNAN; ?>" required>
					<label for="TGL_JT_AGUNAN">Tanggal Jatuh Tempo Agunana</label>
				</div>
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="NO_AGUNAN" name="NO_AGUNAN" value="<?php echo $result['NO_AGUNAN']; ?>" />
			<label for="NO_AGUNAN">No Agunan</label>
		</div>

		<div class="form-group floating-label">
			<input type="text" class="form-control" id="AGUNAN" name="AGUNAN" value="<?php echo $result['AGUNAN']; ?>" />
			<label for="AGUNAN">Agunan</label>
		</div>

	</div>
	
</div>

<br /><br />