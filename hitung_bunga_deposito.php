<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "hitung_bunga_deposito";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	//$saldoMonth = $_POST['saldo-month'];

	// >= 7500000

	//$days_of_month = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
	//echo "days_of_month : " . $days_of_month . ' <br />';

	$tanggal = $_POST['tanggal'];

	$sql = "
	SELECT * FROM deposito 
	WHERE 1 
	AND DATE_FORMAT(deposito.TGL_REGISTRASI,'%m-%Y') = '".$tanggal."'
	";

	//echo $sql;die();

	$query = mysql_query($sql);
	while($fetch = mysql_fetch_array($query))
	{
		$bunga_bulan_ini = $fetch['SUKU_BUNGA'] * $fetch['JML_DEPOSITO'] / 100;
		$pajak_bulan_ini = $bunga_bulan_ini * $fetch['PERSEN_PPH'] / 100;

		$sql = "
		UPDATE deposito SET 
		BUNGA_BLN_INI = '".$bunga_bulan_ini."' ,
		PAJAK_BLN_INI = '".$pajak_bulan_ini."' 
		WHERE 1
		AND NO_REKENING = '".$fetch['NO_REKENING']."'
		";

		mysql_query($sql);
	}

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Hitung Bunga</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data" >

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										
										<div class="form-group">
											<div class="input-group date" id="TGL_container">
												<div class="input-group-content">
													<input type="text" class="form-control" name="tanggal" value="<?php echo date("m-Y"); ?>">
													<label>Tanggal</label>
												</div>
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
										

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

						<?php 
						if ($message == 1)
						{
							$linkBack = "hitung_bunga_deposito.php";
							require_once "layouts/message_success.php";
						}
						?>

					</form>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script>
$(function(){
	$('#TGL_container').datepicker({
		autoclose: true, 
		todayHighlight: true, 
		minViewMode: 1,
		format: "mm-yyyy"
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>