<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "kredit_viewall";
$form = (!empty($_GET['form'])) ? $_GET['form'] : 1;

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/wizard/wizard.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section class="style-default-bright">
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Kredit View All Data</li>
					</ol>
				</div>
				<div class="section-body">

					<?php /*
					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<?php 
											$nomor_rekening = (!empty($_GET['nomor_rekening'])) ? $_GET['nomor_rekening'] : "";
											?>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" data-source="autosuggest_nomorrekening_kredit.php" value="<?php echo $nomor_rekening; ?>">
												<label for="nomor_rekening">Nomor Rekening Kredit</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">View</button>

					</form>*/ ?>

					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive">
								<table id="datatable1" class="table table-striped table-hover">
									<thead>
										<tr>
											<th class="sort-numeric">No Rekening</th>
											<th>Nasabah ID</th>
											<th class="sort-alpha">Nama Nasabah</th>
											<th >Jumlah Pinjaman</th>
											<th >Jumlah Angsuran</th>
										</tr>
									</thead>
									<tbody>

										<?php 
										$sql = "
										SELECT * FROM kredit WHERE 1 AND NO_REKENING != ''
										";
										$sql = mysql_query($sql);
										while($row = mysql_fetch_array($sql))
										{
										?>

										<tr>
											<td><a href="kredit_view_detail.php?nomor_rekening=<?php echo $row['NO_REKENING']; ?>" class="btn ink-reaction btn-flat btn-xs btn-primary"><?php echo $row['NO_REKENING']; ?></a></td>
											<td><?php echo $row['NASABAH_ID']; ?></td>
											<td><?php echo $row['NASABAH']; ?></td>
											<td><?php echo $row['JML_PINJAMAN']; ?></td>
											<td><?php echo $row['JML_ANGSURAN']; ?></td>
										</tr>

										<?php
										}
										?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
					
				</div>	

			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			      },
			});
		}
	});

	$('#TGL_REALISASI_container, #TGL_ANALISA_container, #TGL_JT_AGUNAN_container').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	var noindex = <?php echo $form; ?>;

	var handleTabShow = function(tab, navigation, index, wizard){
		var total = navigation.find('li').length;
		var current = index - 1;
		var percent = (current / (total - 1)) * 100;
		var percentWidth = 100 - (100 / total) + '%';
		
		navigation.find('li').addClass('done');//.removeClass('done');
		//navigation.find('li.active').prevAll().addClass('done');
		
		wizard.find('.progress-bar').css({width: percent + '%'});
		$('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
	};

	$('#rootwizard1').bootstrapWizard({
		onTabShow: function(tab, navigation, index) {
			handleTabShow(tab, navigation, noindex, $('#rootwizard1'));
		}
	});

	$('#bunga_pinjaman').bind('keyup', function(){
		var el = $(this);
		var bunga_pinjaman = parseFloat(el.val());
		var JML_PINJAMAN = parseFloat($('#JML_PINJAMAN').val());
		var JML_BUNGA_PINJAMAN = parseFloat(bunga_pinjaman / 100 * JML_PINJAMAN);
		$('#JML_BUNGA_PINJAMAN').val(JML_BUNGA_PINJAMAN);
	});

	$('#JML_BUNGA_PINJAMAN').bind('keyup', function(){
		var el = $(this);
		var JML_BUNGA_PINJAMAN = parseFloat(el.val());
		var JML_PINJAMAN = parseFloat($('#JML_PINJAMAN').val());
		var bunga_pinjaman = parseFloat( (JML_BUNGA_PINJAMAN / JML_PINJAMAN) * 100);
		$('#bunga_pinjaman').val(bunga_pinjaman);
	});

	$('#bunga_likuidasi').bind('keyup', function(){
		var el = $(this);
		var bunga_likuidasi = parseFloat(el.val());
		var AGUNAN_NILAI = parseFloat($('#AGUNAN_NILAI').val());
		var NILAI_LIKUIDASI = parseFloat(bunga_likuidasi / 100 * AGUNAN_NILAI);
		$('#NILAI_LIKUIDASI').val(NILAI_LIKUIDASI);
	});

	/*
	$('#JENIS_PINJAMAN').bind('change', function(){
		$.ajax({
			type: 'GET',
			url: 'ajax_nomor_rekening.php',
			data : {
				JENIS_PINJAMAN : $(this).val()
			},
			dataType: "json",
			success: function (datas) {
				$('#NO_REKENING').val(datas.NO_REKENING);
			}
		});
	});
	*/
});
</script>


<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>