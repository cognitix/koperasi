<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "deposito_entry_data";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	$tanggal_register = $_POST['tanggal_register'];
	$nomor_rekening = $_POST['nomor_rekening'];
	$kodejenisdeposito = $_POST['kodejenisdeposito'];
	$NO_ALTERNATIF = $_POST['NO_ALTERNATIF'];
	$NASABAH_ID = $_POST['NASABAH_ID'];
	$nama_nasabah = $_POST['nama_nasabah'];
	$QQ = $_POST['QQ'];
	$jumlah_deposito = $_POST['jumlah_deposito'];
	$suku_bunga = $_POST['suku_bunga'];
	$persen_pph = $_POST['persen_pph'];
	$statusbunga = $_POST['statusbunga'];

	$SALDO_AWAL = $jumlah_deposito;
	$SALDO_SETORAN = $jumlah_deposito;
	$SALDO_AKHIR = $jumlah_deposito;

	$NO_REK_TABUNGAN = $_POST['NO_REK_TABUNGAN'];

	$cab = $kodecabang['kode_cab'];
	$userId = $user['USERID'];

	$SALDO_NOMINATIF = $jumlah_deposito;

	$TGL_MULAI = $tanggal_register;

	$sqlKodeJenisDeposito = "SELECT * FROM kodejenisdeposito WHERE 1 AND KODE_JENIS_DEPOSITO = '$kodejenisdeposito'";
	$fetchKodeJenisDeposito = mysql_fetch_array(mysql_query($sqlKodeJenisDeposito));
	$JKW = $fetchKodeJenisDeposito['JKW_DEFAULT'];

	$TGL_JT = date("Y-m-d", strtotime($tanggal_register . "+" . $JKW . " month"));

	$STATUS_AKTIF = 1;

	$sql = "
	INSERT INTO deposito SET 
	KODE_BI_PEMILIK = '874' ,
	NO_REKENING = '".$nomor_rekening."' ,
	NO_ALTERNATIF = '".$NO_ALTERNATIF."' ,
	NASABAH_ID = '".$NASABAH_ID."' ,
	QQ = '".$QQ."' ,
	JENIS_DEPOSITO = '".$kodejenisdeposito."' ,
	JML_DEPOSITO = '".$jumlah_deposito."' ,
	SUKU_BUNGA = '".$suku_bunga."' ,
	PERSEN_PPH = '".$persen_pph."' ,
	TGL_REGISTRASI = '".$tanggal_register."' ,
	JKW = '".$JKW."' ,
	TGL_JT = '".$TGL_JT."' ,
	STATUS_AKTIF = '".$STATUS_AKTIF."' ,
	SALDO_AWAL = '".$SALDO_AWAL."' ,
	SALDO_SETORAN = '".$SALDO_SETORAN."' ,
	SALDO_AKHIR = '".$SALDO_AKHIR."' ,
	NO_REK_TABUNGAN = '".$NO_REK_TABUNGAN."' ,
	USERID = '".$userId."' ,
	SALDO_NOMINATIF = '".$SALDO_NOMINATIF."' ,
	KODE_CAB = '".$cab."' ,
	CAB = '".$cab."' ,
	STATUS_BUNGA = '".$statusbunga."'
	";

	//echo $sql;

	mysql_query($sql);

	$message = 1;
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
				$linkBack = "deposito_entry_data.php";
				require_once "layouts/message_success.php";
			}
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Deposito Entry Data</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="tanggal_register" name="tanggal_register" value="<?php echo $tglsystem; ?>" 
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?>
												/>
												<label for="tanggal_register">Tanggal Register</label>
											</div>

											<div class="form-group floating-label">
												<select id="kodejenisdeposito" name="kodejenisdeposito" class="form-control">
													<option value="">&nbsp;</option>
													<?php 
													$sqlKodeDeposito = "SELECT * FROM kodejenisdeposito WHERE 1 ORDER BY KODE_JENIS_DEPOSITO ASC";
													$fetchKodeDeposito = mysql_query($sqlKodeDeposito);
													while($rowKodeDeposito = mysql_fetch_array($fetchKodeDeposito))
													{
													?>
													<option value="<?php echo $rowKodeDeposito['KODE_JENIS_DEPOSITO']; ?>"><?php echo $rowKodeDeposito['KODE_JENIS_DEPOSITO']; ?> - <?php echo $rowKodeDeposito['DESKRIPSI_JENIS_DEPOSITO']; ?></option>
													<?php
													}
													?>
												</select>
												<label for="kodejenisdeposito">Kode Jenis Deposito</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" value="">
												<label for="nomor_rekening">Nomor Rekening</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_ALTERNATIF" name="NO_ALTERNATIF" value="">
												<label for="NO_ALTERNATIF">Nomor Alternatif</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NASABAH_ID" name="NASABAH_ID" data-source="autosuggest_nomorrekening_all.php">
												<label for="NASABAH_ID">Anggota ID</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly>
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="NO_REK_TABUNGAN" name="NO_REK_TABUNGAN" readonly>
												<label for="NO_REK_TABUNGAN">Nomor Rekening Tabungan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="QQ" name="QQ" value="">
												<label for="QQ">QQ</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="jumlah_deposito" name="jumlah_deposito" value="">
												<label for="jumlah_deposito">Jumlah Deposito</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="suku_bunga" name="suku_bunga" value="10">
												<label for="suku_bunga">Suku Bunga</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="persen_pph" name="persen_pph" value="20">
												<label for="persen_pph">Persen PPh</label>
											</div>

											<div class="form-group floating-label">
												<select id="statusbunga" name="statusbunga" class="form-control">
													<option value="">&nbsp;</option>
													<option value="Aro Ke Pokok">Aro Ke Pokok</option>
													<option value="Aro Ke Titipan">Aro Ke Titipan</option>
													<option value="Aro Ke Tabungan">Overbook Ke Tabungan</option>
												</select>
												<label for="statusbunga">Status Bunga</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

					</form>

				</div>	
			</section>

			<?php require_once "layouts/home/menus.php"; ?>

		</div>

	</div>
	
<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tanggal_register').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#NASABAH_ID').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#NASABAH_ID").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#NASABAH_ID').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);
					$('#NO_REK_TABUNGAN').val(ui.item.NO_REKENING);

					return false;
				},
				focus: function( event, ui ) {

			        $( "#NASABAH_ID" ).val( ui.item.nasabah_id );
			        
			        return false;
			   	},
			});
		}
	});

	$('#kodejenisdeposito').bind('change', function(){
		$.ajax({
			type: 'GET',
			url: 'ajax_nomor_rekening_deposito.php',
			data : {
				kodejenisdeposito : $(this).val()
			},
			dataType: "json",
			success: function (datas) {
				$('#nomor_rekening').val(datas.NO_REKENING);
			}
		});
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>		