<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "user_entrydata";

$userId = $_GET['userid'];

$message = 0;

if (isset($_POST['submit']))
{
	$USERNAME = $_POST['USERNAME'];
	//$PASSWORD = $_POST['PASSWORD'];
	$USERGROUP = $_POST['USERGROUP'];
	$OUTLET = $_POST['OUTLET'];

	$limit_tarik = $_POST['limit_tarik'];
	$limit_setor = $_POST['limit_setor'];

	//$PASSWORD = sha1($PASSWORD . $salt);

	$sql = "
	UPDATE passwd SET 
	USERNAME = '".$USERNAME."' ,
	USERGROUP = '".$USERGROUP."' ,
	OUTLET = '".$OUTLET."' ,
	LIMIT_TARIK = '".$limit_tarik."' ,
	LIMIT_SETOR = '".$limit_setor."'
	WHERE 1
	AND USERID = '".$userId."'
	";

	mysql_query($sql);

	$message = 1;		
}

if (isset($_POST['submit_password']))
{
	$old_PASSWORD = $_POST['old_PASSWORD'];
	$new_PASSWORD = $_POST['new_PASSWORD'];

	$sql = "SELECT * FROM passwd WHERE 1 AND USERID = '$userId'";
	$fetch = mysql_fetch_array(mysql_query($sql));

	$old_PASSWORD = sha1($old_PASSWORD . $salt);

	if ($fetch['PASSWORD'] == $old_PASSWORD && !empty($new_PASSWORD))
	{
		$new_PASSWORD = sha1($new_PASSWORD . $salt);

		$sql = "
		UPDATE passwd SET 
		PASSWORD = '".$new_PASSWORD."'
		WHERE 1
		AND USERID = '".$userId."'
		";

		mysql_query($sql);

		$message = 1;
	}
	else
	{
		$message = 2;
	}

}

$sql = "SELECT * FROM passwd WHERE 1 AND USERID = '$userId'";
$fetch = mysql_fetch_array(mysql_query($sql));

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">User Edit Data</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form form-validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="USERNAME" name="USERNAME" value="<?php echo $fetch['USERNAME']; ?>">
											<label for="USERNAME">Username</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="USERGROUP" name="USERGROUP" value="<?php echo $fetch['USERGROUP']; ?>">
											<label for="USERGROUP">User Group</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="limit_tarik" name="limit_tarik" value="<?php echo $fetch['LIMIT_TARIK']; ?>">
											<label for="limit_tarik">Limit Tarik</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="limit_setor" name="limit_setor" value="<?php echo $fetch['LIMIT_SETOR']; ?>">
											<label for="limit_setor">Limit Setor</label>
										</div>

										<div class="form-group floating-label">
											<select id="OUTLET" name="OUTLET" class="form-control" required>
												<option value="">&nbsp;</option>
												<?php 
												$sqlCabang = "SELECT * FROM kodecabang WHERE 1";
												$fetchCabang = mysql_query($sqlCabang);
												while($rowCabang = mysql_fetch_array($fetchCabang))
												{
													$selected = ($rowCabang['kode_cab'] == $fetch['OUTLET']) ? 'selected' : '';
												?>
												<option value="<?php echo $rowCabang['kode_cab']; ?>" <?php echo $selected; ?>><?php echo $rowCabang['kode_cab'] . ' - ' . $rowCabang['nama_cab']; ?></option>
												<?php
												}
												?>
											</select>
											<label for="OUTLET">Outlet/Cabang</label>
										</div>

									</div>
								</div>

							</div>

						</div>	

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>	

				</div>
			</section>	


			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Change Password</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form form-validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group floating-label">
											<input type="password" class="form-control" id="old_PASSWORD" name="old_PASSWORD">
											<label for="PASSWORD">Old Password</label>
										</div>

										<div class="form-group floating-label">
											<input type="password" class="form-control" id="new_PASSWORD" name="new_PASSWORD">
											<label for="PASSWORD">New Password</label>
										</div>

									</div>
								</div>

							</div>

						</div>	

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit_password">Submit</button>

					</form>	

				</div>
			</section>	

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "user_entrydata.php";
			?>
			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully updated. <a href="user_edit.php?userid=<?php echo $userId; ?>">See Again</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			}
			else if ($message == 2)
			{
				$linkBack = "user_entrydata.php";
			?>
			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-danger" role="alert">
								<strong>Oh snap!</strong> Change a few things up and try submitting again. <a href="user_edit.php?userid=<?php echo $userId; ?>">Try Again</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>	

<?php require_once "layouts/footer.php"; ?>