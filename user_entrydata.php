<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "user_entrydata";

$message = 0;

if (isset($_POST['submit']))
{
	$USERNAME = $_POST['USERNAME'];
	$PASSWORD = $_POST['PASSWORD'];
	$USERGROUP = $_POST['USERGROUP'];
	$OUTLET = $_POST['OUTLET'];

	$limit_tarik = $_POST['limit_tarik'];
	$limit_setor = $_POST['limit_setor'];

	$PASSWORD = sha1($PASSWORD . $salt);

	$sql = "
	INSERT INTO passwd SET 
	USERNAME = '".$USERNAME."' ,
	PASSWORD = '".$PASSWORD."' ,
	USERGROUP = '".$USERGROUP."' ,
	OUTLET = '".$OUTLET."' ,
	LIMIT_TARIK = '".$limit_tarik."' ,
	LIMIT_SETOR = '".$limit_setor."'
	";

	mysql_query($sql);

	$message = 1;		
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">User Entry Data</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form form-validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="USERNAME" name="USERNAME">
											<label for="USERNAME">Username</label>
										</div>

										<div class="form-group floating-label">
											<input type="password" class="form-control" id="PASSWORD" name="PASSWORD">
											<label for="PASSWORD">Password</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="limit_tarik" name="limit_tarik">
											<label for="limit_tarik">Limit Tarik</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="limit_setor" name="limit_setor">
											<label for="limit_setor">Limit Setor</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="USERGROUP" name="USERGROUP">
											<label for="USERGROUP">User Group</label>
										</div>

										<div class="form-group floating-label">
											<select id="OUTLET" name="OUTLET" class="form-control" required>
												<option value="">&nbsp;</option>
												<?php 
												$sqlCabang = "SELECT * FROM kodecabang WHERE 1";
												$fetchCabang = mysql_query($sqlCabang);
												while($rowCabang = mysql_fetch_array($fetchCabang))
												{
												?>
												<option value="<?php echo $rowCabang['kode_cab']; ?>"><?php echo $rowCabang['kode_cab'] . ' - ' . $rowCabang['nama_cab']; ?></option>
												<?php
												}
												?>
											</select>
											<label for="OUTLET">Outlet/Cabang</label>
										</div>

									</div>
								</div>

							</div>

						</div>	

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>	

				</div>
			</section>	

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "user_entrydata.php";
				require_once "layouts/message_success.php";
			}
			else if ($message == 2)
			{
				$linkBack = "user_entrydata.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>	

<?php require_once "layouts/footer.php"; ?>