<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "simpanan_entrydata";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	$NASABAH_ID = $_POST['NASABAH_ID'];
	$nama_nasabah = $_POST['nama_nasabah'];
	$NO_REKENING = $_POST['NO_REKENING'];
	$SUKU_BUNGA = $_POST['SUKU_BUNGA'];
	$PERSEN_PPH = $_POST['PERSEN_PPH'];
	$TGL_REGISTRASI = $_POST['TGL_REGISTRASI'];
	$MINIMUM = $_POST['MINIMUM'];
	$ADM_PER_BLN = $_POST['ADM_PER_BLN'];
	$PERIODE_ADM = $_POST['PERIODE_ADM'];

	$sqlCheck = "SELECT * FROM tabung WHERE 1 AND NASABAH_ID = '$NASABAH_ID'";
	$fetchCheck = mysql_fetch_array(mysql_query($sqlCheck));

	$cab = $kodecabang['kode_cab'];
	$userId = $user['USERID'];

	if (! $fetchCheck && ! empty($NO_REKENING) && ! empty($NASABAH_ID))
	{

		$sql = "
		INSERT INTO tabung SET 
		NASABAH_ID = '$NASABAH_ID' ,
		NO_REKENING = '$NO_REKENING' ,
		SUKU_BUNGA = '$SUKU_BUNGA' ,
		PERSEN_PPH = '$PERSEN_PPH' ,
		TGL_REGISTRASI = '$TGL_REGISTRASI' ,
		MINIMUM = '$MINIMUM' ,
		ADM_PER_BLN = '$ADM_PER_BLN' ,
		PERIODE_ADM = '$PERIODE_ADM' ,
		STATUS_AKTIF = 1 ,
		CAB = '".$cab."' ,
		USERID = '".$userId."'
		";

		mysql_query($sql);

		$message = 1;

	}
	else
	{
		$message = 2;
	}
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Simpanan Entry Data</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NASABAH_ID" name="NASABAH_ID" data-source="autosuggest_anggota.php">
												<label for="NASABAH_ID">Anggota ID</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly>
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_REKENING" name="NO_REKENING" >
												<label for="NO_REKENING">No Rekening</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="SUKU_BUNGA" name="SUKU_BUNGA" value="4">
												<label for="SUKU_BUNGA">Bunga</label>
												<p class="help-block">Default: 4%</p>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="PERSEN_PPH" name="PERSEN_PPH" value="10">
												<label for="PERSEN_PPH">Persen PPH</label>
												<p class="help-block">Default: 10%</p>
											</div>

									</div>
								</div>	

							</div>
							
							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">


											<div class="form-group floating-label">
												<input type="text" class="form-control" id="TGL_REGISTRASI" name="TGL_REGISTRASI" value="<?php echo $tglsystem; ?>" readonly>
												<label for="TGL_REGISTRASI">Tgl Registrasi</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="MINIMUM" name="MINIMUM" value="20000">
												<label for="MINIMUM">Minimum</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="ADM_PER_BLN" name="ADM_PER_BLN" value="1000">
												<label for="ADM_PER_BLN">Adm Per Bulan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="PERIODE_ADM" name="PERIODE_ADM" value="1">
												<label for="PERIODE_ADM">Periode Adm</label>
											</div>

									</div>
								</div>

							</div>

						</div>		

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "simpanan_entrydata.php";
				require_once "layouts/message_success.php";
			}
			else if ($message == 2)
			{
				$linkBack = "simpanan_entrydata.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#NASABAH_ID').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#NASABAH_ID").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#NASABAH_ID').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);

					return false;
				},
				focus: function( event, ui ) {

			        $( "#NASABAH_ID" ).val( ui.item.nasabah_id );
			        
			        return false;
			      },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>