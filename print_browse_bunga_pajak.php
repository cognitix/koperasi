<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$month = date('M Y');
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:700px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
.header-text h5, .header-text h3, .header-text h4{
    margin-top:0;
    margin-bottom:0;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div class="header-text">
			<div class="pull-left">
				<h5>Browse Bunga Pajak</h5>
                <h4 style="margin-bottom:8px;"><?php echo $month; ?></h4>
			</div>
			<div class="pull-right">

			</div>
		</div>

		<div class="clearfix"></div>

		<?php 
		$sql = "SELECT 
				tabung.*,
				nasabah.nasabah_id,
				nasabah.nama_nasabah ,
				nasabah.alamat
				FROM tabung 
				JOIN nasabah ON tabung.NASABAH_ID = nasabah.nasabah_id 
				JOIN tabtrans ON tabung.NO_REKENING = tabtrans.NO_REKENING
				WHERE 1
				AND STATUS_AKTIF = 2
				AND DATE_FORMAT(tabtrans.TGL_TRANS,'%m') = '".date('m')."'
				GROUP BY nasabah.nasabah_id
				";

		$query = mysql_query($sql);
		?>

		<div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover table-custom">
            	<tr>
                    <th style="text-align:left;">No. Anggota</th>
                    <th style="text-align:left;">Nama Anggota</th>
                    <th style="text-align:left;">No. Rekening</th>
                    <th style="text-align:left;">Total Bunga</th>
                    <th style="text-align:left;">Total Pajak</th>
                    <th style="text-align:left;">Total ADM</th>
                    <th style="text-align:left;">Saldo Nomintatif Simpanan Sukarela</th>
                </tr>
                
                <?php 
				while($fetch = mysql_fetch_array($query))
				{
				?>

				<tr>
					<td><?php echo $fetch['nasabah_id']; ?></td>
					<td><?php echo $fetch['nama_nasabah']; ?></td>
					<td><?php echo $fetch['NO_REKENING']; ?></td>
					<td><?php echo number_format($fetch['BUNGA_BLN_INI'],2,'.',','); ?></td>
					<td><?php echo number_format($fetch['PAJAK_BLN_INI'],2,'.',','); ?></td>
					<td><?php echo number_format($fetch['ADM_BLN_INI'],2,'.',','); ?></td>
					<td><?php echo number_format($fetch['simpanan_sukarela'],2,'.',','); ?></td>
				</tr>

				<?php
				}
				?>

            </table>
        </div>

        <div class="clearfix"></div>

	</div>
</div>	

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>