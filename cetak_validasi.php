<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tabTransId = $_GET['tabTransId'];

function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

$sql = "SELECT 
		tabtrans.*,
		kodetranstabungan.DESKRIPSI_TRANS ,
		nasabah.nama_nasabah ,
		passwd.USERNAME
		FROM 
		tabtrans 
		JOIN kodetranstabungan ON 
			kodetranstabungan.KODE_TRANS = tabtrans.KODE_TRANS
		JOIN tabung ON 
			tabung.NO_REKENING = tabtrans.NO_REKENING
		JOIN nasabah ON 
			nasabah.nasabah_id = tabung.NASABAH_ID
		JOIN passwd ON 
			passwd.USERID = tabtrans.NO_TELLER
		WHERE 1 
		AND TABTRANS_ID = $tabTransId";
$fetch = mysql_fetch_array(mysql_query($sql));

?>

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:8px;
}

.table > tbody > tr > td{
	border-top:0;
}
.body-print h3{
	margin-top:0;
}
.card-body{
	padding:10px;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<h3 class="text-center">Setoran Tunai</h3>
		
		<table class="table">
			<tr>
				<td>
					<div class="clearfix">
						<div class="pull-left"> Tanggal </div>
						<div class="pull-right"> <?php echo date("d/m/Y", strtotime($fetch['TGL_TRANS'])); ?> </div>
					</div>
					<div class="clearfix">
						<div class="pull-left"> No Rekening Tujuan </div>
						<div class="pull-right"> <?php echo $fetch['NO_REKENING']; ?> </div>
					</div>
					<div class="clearfix">
						<div class="pull-left"> Nama Pemilik Rekening </div>
						<div class="pull-right"> <?php echo strtoupper($fetch['nama_nasabah']); ?> </div>
					</div>
					<div class="clearfix">
						<div class="pull-left"> Nominal </div>
						<div class="pull-right"> <?php echo number_format($fetch['SALDO_TRANS'],2,'.',','); ?> </div>
					</div>
					<div class="clearfix">
						<div class="pull-left"> Terbilang </div>
						<div class="pull-right"> <?php echo ucwords(Terbilang($fetch['SALDO_TRANS'])); ?> </div>
					</div>
					<div class="clearfix">
						<div class="pull-left"> Keterangan </div>
						<div class="pull-right"> <?php echo ucwords($fetch['keterangan']); ?> </div>
					</div>
				</td>
				<td rowspan="2">
					<div class="col-xs-4">
						<div class="clearfix">
							<div class="pull-left"> Reff <span style="margin-left:10px;"><?php echo $fetch['kuitansi']; ?></span> </div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<?php echo substr(strtoupper($fetch['USERNAME']), 0,3) . 
							'/' . 
							$fetch['KODE_TRANS'] . '-' . $fetch['DESKRIPSI_TRANS'] .
							'/' .
							$fetch['kuitansi'] .
							'/' .
							date("Y-m-d", strtotime($fetch['TGL_TRANS']))
					; ?>
				</td>
			</tr>
			<tr>
				<td>
					<div>Nama Penyetor: </div>
					<div>Telpon Penyetor: </div>
					<div>Alamat Penyetor: </div>
				</td>
				<td>
					<div class="clearfix">
						<div class="pull-left">
							<p>Teller</p>
							<br /><br />
							<?php echo substr(strtoupper($fetch['USERNAME']), 0,3); ?>
						</div>
						<div class="pull-right">
							<p>Tanda Tangan Penyetor</p>
						</div>
					</div>
				</td>
			</tr>
		</table>

	</div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>