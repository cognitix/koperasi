<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "user_viewall";

$userid = $_GET['userid'];

if (isset($_POST['submit']))
{
	$menus = $_POST['menus'];

	/*echo '<pre>';
	print_r($menus);
	echo '</pre>';*/

	$sql = "DELETE FROM user_role_maps 
			WHERE 1 
			AND user_id = '".$userid."'
			";

	foreach($menus as $menu)
	{
		$sqlCheck = "SELECT * FROM user_role_maps WHERE 1 AND menu_id = '".$menu."' AND user_id = '".$userid."'";
		$fetchCheck = mysql_fetch_array(mysql_query($sqlCheck));

		if (!$fetchCheck)
		{
			$sql = "INSERT INTO user_role_maps SET 
				user_id = '".$userid."' ,
				menu_id = '".$menu."'
				";
		}
		else
		{
			$sql = "UPDATE user_role_maps SET 
				user_id = '".$userid."' ,
				menu_id = '".$menu."'
				WHERE 1
				AND id = '".$fetchCheck['id']."'
				";
		}
		

		mysql_query($sql);
	}
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			<section class="style-default-bright">
				<div class="section-header">
					<h2 class="text-primary">User Roles</h2>
				</div>
				<div class="section-body">
					<div class="row">
						<div class="col-lg-12">

<form class="form" method="post" enctype="multipart/form-data">

<?php 
$sqlMenuRoles = "SELECT * FROM user_role_maps WHERE 1 AND user_id = '".$userid."'";
$queryMenuRoles = mysql_query($sqlMenuRoles);
$menuRoles = array();
while($rowMenuRoles = mysql_fetch_array($queryMenuRoles))
{
	$menuRoles[] = $rowMenuRoles['menu_id'];
}

$sqlMenus = "SELECT * FROM menus";
$queryMenus = mysql_query($sqlMenus);

$menus = array();

while($row = mysql_fetch_array($queryMenus))
{
	$menus[] = array(
		'id' => $row['id'] ,
		'name' => $row['name'] ,
		'parent_id' => $row['parent_id'] ,
		'url' => $row['url']
	);
}

function has_children1($rows,$id) {
  	foreach ($rows as $row) {
    	if ($row['parent_id'] == $id)
      		return true;
  	}
  	return false;
}

function build_menu1($rows,$parent = 0, $main = 0, $menu, $menuRoles)
{  
	$result = "<ul>";

  	foreach ($rows as $row)
  	{
    	if ($row['parent_id'] == $parent)
    	{
    		if ($parent == 0)
    		{
	      		$result .= '<li>';

	      		$result .= '<span class="title">'.$row['name'].'</span>';
			}
			else
			{
				if ($row['url'])
				{
					$class = (!empty($menu) && $menu == str_replace(".php", "", $row['url'])) ? 'active' : '';

					$checked = (in_array($row['id'], $menuRoles)) ? 'checked' : '';

					$result .= '<li><label><input type="checkbox" name="menus[]" value="'.$row['id'].'" '.$checked.' /> <span class="title">'.$row['name'].'</span></label>';
				}
				else
				{
					$result .= '<li>
									<span class="title">'.$row['name'].'</span>';
				}
			}
      		
      		if (has_children1($rows, $row['id']))
        		$result.= build_menu1($rows, $row['id'], 0, $menu, $menuRoles);
      	
      		$result.= "</li>";
    	}
  	}
 
  	$result.= "</ul>";

  	return $result;
}

echo build_menu1($menus, 0, 1, $menu, $menuRoles);

?>

<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

</form>
						</div>
					</div>
				</div>
			</section>
		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>	


<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>