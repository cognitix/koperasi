<?php 
require_once "connect.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tabTransId = $_GET['tabTransId'];

$sql = "SELECT 
		tabtrans.*,
		kodetranstabungan.DESKRIPSI_TRANS ,
		nasabah.nama_nasabah ,
		passwd.USERNAME
		FROM 
		tabtrans 
		JOIN kodetranstabungan ON 
			kodetranstabungan.KODE_TRANS = tabtrans.KODE_TRANS
		JOIN tabung ON 
			tabung.NO_REKENING = tabtrans.NO_REKENING
		JOIN nasabah ON 
			nasabah.nasabah_id = tabung.NASABAH_ID
		JOIN passwd ON 
			passwd.USERID = tabtrans.NO_TELLER
		WHERE 1 
		AND TABTRANS_ID = $tabTransId";
$fetch = mysql_fetch_array(mysql_query($sql));

?>

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:8px;
}
</style>

<div class="body-print">
<?php
echo date("d/m/Y", strtotime($fetch['TGL_TRANS'])) . " " . substr(strtoupper($fetch['USERNAME']), 0,3) . " " . $fetch['KODE_TRANS'] . "-" . $fetch['DESKRIPSI_TRANS'] . "<br />";
echo $fetch['NO_REKENING'] . " " . strtoupper($fetch['nama_nasabah']) . "<br />";
echo $fetch['kuitansi'] . " " . number_format($fetch['SALDO_TRANS'],2,'.',',');
?>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>



