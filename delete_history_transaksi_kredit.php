<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "history_transaksi_kredit";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$trans_id = $_GET['trans_id'];

$message = 0;

if (isset($_GET['trans_id']))
{
	$sqlTellerTrans = "SELECT * FROM tellertrans WHERE 1 AND trans_id = '".$trans_id."'";
	$queryTellerTrans = mysql_query($sqlTellerTrans);
	$fetchTellerTrans = mysql_fetch_array($queryTellerTrans);

	$NO_BUKTI = $fetchTellerTrans['NO_BUKTI'];

	$sqlKreTrans = "SELECT * FROM kretrans WHERE 1 AND KUITANSI = '".$NO_BUKTI."'";
	$queryKreTrans = mysql_query($sqlKreTrans);
	$fetchKreTrans = mysql_fetch_array($queryKreTrans);

	$NO_BUKTI = $fetchKreTrans['KUITANSI'];
	$jumlah = $fetchTellerTrans['saldo_trans'];
	$nomor_rekening = $fetchKreTrans['NO_REKENING'];

	// delete tab trans
	$sql = "DELETE FROM kretrans WHERE 1 AND KUITANSI = '".$NO_BUKTI."'";
	mysql_query($sql);

	// delete teller trans
	$sql = "DELETE FROM tellertrans WHERE 1 AND NO_BUKTI = '".$NO_BUKTI."'";
	mysql_query($sql);

	$sqlKredit = "SELECT * FROM kredit WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
	$queryKredit = mysql_query($sqlKredit);
	$fetchKredit = mysql_fetch_array($queryKredit);

	$SALDO_NOMINATIF = $fetchKredit['SALDO_NOMINATIF'] + $jumlah;
	$SALDO_AKHIR = $fetchKredit['SALDO_AKHIR'] + $jumlah;

	$sql = "
	UPDATE kredit SET 
	SALDO_AKHIR = '".$SALDO_AKHIR."' ,
	SALDO_NOMINATIF = '".$SALDO_NOMINATIF."'
	WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
	";
	mysql_query($sql);

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
			?>

			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully delete this item.
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<?php
			}
			?>

		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>
	