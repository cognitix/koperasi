<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "history_transaksi";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$trans_id = $_GET['trans_id'];

$message = 0;

if (isset($_GET['trans_id']))
{

	$sqlTransSimpan = "
	SELECT
	trans_master.tgl_trans ,
	trans_master.kode_jurnal ,
	trans_master.no_bukti ,
	trans_master.NOMINAL ,
	trans_detail.trans_id ,
	trans_detail.URAIAN ,
	trans_detail.kode_perk ,
	trans_detail.debet ,
	trans_detail.kredit ,
	trans_detail.master_id ,
	trans_detail.saldo_akhir ,
	perkiraan.nama_perk ,
	perkiraan.saldo_debet ,
	perkiraan.saldo_kredit
	FROM
	trans_detail
	JOIN trans_master ON trans_detail.master_id = trans_master.trans_id
	JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
	WHERE 1
	AND trans_detail.trans_id = '".$trans_id."'
	";

	$queryTransSimpan = mysql_query($sqlTransSimpan);
	$resultSimpan = mysql_fetch_array($queryTransSimpan);

	$selisih = $jumlah = 0;

	if ((int) $resultSimpan['debet'])
	{
		$selisih = $resultSimpan['NOMINAL'];
		$posisi = "debet";
		$jumlah = 0;		
	}
	else if ((int) $resultSimpan['kredit'])
	{
		$selisih = $resultSimpan['NOMINAL'];
		$posisi = "kredit";
		$jumlah = 0;
	}

	$nominal = $resultSimpan['NOMINAL'] - $selisih;
	$saldo_akhir = $resultSimpan['saldo_akhir'] - $selisih;

	$sqlInsertTransMaster = "
	UPDATE trans_master SET 
	NOMINAL = '".$nominal."'
	WHERE 1
	AND trans_id = '".$resultSimpan['master_id']."'
	";

	mysql_query($sqlInsertTransMaster);

	$master_id = $resultSimpan['master_id'];

	$sqlTransDetail = "
	UPDATE trans_detail SET 
	master_id = '".$master_id."' ,
	".$posisi." = '".$jumlah."' ,
	saldo_akhir = '".$saldo_akhir."'
	WHERE 1
	AND trans_id = '".$resultSimpan['trans_id']."' 
	";

	mysql_query($sqlTransDetail);

	$sqlTransDetail = "
	SELECT 
	trans_detail.* ,
	
	perkiraan.kode_perk ,
	perkiraan.nama_perk ,
	perkiraan.saldo_debet ,
	perkiraan.saldo_kredit

	FROM trans_detail 
	JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
	WHERE 1 
	AND master_id = '".$master_id."'
	AND trans_id = '".$resultSimpan['trans_id']."' 
	";

	$queryTransDetail = mysql_query($sqlTransDetail);

	while($fetchTransDetail = mysql_fetch_array($queryTransDetail))
	{
		$saldo_debet = $fetchTransDetail['saldo_debet'] + $fetchTransDetail['debet'] - $fetchTransDetail['kredit'];
		$saldo_kredit = $fetchTransDetail['saldo_kredit'] + $fetchTransDetail['debet'] - $fetchTransDetail['kredit'];
		$saldo_akhir = $saldo_debet - $saldo_kredit;

		$kode_perk = $fetchTransDetail['kode_perk'];

		$sql = "
		UPDATE perkiraan SET 
		saldo_debet = '".$saldo_debet."' ,
		saldo_kredit = '".$saldo_kredit."' ,
		saldo_akhir = '".$saldo_akhir."'
		WHERE 1
		AND kode_perk = '".$kode_perk."'
		";

		mysql_query($sql);
	}

	$sqlTransDetail = "
	DELETE FROM trans_detail 
	WHERE 1
	AND trans_id = '".$resultSimpan['trans_id']."' 
	";

	mysql_query($sqlTransDetail);

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
				$linkBack = "edit_history_transaksi.php";
				require_once "layouts/message_success.php";
			}
			?>

		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>
	