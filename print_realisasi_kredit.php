<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$kretransId = $_GET['kretransId'];

$sql = "
SELECT  
kretrans.* ,
kredit.JML_PINJAMAN ,
nasabah.nama_nasabah ,
nasabah.NO_PASSPORT ,
kredit.PROVISI ,
kredit.ADM
FROM kretrans 
JOIN kredit ON kredit.NO_REKENING = kretrans.NO_REKENING
JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id 
WHERE 1 
AND KRETRANS_ID = '".$kretransId."'";
$fetch = mysql_fetch_array(mysql_query($sql));

function Terbilang($x)
{
  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . "belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:650px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div>
			<div class="pull-left">
				<div>TANDA TERIMA PENCAIRAN PINJAMAN</div>
				<div>KSP ADIL MAKMUR FAJAR</div>
			</div>
			<div class="pull-right">
				<table id="datatable1" class="table table-bordered table-custom-border">
					<tbody>
						<tr>
							<td>Adm Kredit</td>
							<td>Akunting</td>
							<td>SPV</td>
						</tr>
						<tr>
							<td class="tanda-tangan"></td>
							<td class="tanda-tangan"></td>
							<td class="tanda-tangan"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="clearfix"></div>

		<div>
			<div class="pull-left">
				<table id="datatable1" class="table table-custom2">
					<tbody>
						<tr>
							<td>Tanggal</td>
							<td>:</td>
							<td><?php echo date('d/m/Y', strtotime($fetch['TGL_TRANS'])); ?></td>
						</tr>
						<tr>
							<td>No. Kuitansi</td>
							<td>:</td>
							<td><?php echo $fetch['KUITANSI']; ?></td>
						</tr>
						<tr>
							<td>No. Diterima Oleh</td>
							<td>:</td>
							<td><?php echo $fetch['nama_nasabah']; ?></td>
						</tr>
						<tr>
							<td>No. Rekening</td>
							<td>:</td>
							<td><?php echo $fetch['NO_REKENING']; ?></td>
						</tr>
						<tr>
							<td>No. Passport</td>
							<td>:</td>
							<td><?php echo $fetch['NO_PASSPORT']; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				<table id="datatable1" class="table table-custom2" style="width:250px;">
					<tbody>
						<tr>
							<td style="text-align:center;" colspan="3"> ---------------------- REALISASI ------------------------ </td>
						</tr>
						<tr>
							<td>Jumlah Pinjaman</td>
							<td>:</td>
							<td><?php echo number_format($fetch['JML_PINJAMAN'],2,'.',','); ?></td>
						</tr>
						<tr>
							<td>Provisi</td>
							<td>:</td>
							<td><?php echo number_format($fetch['PROVISI'],2,'.',','); ?></td>
						</tr>
						<tr>
							<td>Notariel</td>
							<td>:</td>
							<td><?php echo number_format(0,2,'.',','); ?></td>
						</tr>
						<tr>
							<td>Materai</td>
							<td>:</td>
							<td><?php echo number_format(0,2,'.',','); ?></td>
						</tr>
						<tr>
							<td>Administrasi</td>
							<td>:</td>
							<td><?php echo number_format($fetch['ADM'],2,'.',','); ?></td>
						</tr>
						<tr>
							<td>Lain-Lain</td>
							<td>:</td>
							<td><?php echo number_format(0,2,'.',','); ?></td>
						</tr>
						<tr>
							<td style="text-align:center;" colspan="3"> ---------------------------------------------------------------- </td>
						</tr>
						<tr>
							<td>Jumlah Diterima</td>
							<td>:</td>
							<td><?php echo number_format($fetch['JML_PINJAMAN'],2,'.',','); ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="clearfix"></div>

		<table id="datatable1" class="table table-custom2" style="width:500px;">
			<tbody>
				<tr>
					<td>Terbilang</td>
					<td>:</td>
					<td><?php echo ucwords(Terbilang($fetch['JML_PINJAMAN'])); ?></td>
				</tr>
			</tbody>
		</table>

		<div class="clearfix"></div>

		<div style="margin:0 auto;">
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Debitur</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:95px;text-align:center;border:1px dashed #000;">
									
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Teller</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:95px;text-align:center;border:1px dashed #000;">
									
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>Jakarta,  
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">KSP ADIL MAKMUR FAJAR<br />Mengetahui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:62px;text-align:center;border:1px dashed #000;">
									
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>

	</div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>



<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>