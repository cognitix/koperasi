<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "browse_bunga_pajak_deposito";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Browse Bunga dan Pajak</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<div class="input-group date" id="TGL_container">
													<div class="input-group-content">
														<input type="text" class="form-control" name="tanggal" value="<?php echo date("m-Y"); ?>">
														<label>Tanggal</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>

					<hr />

					<?php 

					if (!empty($_GET['tanggal']))
					{

						$tanggal = $_GET['tanggal'];

						$sql = "
						SELECT
						deposito.* ,
						nasabah.nama_nasabah
						FROM deposito 
						JOIN nasabah ON nasabah.nasabah_id = deposito.NASABAH_ID
						WHERE 1 
						AND DATE_FORMAT(deposito.TGL_REGISTRASI,'%m-%Y') = '".$tanggal."'
						";

						$query = mysql_query($sql);
					?>

					<section class="style-default-bright">

						<form method="post" >

							<div class="row">
								<div class="col-lg-12">

									<div class="table-responsive">
										<table id="datatable1" class="table table-striped table-hover">
											<thead>
												<tr>
													<th>Tanggal Registrasi</th>
													<th>No. Rekening</th>
													<th>Nasabah Id/Nama</th>
													<th>Suku Bunga</th>
													<th>Persen PPh</th>
													<th>Bunga Bulan Ini</th>
													<th>Pajak Bulan Ini</th>
													<th>Jumlah Deposito</th>
													<th>JKW</th>
													<th>Tanggal Jatuh Tempo</th>
												</tr>
											</thead>
											<tbody>

												<?php 
												while($fetch = mysql_fetch_array($query))
												{
												?>

												<tr>
													<td><?php echo date('d M Y', strtotime($fetch['TGL_REGISTRASI'])); ?></td>
													<td><?php echo $fetch['NO_REKENING']; ?></td>
													<td><?php echo $fetch['NASABAH_ID']; ?> / <?php echo $fetch['nama_nasabah']; ?></td>
													<td><?php echo $fetch['SUKU_BUNGA']; ?></td>
													<td><?php echo $fetch['PERSEN_PPH']; ?></td>
													<td><?php echo number_format($fetch['BUNGA_BLN_INI'],0,'.',','); ?></td>
													<td><?php echo number_format($fetch['PAJAK_BLN_INI'],0,'.',','); ?></td>
													<td><?php echo number_format($fetch['JML_DEPOSITO'],0,'.',','); ?></td>
													<td><?php echo $fetch['JKW']; ?></td>
													<td><?php echo date('d M Y', strtotime($fetch['TGL_JT'])); ?></td>
												</tr>

												<?php
												}
												?>

											</tbody>
										</table>

									</div>
								</div>
							</div>

						</form>

						<a href="print_browse_bunga_pajak_deposito.php?tgl_transaksi=<?php echo $_GET['tanggal']; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print</a>

					</section>

					<?php 
					}
					?>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "print_buku_simpanan.php";
				require_once "layouts/message_success.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

	$('#TGL_container').datepicker({
		autoclose: true, 
		todayHighlight: true, 
		minViewMode: 1,
		format: "mm-yyyy"
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>