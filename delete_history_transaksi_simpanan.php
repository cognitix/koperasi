<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "history_transaksi_simpanan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$TABTRANS_ID = $_GET['TABTRANS_ID'];

$message = 0;

if (isset($_GET['TABTRANS_ID']))
{

	$sqlTabTrans = "SELECT * FROM tabtrans WHERE 1 AND TABTRANS_ID = '".$TABTRANS_ID."'";
	$queryTabTrans = mysql_query($sqlTabTrans);
	$fetchTabTrans = mysql_fetch_array($queryTabTrans);
	
	$NO_BUKTI = $fetchTabTrans['kuitansi'];
	$jumlah = $fetchTabTrans['SALDO_TRANS'];
	$KODE_TRANS = $fetchTabTrans['KODE_TRANS'];
	$nomor_rekening = $fetchTabTrans['NO_REKENING'];

	// delete tab trans
	$sql = "DELETE FROM tabtrans WHERE 1 AND TABTRANS_ID = '".$TABTRANS_ID."'";
	mysql_query($sql);

	// delete teller trans
	$sql = "DELETE FROM tellertrans WHERE 1 AND NO_BUKTI = '".$NO_BUKTI."'";
	mysql_query($sql);

	$sqlTabung = "SELECT * FROM tabung WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
	$fetchTabung = mysql_fetch_array(mysql_query($sqlTabung));

	// update tabung
	if ($KODE_TRANS == "01")
	{
		$SALDO_SETORAN = $fetchTabung['SALDO_SETORAN'] - $jumlah;
		$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] - $jumlah;
		$simpanan_sukarela = $fetchTabung['simpanan_sukarela'] - $jumlah;

		$sql = "
		UPDATE tabung SET 
		SALDO_SETORAN = '".$SALDO_SETORAN."' ,
		SALDO_NOMINATIF = '".$SALDO_NOMINATIF."' ,
		simpanan_sukarela = '".$simpanan_sukarela."'
		WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
		";
	}
	else if ($KODE_TRANS == "11")
	{
		$simpanan_pokok = $fetchTabung['simpanan_pokok'] - $jumlah;

		$sql = "
		UPDATE tabung SET 
		simpanan_pokok = '".$simpanan_pokok."'
		WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
		";
	}
	else if ($KODE_TRANS == "12")
	{
		$simpanan_wajib = $fetchTabung['simpanan_wajib'] - $jumlah;

		$sql = "
		UPDATE tabung SET 
		simpanan_wajib = '".$simpanan_wajib."'
		WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
		";
	}
	else if ($KODE_TRANS == "13")
	{
		$SALDO_SETORAN = $fetchTabung['SALDO_SETORAN'] - $jumlah;
		$SALDO_NOMINATIF = $fetchTabung['SALDO_NOMINATIF'] - $jumlah;
		$simpanan_sukarela = $fetchTabung['simpanan_sukarela'] - $jumlah;

		$sql = "
		UPDATE tabung SET 
		SALDO_SETORAN = '".$SALDO_SETORAN."' ,
		SALDO_NOMINATIF = '".$SALDO_NOMINATIF."' ,
		simpanan_sukarela = '".$simpanan_sukarela."'
		WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
		";
	}
	else if ($KODE_TRANS == "14")
	{
		$simpanan_wajib = $fetchTabung['simpanan_wajib'] - $jumlah;

		$sql = "
		UPDATE tabung SET 
		simpanan_wajib = '".$simpanan_wajib."'
		WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
		";
	}

	mysql_query($sql);

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
			?>

			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully delete this item.
							</div>
						</div>
					</div>
				</div>
			</section>
			
			<?php
			}
			?>

		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>
	