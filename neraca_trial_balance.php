<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "neraca_trial_balance";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl1 = date("Y-m-d", strtotime("-14 day"));
$tgl2 = date("Y-m-d");
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Neraca Trial Balance / Neraca Percobaan</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data" action="print_neraca_trial_balance.php">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl1" name="tgl1" value="<?php echo $tgl1; ?>" required>
														<label for="tgl1">From</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl2" name="tgl2" value="<?php echo $tgl2; ?>" required>
														<label for="tgl2">To</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Search</button>

					</form>

					<hr />

					<?php 
					if (isset($_POST['submit']))
					{
						$tgl1 = $_POST['tgl1'];
						$tgl2 = $_POST['tgl2'];	

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

					?>

					<section class="style-default-bright">

						<div class="row">
							<div class="col-lg-12">

								<div><strong>Periode : <?php echo date('d M Y', strtotime($tgl1)); ?> s/d <?php echo date('d M Y', strtotime($tgl2)); ?></strong></div>

								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Kode Intern</th>
												<th>Nama Perkiraan</th>
												<th>Saldo Awal</th>
												<th>Debet</th>
												<th>Kredit</th>
												<th>Saldo Akhir</th>
											</tr>
										</thead>
										<tbody>

											<?php 

											$totalPendapatan = $totalBiaya = 0;

											$jmlSaldoAwal = $jmlSaldoAkhir = 0;

											while($results = mysql_fetch_array($queryPerkiraan))
											{


												$sqlBefore = "
												SELECT 
												*
												FROM trans_detail
												JOIN trans_master ON 
													 trans_detail.master_id = trans_master.trans_id
												WHERE 1
												AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
												AND trans_detail.kode_perk = '".$results['kode_perk']."'
												ORDER BY trans_master.tgl_trans DESC
												";
												$queryBefore = mysql_query($sqlBefore);

												$saldoAwal = 0;
												$debet = $kredit = 0;
												$totalDebet = $totalKredit = 0;
												while($resultBefore = mysql_fetch_array($queryBefore))
												{
													$debet = $resultBefore['debet'];
													$totalDebet += $debet;

													$kredit = $resultBefore['kredit'];
													$totalKredit += $kredit;

													$saldoAwal += $debet - $kredit;
												}

												$sql = "
												SELECT 
												*
												FROM trans_detail
												JOIN trans_master ON 
													 trans_detail.master_id = trans_master.trans_id
												WHERE 1
												AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
												AND trans_detail.kode_perk = '".$results['kode_perk']."'
												ORDER BY trans_master.tgl_trans DESC
												";

												$query = mysql_query($sql);

											?>

											<?php 
											$saldo = $saldoAwal;
											$debet = $kredit = 0;
											$totalDebet = $totalKredit = 0;
											while($result1 = mysql_fetch_array($query))
											{
												$debet = $result1['debet'];
												$totalDebet += $debet;

												$kredit = $result1['kredit'];
												$totalKredit += $kredit;

												$saldo += $debet - $kredit;
											}

											if (! $results['kode_perk']) continue;

											$jmlSaldoAwal += $saldoAwal;
											$jmlSaldoAkhir += $saldo;

											if (isset($results['kode_perk'][0]) && 
												isset($results['kode_perk'][1]) && 
												$results['kode_perk'][0] == "4" && 
												$results['kode_perk'][1] == "0")
											{
												$totalPendapatan += $saldo;
											}

											if (isset($results['kode_perk'][0]) && 
												isset($results['kode_perk'][1]) && 
												$results['kode_perk'][0] == "5" && 
												$results['kode_perk'][1] == "0")
											{
												$totalBiaya += $saldo;
											}
											?>

											<tr>
												<td><?php echo $results['kode_perk']; ?></td>
												<td><?php echo $results['nama_perk']; ?></td>
												<td>
													<?php 
													if ($saldoAwal < 0)
													{
														echo "( ".number_format(abs($saldoAwal),2,'.',',')." )";
													}
													else
													{
														echo ((int)$saldoAwal != 0) ? number_format($saldoAwal,2,'.',',') : ''; 
													}
													?>
												</td>
												<td><?php //echo number_format($totalDebet,2,'.',','); ?></td>
												<td><?php //echo number_format($totalKredit,2,'.',','); ?></td>
												<td>
													<?php 
													if ($saldo < 0)
													{
														echo "( ".number_format(abs($saldo),2,'.',',')." )";
													}
													else
													{
														echo ((int)$saldo != 0) ? number_format($saldo,2,'.',',') : ''; 
													}
													?>
												</td>
											</tr>

											<?php
											}
											?>

											<tr>
												<td></td>
												<td></td>
												<td>
													<?php 
													if ($jmlSaldoAwal < 0)
													{
														echo "( ".number_format(abs($jmlSaldoAwal),2,'.',',')." )";
													}
													else
													{
														echo ((int)$jmlSaldoAwal != 0) ? number_format($jmlSaldoAwal,2,'.',',') : ''; 
													}
													?>
												</td>
												<td><?php //echo number_format($totalDebet,2,'.',','); ?></td>
												<td><?php //echo number_format($totalKredit,2,'.',','); ?></td>
												<td>
													<?php 
													if ($jmlSaldoAkhir < 0)
													{
														echo "( ".number_format(abs($jmlSaldoAkhir),2,'.',',')." )";
													}
													else
													{
														echo ((int)$jmlSaldoAkhir != 0) ? number_format($jmlSaldoAkhir,2,'.',',') : ''; 
													}
													?>
												</td>
											</tr>

											<tr>
												<td>Total Pendapatan</td>
												<td></td>
												<td>
													<?php 
													if ($totalPendapatan < 0)
													{
														echo "( ".number_format(abs($totalPendapatan),2,'.',',')." )";
													}
													else
													{
														echo ((int)$totalPendapatan != 0) ? number_format($totalPendapatan,2,'.',',') : ''; 
													}
													?>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
											</tr>

											<tr>
												<td>Total Biaya</td>
												<td></td>
												<td>
													<?php 
													if ($totalBiaya < 0)
													{
														echo "( ".number_format(abs($totalBiaya),2,'.',',')." )";
													}
													else
													{
														echo ((int)$totalBiaya != 0) ? number_format($totalBiaya,2,'.',',') : ''; 
													}
													?>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
											</tr>

											<tr>
												<td>Laba (Rugi)</td>
												<td></td>
												<td>
													<?php 

													$labaRugi = $totalBiaya + $totalPendapatan;

													if ($labaRugi < 0)
													{
														echo "( ".number_format(abs($labaRugi),2,'.',',')." )";
													}
													else
													{
														echo ((int)$labaRugi != 0) ? number_format($labaRugi,2,'.',',') : ''; 
													}
													?>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
											</tr>

											<tr>
												<td>Pajak</td>
												<td></td>
												<td>
													<?php 
													$pajak = 0.00;
													echo number_format(($pajak),2,'.',',');
													?>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
											</tr>

											<tr>
												<td>Laba (Rugi) Setelah Pajak</td>
												<td></td>
												<td>
													<?php 
													$labaRugiPajak = $labaRugi + $pajak;
													echo number_format(($labaRugiPajak),2,'.',',');
													?>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
											</tr>

										</tbody>
									</table>
								</div>

							</div>
						</div>

					</section>

					<?php 
					}
					?>

				</div>	
			</section>

			<?php require_once "layouts/home/menus.php"; ?>

		</div>

	</div>
	
<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<script type="text/javascript">
$(function(){
	
	$('#tgl1').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$('#tgl2').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>		