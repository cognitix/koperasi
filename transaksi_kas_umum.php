<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "transaksi_kas_umum";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

$trans_id = 0;

if (isset($_POST['submit']))
{
	$tgl_trans = $_POST['tgl_transaksi'];
	$kode_jurnal = $_POST['kode_jurnal'];
	$NO_BUKTI = $_POST['NO_BUKTI'];
	$keterangan = $_POST['keterangan'];
	$jumlah = $_POST['jumlah'];

	$GL_TRANS = $_POST['GL_TRANS'];
	
	$kode_perk = $_POST['kode_perk'];
	$nama_perk = $_POST['nama_perk'];
	
	$saldo_awal = $_POST['saldo_awal'];
	$saldo_kredit = $_POST['saldo_kredit'];
	
	$kode_alt = $_POST['kode_alt'];
	$kode_induk = $_POST['kode_induk'];
	
	$saldo_debet = $_POST['saldo_debet'];
	$saldo_akhir = $_POST['saldo_akhir'];

	$modul = 'PC';
	$tob = 'T';

	if ($kode_jurnal == "KM") $my_kode_trans = 200;
	if ($kode_jurnal == "KK") $my_kode_trans = 300;

	$userId = $user['USERID'];
	$cab = $kodecabang['kode_cab'];

	$sql = "
	INSERT INTO tellertrans SET 
	modul = '".$modul."' ,
	tgl_trans = '".$tgl_trans."' ,
	kode_jurnal = '".$kode_jurnal."' ,
	NO_BUKTI = '".$NO_BUKTI."' ,
	uraian = '".$keterangan."' ,
	my_kode_trans = '".$my_kode_trans."' ,
	saldo_trans = '".$jumlah."' ,
	tob = '".$tob."' ,
	userid = '".$userId."' ,
	GL_TRANS = '".$GL_TRANS."' ,
	cab = '".$cab."' ,
	USERAPP = '".$userId."'
	";

	$result = mysql_query($sql);

	$trans_id = mysql_insert_id();

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Transaksi Kas Umum</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php echo $tglsystem; ?>" 
														required
														<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
														readonly 
														<?php } ?>
														>
														<label for="tgl1">Tanggal Transaksi</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<select id="kode_jurnal" name="kode_jurnal" class="form-control">
													<option value="">&nbsp;</option>
													<option value="KM">KM</option>
													<option value="KK">KK</option>
												</select>
												<label for="kode_jurnal">Kode Jurnal</label>
											</div>

											<?php 
											$sqlTabTrans = "SELECT NO_BUKTI FROM tellertrans WHERE 1 AND kode_jurnal IN ('KM', 'KK') ORDER by trans_id desc limit 1";
											$fetchTabTrans = mysql_fetch_array(mysql_query($sqlTabTrans));
											$idMax = (int) str_replace("K-", "", $fetchTabTrans['NO_BUKTI']);
											$idMax++;
											$kuitansi = "K-" . sprintf("%05s", $idMax);
											?>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_BUKTI" name="NO_BUKTI" readonly value="<?php echo $kuitansi; ?>" />
												<label for="NO_BUKTI">Nomor Bukti</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="keterangan" name="keterangan">
												<label for="keterangan">Keterangan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="jumlah" name="jumlah">
												<label for="jumlah">Jumlah / Saldo Transfer</label>
											</div>

									</div>
								</div>	

							</div>
							
							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">


											<div class="form-group floating-label">
												<input type="text" class="form-control" id="GL_TRANS" name="GL_TRANS" data-source="autosuggest_gltrans.php">
												<label for="GL_TRANS">GL Trans</label>
											</div>

											<div class="clearfix"></div>

											<div class="col-lg-6">
												<div class="form-group">
													<input type="text" class="form-control" id="kode_perk" name="kode_perk" readonly>
													<label for="kode_perk">Kode Perkiraan</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="nama_perk" name="nama_perk" readonly>
													<label for="nama_perk">Nama Perkiraan</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="saldo_awal" name="saldo_awal" readonly>
													<label for="saldo_awal">Saldo Awal</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="saldo_kredit" name="saldo_kredit" readonly>
													<label for="saldo_kredit">Saldo Kredit</label>
												</div>
											</div>

											<div class="col-lg-6">
												<div class="form-group">
													<input type="text" class="form-control" id="kode_alt" name="kode_alt" readonly>
													<label for="kode_alt">Kode Alt</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="kode_induk" name="kode_induk" readonly>
													<label for="kode_induk">Kode Induk</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="saldo_debet" name="saldo_debet" readonly>
													<label for="saldo_debet">Saldo Debet</label>
												</div>
												<div class="form-group">
													<input type="text" class="form-control" id="saldo_akhir" name="saldo_akhir" readonly>
													<label for="saldo_akhir">Saldo Akhir</label>
												</div>
											</div>

											<div class="clearfix"></div>

									</div>
								</div>

							</div>

						</div>		

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "transaksi_kas_umum.php";
				require_once "layouts/message_success.php";
			?>

			<section>
				<div class="section-body contain-lg">
					<a href="print_transaksi_kas_umum.php?tabTransId=<?php echo $trans_id; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print Transaksi Kas Umum</a>
				</div>
			</section>

			<?php
			}
			else if ($message == 2)
			{
				$linkBack = "transaksi_kas_umum.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){

	$('#tgl_transaksi').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#GL_TRANS').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#GL_TRANS").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#GL_TRANS').val(ui.item.kode_perk);

					$('#kode_perk').val(ui.item.kode_perk);
					$('#kode_alt').val(ui.item.kode_alt);
					
					$('#nama_perk').val(ui.item.nama_perk);
					$('#kode_induk').val(ui.item.kode_induk);
					
					$('#saldo_awal').val(ui.item.saldo_awal);
					$('#saldo_debet').val(ui.item.saldo_debet);

					$('#saldo_kredit').val(ui.item.saldo_awal);
					$('#saldo_akhir').val(ui.item.saldo_akhir);

					return false;
				},
				focus: function( event, ui ) {

			        $('#GL_TRANS').val(ui.item.kode_perk);
			        
			        return false;
			    },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>