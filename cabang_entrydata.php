<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "cabang_entrydata";

$message = 0;

if (isset($_POST['submit']))
{
	$kode_cab = $_POST['kode_cab'];
	$nama_cab = $_POST['nama_cab'];
	$KODE_CAB_INDUK = $_POST['KODE_CAB_INDUK'];
	$NAMA_CAB_INDUK = $_POST['NAMA_CAB_INDUK'];

	$sql = "
	INSERT INTO kodecabang SET 
	kode_cab = '".$kode_cab."' ,
	nama_cab = '".$nama_cab."' ,
	KODE_CAB_INDUK = '".$KODE_CAB_INDUK."' ,
	NAMA_CAB_INDUK = '".$NAMA_CAB_INDUK."'
	";

	mysql_query($sql);

	$message = 1;		
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Cabang Entry Data</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form form-validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="kode_cab" name="kode_cab">
											<label for="kode_cab">Kode Cabang</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="nama_cab" name="nama_cab">
											<label for="nama_cab">Nama Cabang</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="KODE_CAB_INDUK" name="KODE_CAB_INDUK">
											<label for="KODE_CAB_INDUK">Kode Cabang Induk</label>
										</div>

										<div class="form-group floating-label">
											<input type="text" class="form-control" id="NAMA_CAB_INDUK" name="NAMA_CAB_INDUK">
											<label for="NAMA_CAB_INDUK">Nama Cabang Induk</label>
										</div>

									</div>
								</div>

							</div>

						</div>	

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>	

				</div>
			</section>	

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "cabang_entrydata.php";
				require_once "layouts/message_success.php";
			}
			else if ($message == 2)
			{
				$linkBack = "cabang_entrydata.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>	

<?php require_once "layouts/footer.php"; ?>