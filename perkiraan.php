<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "perkiraan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

$kode_perk = $nama_perk = $type = $kode_induk = $level = $posisi = $saldo_awal = null;

if (isset($_POST['submit']))
{
	$kode_perk_hidden = $_POST['kode_perk_hidden'];

	$kode_perk = $_POST['kode_perk'];
	$nama_perk = $_POST['nama_perk'];
	$type = $_POST['type'];
	$kode_induk = $_POST['kode_induk'];
	$level = $_POST['level'];
	$posisi = $_POST['posisi'];
	$saldo_awal = $_POST['saldo_awal'];

	if (empty($kode_perk_hidden))
	{
		$sql = "
		INSERT INTO perkiraan SET 
		kode_perk = '".$kode_perk."' ,
		nama_perk = '".$nama_perk."' ,
		type = '".$type."' ,
		kode_induk = '".$kode_induk."' ,
		level = '".$level."' ,
		dk = '".$posisi."' ,
		saldo_awal = '".$saldo_awal."'
		";

		mysql_query($sql);
	}
	else
	{
		$sql = "
		UPDATE perkiraan SET 
		kode_perk = '".$kode_perk."' ,
		nama_perk = '".$nama_perk."' ,
		type = '".$type."' ,
		kode_induk = '".$kode_induk."' ,
		level = '".$level."' ,
		dk = '".$posisi."' ,
		saldo_awal = '".$saldo_awal."'
		WHERE 1
		AND kode_perk = '".$kode_perk."' 
		";

		mysql_query($sql);

		header("location:perkiraan.php?update_message=1");
	}

	$message = 1;
}
else if (isset($_GET['mode']) && $_GET['mode'] == "delete")
{
	$kode_perk = $_GET['kode_perk'];

	$sql = "DELETE FROM perkiraan WHERE 1 AND kode_perk = '".$kode_perk."'";

	mysql_query($sql);

	header("location:perkiraan.php?delete_message=1");
}
else if (isset($_GET['mode']) && $_GET['mode'] == "edit")
{
	$kode_perk = $_GET['kode_perk'];

	$sql = "SELECT * FROM perkiraan WHERE 1 AND kode_perk = '".$kode_perk."'";

	$fetch = mysql_fetch_array(mysql_query($sql));	

	$kode_perk = $fetch['kode_perk'];
	$nama_perk = $fetch['nama_perk'];
	$type = $fetch['type'];
	$kode_induk = $fetch['kode_induk'];
	$level = $fetch['level'];
	$posisi = $fetch['dk'];
	$saldo_awal = $fetch['saldo_awal'];
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 1)
			{
			?>

			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully submit.
							</div>
						</div>
					</div>
				</div>
			</section>

			<?php
			}
			else if (isset($_GET['delete_message']) && $_GET['delete_message'] == 1)
			{
			?>

			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully deleted data.
							</div>
						</div>
					</div>
				</div>
			</section>

			<?php
			}
			else if (isset($_GET['update_message']) && $_GET['update_message'] == 1)
			{
			?>

			<section>
				<div class="section-body contain-lg">
					<div class="card">
						<div class="card-body">
							<div class="alert alert-callout alert-success" role="alert">
								<strong>Well done!</strong> You successfully updated data.
							</div>
						</div>
					</div>
				</div>
			</section>

			<?php
			}
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Perkiraan <?php echo ($kode_perk) ? $kode_perk.' '.$nama_perk : ''; ?></li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="kode_perk" name="kode_perk" value="<?php echo $kode_perk; ?>">
												<label for="kode_perk">Kode Perkiraan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nama_perk" name="nama_perk" value="<?php echo $nama_perk; ?>">
												<label for="nama_perk">Nama Perkiraan</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="type" name="type" value="<?php echo $type; ?>">
												<label for="type">Type</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="kode_induk" name="kode_induk" value="<?php echo $kode_induk; ?>">
												<label for="kode_induk">Kode Induk</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="level" name="level" value="<?php echo $level; ?>">
												<label for="level">Level</label>
											</div>

											<div class="form-group floating-label">
												<select id="posisi" name="posisi" class="form-control" required>
													<option value="">&nbsp;</option>
													<?php 
													$posisis = array("D", "K");
													foreach($posisis as $index => $value)
													{
														$selected = ($value == $posisi) ? 'selected' : '';
													?>
													<option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
													<?php
													}
													?>
												</select>
												<label for="level">DK/Posisi</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="saldo_awal" name="saldo_awal" value="<?php echo $saldo_awal; ?>">
												<label for="saldo_awal">Saldo Awal</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<input type="hidden" name="kode_perk_hidden" value="<?php echo $kode_perk; ?>" />

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Save</button>

					</form>

					<hr />

					<section class="style-default-bright">

						<div class="row">
							<div class="col-lg-12">

								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Kode Perkiraan</th>
												<th>Nama Perkiraan</th>
												<th>Kode Induk</th>
												<th>Type</th>
												<th>Level</th>
												<th>DK/Posisi</th>
												<th>Saldo Awal</th>
												<th>Saldo Debet</th>
												<th>Saldo Kredit</th>
												<th>Saldo Akhir</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$sql = "SELECT * FROM perkiraan WHERE 1";
											$query = mysql_query($sql);

											while($rows = mysql_fetch_array($query))
											{
											?>

											<tr>
												<td><?php echo $rows['kode_perk']; ?>
													<div>
													<a href="perkiraan.php?mode=edit&kode_perk=<?php echo $rows['kode_perk']; ?>" class="btn ink-reaction btn-default-bright btn-xs btn-primary" role="button">Edit</a>
													<a href="perkiraan.php?mode=delete&kode_perk=<?php echo $rows['kode_perk']; ?>" class="btn ink-reaction btn-default-bright btn-xs btn-primary" role="button" onclick="return confirm('Yakin mau mendelete item ini ?');">Delete</a>
													</div>
												</td>
												<td><?php echo $rows['nama_perk']; ?></td>
												<td><?php echo $rows['kode_induk']; ?></td>
												<td><?php echo $rows['type']; ?></td>
												<td><?php echo $rows['level']; ?></td>
												<td><?php echo ($rows['dk'] == "K") ? 'Kredit' : 'Debet'; ?></td>
												<td><?php echo number_format($rows['saldo_awal'],2,'.',','); ?></td>
												<td><?php echo number_format($rows['saldo_debet'],2,'.',','); ?></td>
												<td><?php echo number_format($rows['saldo_kredit'],2,'.',','); ?></td>
												<td><?php echo number_format($rows['saldo_akhir'],2,'.',','); ?></td>
											</tr>

											<?php
											}
											?>
										</tbody>
									</table>
								</div>

							</div>
						</div>

					</section>
					
				</div>	
			</section>

		</div>

		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>