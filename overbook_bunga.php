<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "overbook_bunga";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi = date('Y-m-d');

$message = 0;

if (isset($_POST['submit']))
{
	$tgl_transaksi = $_POST['tgl_transaksi'];

	$sqlDeposito = "
	SELECT * FROM deposito 
	WHERE 1 
	AND TGL_REGISTRASI <= '".$tgl_transaksi."'
	AND BUNGA_BLN_INI > 0
	";

	$queryDeposito = mysql_query($sqlDeposito);

	$my_kode_trans = array('250', '450');

	$userId = $user['USERID'];

	while($results = mysql_fetch_array($queryDeposito))
	{
		foreach($my_kode_trans as $kode_trans)
		{
			$sqlCheck = "
			SELECT * FROM deptrans 
			WHERE 1 
			AND NO_REKENING = '".$results['NO_REKENING']."'
			AND MY_KODE_TRANS = '".$kode_trans."'
			";

			$resultCheck = mysql_fetch_array(mysql_query($sqlCheck));

			if (empty($resultCheck['DEPTRANS_ID']))
			{
				$TGL_TRANS = $tgl_transaksi;
				$NO_REKENING = $results['NO_REKENING'];

				if ($kode_trans == "250")
				{
					$SALDO_TRANS = $results['BUNGA_BLN_INI'];
				}
				else if ($kode_trans == "450")
				{
					$SALDO_TRANS = $results['PAJAK_BLN_INI'];
				}

				mysql_query("
				INSERT INTO deptrans SET 
				TGL_TRANS = '".$TGL_TRANS."' ,
				NO_REKENING = '".$NO_REKENING."' ,
				KODE_TRANS = '01' ,
				SALDO_TRANS = '".$SALDO_TRANS."' ,
				MY_KODE_TRANS = '".$kode_trans."' ,
				USERID = '".$userId."'
				");

			}
		}
	}

	$message = 1;
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Overbook Bunga</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php echo $tgl_transaksi; ?>" required>
														<label for="tgl_transaksi">Tanggal Transaksi</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Search</button>

					</form>
					
					<hr />

					<?php 
					if ($message == 1)
					{
						$linkBack = "overbook_bunga.php";
						require_once "layouts/message_success.php";
					}
					?>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tgl_transaksi').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>