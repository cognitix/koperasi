<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "kredit_viewall";
$form = (!empty($_GET['form'])) ? $_GET['form'] : 1;

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$nomor_rekening = (!empty($_GET['nomor_rekening'])) ? $_GET['nomor_rekening'] : "";

if (isset($_POST['simpan']))
{
	$userId = $user['USERID'];

	$JENIS_PINJAMAN = $_POST['JENIS_PINJAMAN'];
	
	$NO_REKENING = $_POST['NO_REKENING'];
	$NASABAH_ID = $_POST['NASABAH_ID'];
	$NASABAH = $_POST['NASABAH'];

	$KODE_GROUP5 = $_POST['KODE_GROUP5'];
	
	$slip_payment = $_POST['slip_payment'];
	$KODE_TYPE_KREDIT = $_POST['KODE_TYPE_KREDIT'];
	$TGL_REALISASI = $_POST['TGL_REALISASI'];

	$agency = $_POST['agency'];
	$nohp_agency = $_POST['nohp_agency'];

	$nama_majikan = $_POST['nama_majikan'];
	$no_telepon = $_POST['no_telepon'];
	$JML_PINJAMAN = $_POST['JML_PINJAMAN'];

	$bunga_pinjaman = $_POST['bunga_pinjaman'];
	$JML_BUNGA_PINJAMAN = $_POST['JML_BUNGA_PINJAMAN'];

	$JML_ANGSURAN = $_POST['JML_ANGSURAN'];
	$PERIODE_ANGSURAN = $_POST['PERIODE_ANGSURAN'];
	$PROVISI = $_POST['PROVISI'];
	$ADM = $_POST['ADM'];
	$GRACE_PERIODE = $_POST['GRACE_PERIODE'];

	$pokok = ($JML_PINJAMAN / $JML_ANGSURAN);
	$bunga = ($JML_BUNGA_PINJAMAN / $JML_ANGSURAN);
	$admin = ($ADM / $JML_ANGSURAN);
	$angsuran = $pokok + $bunga + $admin;

	$saldoNominatif = $JML_PINJAMAN + $bunga + $admin;



	// =======================



	$PENJAMIN = $_POST['PENJAMIN'];
	$PEKERJAAN_PENJAMIN = $_POST['PEKERJAAN_PENJAMIN'];
	$ALAMAT_PENJAMIN = $_POST['ALAMAT_PENJAMIN'];
	$ID_PENJAMIN = $_POST['ID_PENJAMIN'];
	$TGL_ANALISA = $_POST['TGL_ANALISA'];
	$NAMA_PASANGAN = $_POST['NAMA_PASANGAN'];
	$ALAMAT_PASANGAN = $_POST['ALAMAT_PASANGAN'];
	$PEKERJAAN_PASANGAN = $_POST['PEKERJAAN_PASANGAN'];
	$STATUS_PASANGAN = $_POST['STATUS_PASANGAN'];
	$TUJUAN_PENGGUNAAN = $_POST['TUJUAN_PENGGUNAAN'];



	// =======================



	$AGUNAN_JENIS = $_POST['AGUNAN_JENIS'];
	$AGUNAN_IKATAN_HUKUM = $_POST['AGUNAN_IKATAN_HUKUM'];
	$AGUNAN_NILAI = $_POST['AGUNAN_NILAI'];
	$bunga_likuidasi = $_POST['bunga_likuidasi'];
	$NILAI_LIKUIDASI = $_POST['NILAI_LIKUIDASI'];
	$PEMILIK_AGUNAN = $_POST['PEMILIK_AGUNAN'];
	$alamat_agunan = $_POST['alamat_agunan'];
	$TGL_JT_AGUNAN = $_POST['TGL_JT_AGUNAN'];
	$NO_AGUNAN = $_POST['NO_AGUNAN'];
	$AGUNAN = $_POST['AGUNAN'];



	// =======================

	$query = "UPDATE kredit SET
							JENIS_PINJAMAN = '".$JENIS_PINJAMAN."' ,
							
							NO_REKENING = '".$NO_REKENING."' ,
							NASABAH_ID = '".$NASABAH_ID."' ,
							NASABAH = '".$NASABAH."' ,

							TYPE_PINJAMAN = '".$KODE_TYPE_KREDIT."' ,
							KODE_GROUP5 = '".$KODE_GROUP5."' ,
							
							slip_payment = '".$slip_payment."' ,
							TGL_REALISASI = '".$TGL_REALISASI."' ,

							nama_majikan = '".$nama_majikan."' ,
							no_telepon = '".$no_telepon."' ,
							JML_PINJAMAN = '".$JML_PINJAMAN."' ,
							
							JML_ANGSURAN = '".$JML_ANGSURAN."' ,
							PERIODE_ANGSURAN = '".$PERIODE_ANGSURAN."' ,
							PROVISI = '".$PROVISI."' ,
							ADM = '".$ADM."' ,
							GRACE_PERIODE = '".$GRACE_PERIODE."' ,

							JML_BUNGA_PINJAMAN = '".$JML_BUNGA_PINJAMAN ."' ,

							agency = '".$agency."' ,
							nohp_agency = '".$nohp_agency."' ,

							USERID = '".$userId."' ,
							CAB = '".$kodecabang['kode_cab']."' ,
							OUTLET = '".$kodecabang['kode_cab']."' ,

							angsuran_pokok = '".$pokok."' ,
							angsuran_bunga = '".$bunga."' ,
							angsuran_total = '".$angsuran."' ,
							ANGSURAN_ADMIN = '".$admin."' ,

							SUKU_BUNGA_PER_TAHUN = '".$bunga_pinjaman."' ,
							SALDO_NOMINATIF = '".$saldoNominatif."' ,
							SUKU_BUNGA_PER_ANGSURAN = '".($bunga_pinjaman / 12)."' ,

							PENJAMIN = '".$PENJAMIN."' ,
							PEKERJAAN_PENJAMIN = '".$PEKERJAAN_PENJAMIN."' ,
							ALAMAT_PENJAMIN = '".$ALAMAT_PENJAMIN."' ,
							ID_PENJAMIN = '".$ID_PENJAMIN."' ,
							TGL_ANALISA = '".$TGL_ANALISA."' ,
							NAMA_PASANGAN = '".$NAMA_PASANGAN."' ,
							ALAMAT_PASANGAN = '".$ALAMAT_PASANGAN."' ,
							PEKERJAAN_PASANGAN = '".$PEKERJAAN_PASANGAN."' ,
							STATUS_PASANGAN = '".$STATUS_PASANGAN."' ,
							TUJUAN_PENGGUNAAN = '".$TUJUAN_PENGGUNAAN."' ,

							AGUNAN_JENIS = '".$AGUNAN_JENIS."' ,
							AGUNAN_IKATAN_HUKUM = '".$AGUNAN_IKATAN_HUKUM."' ,
							AGUNAN_NILAI = '".$AGUNAN_NILAI."' ,
							NILAI_LIKUIDASI = '".$NILAI_LIKUIDASI."' ,
							PEMILIK_AGUNAN = '".$PEMILIK_AGUNAN."' ,
							alamat_agunan = '".$alamat_agunan."' ,
							TGL_JT_AGUNAN = '".$TGL_JT_AGUNAN."' ,
							NO_AGUNAN = '".$NO_AGUNAN."' ,
							AGUNAN = '".$AGUNAN."'

							WHERE 1
							AND NO_REKENING = '".$nomor_rekening."'
						";

	$result = mysql_query($query);
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/wizard/wizard.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/jquery.dataTables.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css" />

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section class="">
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Kredit View Detail Data - <?php echo $nomor_rekening; ?></li>
					</ol>
				</div>
				<div class="section-body">

					
					
				</div>	

				<?php 
				if (!empty($_GET['nomor_rekening']))
				{
					$sqlKredit = "
					SELECT
					kredit.* ,
					nasabah.NO_PASSPORT ,
					nasabah.alamat
					FROM kredit
					JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id 
					WHERE 1 
					AND NO_REKENING = '".$nomor_rekening."'";
					$resultKredit = mysql_query($sqlKredit);
					$fetchKredit = mysql_fetch_array($resultKredit);

					//echo '<pre>';print_r($fetchKredit);echo '</pre>';
				?>

				<div class="section-body contain-lg" style="margin-top:20px;">

					<form class="form" method="POST" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-12">
								<div class="card">
									<div class="card-body ">

										<h2 class="text-primary">Form 1</h2>

										<?php include_once('layouts/kredit/edit/form1.php'); ?>

									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="card">
									<div class="card-body ">

										<h2 class="text-primary">Form 2</h2>

										<?php include_once('layouts/kredit/edit/form2.php'); ?>

									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="card">
									<div class="card-body ">

										<h2 class="text-primary">Form 3</h2>

										<?php include_once('layouts/kredit/edit/form3.php'); ?>

									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="card">
									<div class="card-body ">

										<h2 class="text-primary">Form 4</h2>

										<?php include_once('layouts/kredit/edit/form4.php'); ?>

									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="simpan">Simpan</button>
							</div>

					</form>

				</div>	
				<?php 
				}
				?>

			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			      },
			});
		}
	});

	$('#TGL_REALISASI_container, #TGL_ANALISA_container, #TGL_JT_AGUNAN_container').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	var noindex = <?php echo $form; ?>;

	var handleTabShow = function(tab, navigation, index, wizard){
		var total = navigation.find('li').length;
		var current = index - 1;
		var percent = (current / (total - 1)) * 100;
		var percentWidth = 100 - (100 / total) + '%';
		
		navigation.find('li').addClass('done');//.removeClass('done');
		//navigation.find('li.active').prevAll().addClass('done');
		
		wizard.find('.progress-bar').css({width: percent + '%'});
		$('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
	};

	$('#rootwizard1').bootstrapWizard({
		onTabShow: function(tab, navigation, index) {
			handleTabShow(tab, navigation, noindex, $('#rootwizard1'));
		}
	});

	$('#bunga_pinjaman').bind('keyup', function(){
		var el = $(this);
		var bunga_pinjaman = parseFloat(el.val());
		var JML_PINJAMAN = parseFloat($('#JML_PINJAMAN').val());
		var JML_BUNGA_PINJAMAN = parseFloat(bunga_pinjaman / 100 * JML_PINJAMAN);
		$('#JML_BUNGA_PINJAMAN').val(JML_BUNGA_PINJAMAN);
	});

	$('#JML_BUNGA_PINJAMAN').bind('keyup', function(){
		var el = $(this);
		var JML_BUNGA_PINJAMAN = parseFloat(el.val());
		var JML_PINJAMAN = parseFloat($('#JML_PINJAMAN').val());
		var bunga_pinjaman = parseFloat( (JML_BUNGA_PINJAMAN / JML_PINJAMAN) * 100);
		$('#bunga_pinjaman').val(bunga_pinjaman);
	});

	$('#bunga_likuidasi').bind('keyup', function(){
		var el = $(this);
		var bunga_likuidasi = parseFloat(el.val());
		var AGUNAN_NILAI = parseFloat($('#AGUNAN_NILAI').val());
		var NILAI_LIKUIDASI = parseFloat(bunga_likuidasi / 100 * AGUNAN_NILAI);
		$('#NILAI_LIKUIDASI').val(NILAI_LIKUIDASI);
	});

	/*
	$('#JENIS_PINJAMAN').bind('change', function(){
		$.ajax({
			type: 'GET',
			url: 'ajax_nomor_rekening.php',
			data : {
				JENIS_PINJAMAN : $(this).val()
			},
			dataType: "json",
			success: function (datas) {
				$('#NO_REKENING').val(datas.NO_REKENING);
			}
		});
	});
	*/
});
</script>


<script src="assets/js/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
<script src="assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script>
$(function(){

	$('#datatable1').DataTable({
		"dom": 'lCfrtip',
		"order": [],
		"colVis": {
			"buttonText": "Columns",
			"overlayFade": 0,
			"align": "right"
		},
		"language": {
			"lengthMenu": '_MENU_ entries per page',
			"search": '<i class="fa fa-search"></i>',
			"paginate": {
				"previous": '<i class="fa fa-angle-left"></i>',
				"next": '<i class="fa fa-angle-right"></i>'
			}
		}
	});

	$('#datatable1 tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>