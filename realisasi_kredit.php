<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "realisasi_kredit";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

$kretransId = 0;

if (isset($_POST['submit']))
{
	$NO_REKENING = $_POST['NO_REKENING'];
	$sqlCheck = "SELECT * FROM kredit WHERE 1 AND NO_REKENING = '".$NO_REKENING."'";
	$fetchCheck = mysql_fetch_array(mysql_query($sqlCheck));
	if (!empty($fetchCheck['STATUS_AKTIF']) && $fetchCheck['STATUS_AKTIF'] == 2) 
	{
		$message = 2;
	}
	else
	{

		// insert kretrans
		$NASABAH = $_POST['NASABAH'];
		$TGL_TRANS = $_POST['tgl_transaksi'];
		$MY_KODE_TRANS = 100;
		$KUITANSI = $_POST['no_bukti'];
		$ANGSURAN_KE = 0;
		$POKOK_TRANS = $_POST['angsuran_pokok'];
		$BUNGA_TRANS = $_POST['angsuran_bunga'];
		$ADMIN_TRANS = $_POST['angsuran_admin'];
		$KODE_TRANS = $_POST['kodetranskredit'];
		$userId = $user['USERID'];
		$NO_TELLER = $userId;
		$TOB = 'T';
		$JML_ANGSURAN = $_POST['JML_PINJAMAN'];
		$TGL_INPUT = date('Y-m-d');
		$KODE_PERK_GL = "10102";
		$FLAG_CETAK = 'N';
		$PROVISI_TRANS = $_POST['total_biaya_provisi'];
		$ADM_TRANS = $_POST['total_biaya_adm'];
		$CAB = $kodecabang['kode_cab'];

		$sql = "
		INSERT INTO kretrans SET 
		TGL_TRANS = '".$TGL_TRANS."' ,
		NO_REKENING = '".$NO_REKENING."' ,
		MY_KODE_TRANS = '".$MY_KODE_TRANS."' ,
		KUITANSI = '".$KUITANSI."' ,
		ANGSURAN_KE = '".$ANGSURAN_KE."' ,
		POKOK_TRANS = '".$POKOK_TRANS."' ,
		BUNGA_TRANS = '".$BUNGA_TRANS."' ,
		ADMIN_TRANS = '".$ADMIN_TRANS."' ,
		NO_TELLER = '".$NO_TELLER."' ,
		USERID = '".$userId."' ,
		FLAG_CETAK = '".$FLAG_CETAK."' ,
		TOB = '".$TOB."' ,
		JML_ANGSURAN = '".$JML_ANGSURAN."' ,
		TGL_INPUT = '".$TGL_INPUT."' ,
		TGL_TRANSAKSI = '".$TGL_TRANS."' ,
		KODE_PERK_GL = '".$KODE_PERK_GL."' ,
		PROVISI_TRANS = '".$PROVISI_TRANS."' ,
		ADM_TRANS = '".$ADM_TRANS."' ,
		CAB = '".$CAB."' ,
		KODE_TRANS = '".$KODE_TRANS."'
		";

		$result = mysql_query($sql);

		$modul_trans_id = $kretransId = mysql_insert_id();

		// insert teller trans
		$sql = "
		INSERT INTO tellertrans SET 
		modul = 'KRE' ,
		tgl_trans = '".$TGL_TRANS."' ,
		kode_jurnal = '' ,
		NO_BUKTI = '".$KUITANSI."' ,
		uraian = 'Realisasi Kredit Tunai : #".trim($NO_REKENING)." - ".trim($NASABAH)."' ,
		my_kode_trans = '300' ,
		saldo_trans = '".$JML_ANGSURAN."' ,
		tob = 'T' ,
		modul_trans_id = '".$modul_trans_id."' ,
		userid = '".$userId."' ,
		cab = '".$CAB."'
		";

		$result = mysql_query($sql);

		// update kredit
		$sql = "
		UPDATE kredit SET 
		STATUS_AKTIF = 2
		WHERE 1 
		AND NO_REKENING = '".$NO_REKENING."'
		";

		$result = mysql_query($sql);

		$message = 1;

	}
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Realisasi Kredit</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_REKENING" name="NO_REKENING" data-source="autosuggest_nomorrekening_kredit.php">
												<label for="NO_REKENING">Nomor Rekening Kredit</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="NASABAH_ID" name="NASABAH_ID" readonly>
												<label for="NASABAH_ID">Nasabah ID</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="NASABAH" name="NASABAH" readonly>
												<label for="NASABAH">Nasabah</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="JML_PINJAMAN" name="JML_PINJAMAN" value="0" readonly />
												<label for="JML_PINJAMAN">Jumlah Pinjaman</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="JML_BUNGA_PINJAMAN" name="JML_BUNGA_PINJAMAN" required readonly>
												<label for="JML_BUNGA_PINJAMAN">Jumlah Bunga Pinjaman</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="TGL_REALISASI" name="TGL_REALISASI" value="<?php echo $tglsystem; ?>"  
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?> 
												/>
												<label for="TGL_REALISASI">Tanggal Realisasi</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="JML_ANGSURAN" name="JML_ANGSURAN" required readonly>
												<label for="JML_ANGSURAN">Jumlah Angsuran</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="bunga_pinjaman" name="bunga_pinjaman" required readonly>
												<label for="bunga_pinjaman">Bunga Pinjaman (%)</label>
											</div>

											<div class="form-group">
												<select id="JENIS_PINJAMAN" name="JENIS_PINJAMAN" class="form-control" required readonly>
													<option value="">&nbsp;</option>

													<?php 
													$sqlJenisPinjaman = mysql_query("SELECT * FROM kodejeniskredit WHERE 1 ORDER BY KODE_JENIS_KREDIT ASC");
													while($rowJenisPinjaman = mysql_fetch_array($sqlJenisPinjaman))
													{
													?>
													<option value="<?php echo $rowJenisPinjaman['KODE_JENIS_KREDIT']; ?>"><?php echo $rowJenisPinjaman['KODE_JENIS_KREDIT'] .' - '. $rowJenisPinjaman['DESKRIPSI_JENIS_KREDIT']; ?></option>
													<?php
													}
													?>

												</select>
												<label for="JENIS_PINJAMAN">Jenis Pinjaman</label>
											</div>

											<div class="form-group">
												<select id="KODE_TYPE_KREDIT" name="KODE_TYPE_KREDIT" class="form-control" required readonly>
													<option value="">&nbsp;</option>

													<?php 
													$sqlkodetypekredit = mysql_query("SELECT * FROM kodetypekredit WHERE 1");
													while($rowkodetypekredit = mysql_fetch_array($sqlkodetypekredit))
													{
													?>
													<option value="<?php echo $rowkodetypekredit['KODE_TYPE_KREDIT']; ?>"><?php echo $rowkodetypekredit['KODE_TYPE_KREDIT'] .' - '. $rowkodetypekredit['DESKRIPSI_TYPE_KREDIT']; ?></option>
													<?php
													}
													?>

												</select>
												<label for="KODE_TYPE_KREDIT">Kode Type Kredit</label>
											</div>

									</div>
								</div>	

							</div>
							
							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">


											<div class="form-group">
												<select id="kodetranskredit" name="kodetranskredit" class="form-control" required>
													<?php 
													$sqlkodetranskredit = mysql_query("SELECT * FROM kodetranskredit WHERE 1");
													while($rowkodetranskredit = mysql_fetch_array($sqlkodetranskredit))
													{
													?>
													<option value="<?php echo $rowkodetranskredit['KODE_TRANS']; ?>"><?php echo $rowkodetranskredit['KODE_TRANS'] .' - '. $rowkodetranskredit['DESKRIPSI_TRANS']; ?></option>
													<?php
													}
													?>

												</select>
												<label for="kodetranskredit">Kode Trans Kredit</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="angsuran_pokok" name="angsuran_pokok" value="" readonly />
												<label for="angsuran_pokok">Angsuran Pokok</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="angsuran_bunga" name="angsuran_bunga" value="" readonly />
												<label for="angsuran_bunga">Angsuran Bunga</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="angsuran_admin" name="angsuran_admin" value="" readonly />
												<label for="angsuran_admin">Angsuran ADM</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="total_biaya_adm" name="total_biaya_adm" value="" readonly />
												<label for="total_biaya_adm">Total Biaya ADM</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="total_biaya_provisi" name="total_biaya_provisi" value="" readonly />
												<label for="total_biaya_provisi">Total Biaya Provisi</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="angsuran_total" name="angsuran_total" value="" readonly />
												<label for="angsuran_total">Total Angsuran</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php echo $tglsystem; ?>" 
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?> 
												/>
												<label for="tgl_transaksi">Tanggal Transaksi</label>
											</div>

											<div class="form-group">
												<?php 
												$sqlNoBukti = "SELECT NO_BUKTI FROM tellertrans WHERE 1 and modul='KRE' ORDER by trans_id DESC LIMIT 1";
												$fetchNoBukti = mysql_fetch_array(mysql_query($sqlNoBukti));
												$idMax = (int) str_replace("K-", "", $fetchNoBukti['NO_BUKTI']);
												$idMax++;
												$nobukti = "K-" . sprintf("%05s", $idMax);
												?>
												<input type="text" class="form-control" id="no_bukti" name="no_bukti" value="<?php echo $nobukti; ?>" readonly />
												<label for="no_bukti">No Bukti</label>
											</div>

									</div>
								</div>

							</div>

						</div>		

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "realisasi_kredit.php";
				require_once "layouts/message_success.php";
			?>

			<section>
				<div class="section-body contain-lg">
					<a href="print_realisasi_kredit.php?kretransId=<?php echo $kretransId; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print Realisasi Kredit</a>
				</div>
			</section>

			<?php
			}
			else if ($message == 2)
			{
				$linkBack = "realisasi_kredit.php";
				require_once "layouts/message_error.php";
			?>

				<section>
					<div class="section-body contain-lg">
						<div class="card">
							<div class="card-body">
								<div class="alert alert-callout alert-danger" role="alert">
									<strong>Oh snap!</strong> Realisasi tidak bisa lagi di simpan atau norekening ini sudah di realisasi.</a>
								</div>
							</div>
						</div>
					</div>
				</section>

			<?php
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){

	$('#tgl_transaksi, #TGL_REALISASI').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#NO_REKENING').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#NO_REKENING").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#NO_REKENING').val(ui.item.NO_REKENING);
					$('#NASABAH_ID').val(ui.item.NASABAH_ID);
					$('#NASABAH').val(ui.item.NASABAH);
					$('#JML_PINJAMAN').val(ui.item.JML_PINJAMAN);
					$('#JML_BUNGA_PINJAMAN').val(ui.item.JML_BUNGA_PINJAMAN);
					$('#TGL_REALISASI').val(ui.item.TGL_REALISASI);
					$('#JML_ANGSURAN').val(ui.item.JML_ANGSURAN);
					$('#bunga_pinjaman').val(ui.item.SUKU_BUNGA_PER_TAHUN);
					$('#JENIS_PINJAMAN option[value="'+ui.item.JENIS_PINJAMAN+'"]').prop("selected",true);
					$('#KODE_TYPE_KREDIT option[value="'+ui.item.TYPE_PINJAMAN+'"]').prop("selected",true);

					$('#angsuran_pokok').val(ui.item.angsuran_pokok);
					$('#angsuran_bunga').val(ui.item.angsuran_bunga);
					$('#angsuran_admin').val(ui.item.ANGSURAN_ADMIN);
					$('#angsuran_total').val(ui.item.angsuran_total);

					$('#total_biaya_adm').val(ui.item.ADM);
					$('#total_biaya_provisi').val(ui.item.PROVISI);

					return false;
				},
				focus: function( event, ui ) {

			        $( "#NO_REKENING" ).val( ui.item.NO_REKENING );
			        
			        return false;
			      },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>