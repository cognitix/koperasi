<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "angsuran_kredit";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

$kretransId = 0;

if (isset($_POST['submit']))
{
	// insert kretrans
	$NO_REKENING = $_POST['NO_REKENING'];
	$NASABAH = $_POST['NASABAH'];
	$TGL_TRANS = $_POST['tgl_transaksi'];
	$MY_KODE_TRANS = 300;
	$KUITANSI = $_POST['no_bukti'];
	$ANGSURAN_KE = 0;
	$POKOK_TRANS = $_POST['pokok_trans'];
	$BUNGA_TRANS = $_POST['bunga_trans'];
	$ADMIN_TRANS = $_POST['admin_trans'];
	$userId = $user['USERID'];
	$NO_TELLER = $userId;
	$TOB = 'T';
	$JML_ANGSURAN = $_POST['JML_PINJAMAN'];
	$TGL_INPUT = date('Y-m-d');
	$KODE_PERK_GL = "10102";
	$FLAG_CETAK = 'N';
	$CAB = $kodecabang['kode_cab'];
	$PELUNASAN = $_POST['cicilan_ke'];
	$keterangan = $_POST['keterangan'];
	$jumlah = $_POST['jumlah'];
	$kodetrans_kredit = $_POST['kodetrans_kredit'];
	$kodetrans_tabungan = $_POST['kodetrans_tabungan'];

	$NASABAH_ID_2 = $_POST['NASABAH_ID_2'];
	$nama_nasabah = $_POST['nama_nasabah'];
	$NO_REK_TABUNGAN = $_POST['NO_REK_TABUNGAN'];

	$sql = "
	INSERT INTO kretrans SET 
	TGL_TRANS = '".$TGL_TRANS."' ,
	NO_REKENING = '".$NO_REKENING."' ,
	MY_KODE_TRANS = '".$MY_KODE_TRANS."' ,
	KUITANSI = '".$KUITANSI."' ,
	ANGSURAN_KE = '".$ANGSURAN_KE."' ,
	POKOK_TRANS = '".$POKOK_TRANS."' ,
	BUNGA_TRANS = '".$BUNGA_TRANS."' ,
	ADMIN_TRANS = '".$ADMIN_TRANS."' ,
	NO_TELLER = '".$NO_TELLER."' ,
	USERID = '".$userId."' ,
	FLAG_CETAK = '".$FLAG_CETAK."' ,
	TOB = '".$TOB."' ,
	JML_ANGSURAN = '".$JML_ANGSURAN."' ,
	TGL_INPUT = '".$TGL_INPUT."' ,
	TGL_TRANSAKSI = '".$TGL_TRANS."' ,
	KODE_PERK_GL = '".$KODE_PERK_GL."' ,
	CAB = '".$CAB."' ,
	KODE_TRANS = '".$kodetrans_kredit."' ,
	PELUNASAN = '".$PELUNASAN."' ,
	keterangan = '".$keterangan."'
	";

	$result = mysql_query($sql);

	$modul_trans_id = $kretransId = mysql_insert_id();

	// insert teller trans
	$sql = "
	INSERT INTO tellertrans SET 
	modul = 'KRE' ,
	tgl_trans = '".$TGL_TRANS."' ,
	kode_jurnal = '' ,
	NO_BUKTI = '".$KUITANSI."' ,
	uraian = 'Angsuran Kredit Tunai : #".trim($NO_REKENING)." - ".trim($NASABAH)."' ,
	my_kode_trans = '200' ,
	saldo_trans = '".$jumlah."' ,
	tob = 'T' ,
	modul_trans_id = '".$modul_trans_id."' ,
	userid = '".$userId."' ,
	cab = '".$CAB."'
	";

	$result = mysql_query($sql);

	// update kredit
	$outstanding_baki_debet = $_POST['SALDO_NOMINATIF'];
	$SALDO_NOMINATIF = $outstanding_baki_debet - $jumlah;

	if ($SALDO_NOMINATIF <= 0) $SALDO_NOMINATIF = 0;

	$sql = "
	UPDATE kredit SET 
	POKOK_SALDO_AWAL = '".$outstanding_baki_debet."' ,
	SALDO_NOMINATIF = '".$SALDO_NOMINATIF."'
	WHERE 1 
	AND NO_REKENING = '".$NO_REKENING."'
	";

	$result = mysql_query($sql);

	if ($kodetrans_kredit == "05" && $kodetrans_tabungan == "13")
	{
		// insert teller trans
		$sql = "
		INSERT INTO tellertrans SET 
		modul = 'KRE' ,
		tgl_trans = '".$TGL_TRANS."' ,
		kode_jurnal = '' ,
		NO_BUKTI = '".$KUITANSI."' ,
		uraian = 'OB/PM Angsuran Kredit: #".trim($NO_REKENING)." - ".trim($NASABAH)." dari tabungan, no rekening: #".trim($NO_REK_TABUNGAN)." - ".trim($nama_nasabah)."' ,
		my_kode_trans = '200' ,
		saldo_trans = '".$jumlah."' ,
		tob = 'T' ,
		modul_trans_id = '".$modul_trans_id."' ,
		userid = '".$userId."' ,
		cab = '".$CAB."'
		";

		$result = mysql_query($sql);
	}

	$message = 1;
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Angsuran Kredit</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<div class="checkbox checkbox-styled">
													<label>
														<input type="checkbox" value="1" name="pelunasan_semua" id="pelunasan_semua">
														<span>Penulasan Menyeluruh</span>
													</label>
												</div>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NO_REKENING" name="NO_REKENING" data-source="autosuggest_nomorrekening_kredit.php?status_aktif=2">
												<label for="NO_REKENING">Nomor Rekening Kredit</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="NASABAH_ID" name="NASABAH_ID" readonly>
												<label for="NASABAH_ID">Nasabah ID</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="NASABAH" name="NASABAH" readonly>
												<label for="NASABAH">Nasabah</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="TGL_REALISASI" name="TGL_REALISASI" value="<?php echo $tglsystem; ?>" 
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?> 
												/>
												<label for="TGL_REALISASI">Tanggal Realisasi</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="JML_PINJAMAN" name="JML_PINJAMAN" value="0" readonly />
												<label for="JML_PINJAMAN">Jumlah Pinjaman</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="SALDO_NOMINATIF" name="SALDO_NOMINATIF" value="0" readonly />
												<label for="SALDO_NOMINATIF">Outstanding Baki Kredit</label>
											</div>

											<div class="form-group">
												<select id="kodetrans_kredit" name="kodetrans_kredit" class="form-control" required>
													<option value="">&nbsp;</option>

													<?php 
													$KODE_TRANS = "08";
													$sqlKodeTrans = mysql_query("SELECT * FROM kodetranskredit WHERE 1 ORDER BY KODE_TRANS ASC");
													while($rowKodeTrans = mysql_fetch_array($sqlKodeTrans))
													{
														$selected = ($rowKodeTrans['KODE_TRANS'] == $KODE_TRANS) ? 'selected' : '';
													?>
													<option value="<?php echo $rowKodeTrans['KODE_TRANS']; ?>" <?php echo $selected; ?>><?php echo $rowKodeTrans['KODE_TRANS'] .' - '. $rowKodeTrans['DESKRIPSI_TRANS']; ?></option>
													<?php
													}
													?>

												</select>
												<label for="JENIS_PINJAMAN">Kode Trans Kredit</label>
											</div>

											<div class="form-group">
												<select id="kodetrans_tabungan" name="kodetrans_tabungan" class="form-control" required>
													<option value="">&nbsp;</option>

													<?php 
													$KODE_TRANS = "13";
													$sqlKodeTrans = mysql_query("SELECT * FROM kodetranstabungan WHERE 1 ORDER BY KODE_TRANS ASC");
													while($rowKodeTrans = mysql_fetch_array($sqlKodeTrans))
													{
														$selected = ($rowKodeTrans['KODE_TRANS'] == $KODE_TRANS) ? 'selected' : '';
													?>
													<option value="<?php echo $rowKodeTrans['KODE_TRANS']; ?>" <?php echo $selected; ?>><?php echo $rowKodeTrans['KODE_TRANS'] .' - '. $rowKodeTrans['DESKRIPSI_TRANS']; ?></option>
													<?php
													}
													?>

												</select>
												<label for="kodetrans_tabungan">Kode Trans Tabungan</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php echo $tglsystem; ?>"  
												<?php if ($user['USERGROUP'] != "ADMIN"){ ?>
												readonly 
												<?php } ?> 
												/>
												<label for="tgl_transaksi">Tanggal Transaksi</label>
											</div>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="NASABAH_ID_2" name="NASABAH_ID_2" data-source="autosuggest_nomorrekening_all.php">
												<label for="NASABAH_ID_2">Anggota ID</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly>
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="NO_REK_TABUNGAN" name="NO_REK_TABUNGAN" readonly>
												<label for="NO_REK_TABUNGAN">Nomor Rekening Tabungan</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="simpanan_sukarela" name="simpanan_sukarela" readonly>
												<label for="simpanan_sukarela">Simpanan Sukarela</label>
											</div>

									</div>
								</div>	

							</div>
							
							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<input type="text" class="form-control" id="cicilan_ke" name="cicilan_ke" value="0" />
												<label for="cicilan_ke">Cicilan Ke</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="tgl_tagihan" name="tgl_tagihan" value="" readonly />
												<label for="tgl_tagihan">Tanggal Tagihan</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="pokok_trans" name="pokok_trans" value="0"  />
												<label for="pokok_trans">Pokok Trans</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="bunga_trans" name="bunga_trans" value="0"  />
												<label for="bunga_trans">Bunga Trans</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="admin_trans" name="admin_trans" value="0" />
												<label for="admin_trans">Admin Trans</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="jumlah" name="jumlah" value="0" />
												<label for="jumlah">Jumlah</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="keterangan" name="keterangan" value="0" />
												<label for="keterangan">Keterangan</label>
											</div>

											<div class="form-group">
												<?php 
												$sqlNoBukti = "SELECT NO_BUKTI FROM tellertrans WHERE 1 and modul='KRE' ORDER by trans_id DESC LIMIT 1";
												$fetchNoBukti = mysql_fetch_array(mysql_query($sqlNoBukti));
												$idMax = (int) str_replace("K-", "", $fetchNoBukti['NO_BUKTI']);
												$idMax++;
												$nobukti = "K-" . sprintf("%05s", $idMax);
												?>
												<input type="text" class="form-control" id="no_bukti" name="no_bukti" value="<?php echo $nobukti; ?>" readonly />
												<label for="no_bukti">No Bukti</label>
											</div>

									</div>
								</div>

							</div>

						</div>		

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "realisasi_kredit.php";
				require_once "layouts/message_success.php";
			?>

			<section>
				<div class="section-body contain-lg">
					<a href="print_angsuran_kredit.php?kretransId=<?php echo $kretransId; ?>" class="btn ink-reaction btn-raised btn-primary" name="submit" target="_blank">Print Angsuran Kredit</a>
				</div>
			</section>

			<?php
			}
			else if ($message == 2)
			{
				$linkBack = "realisasi_kredit.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	/*$('#pelunasan_semua').bind('click', function(){
		var el = $(this);
		if (el.is(':checked'))
		{
			$('#bunga_trans').removeAttr('readonly');
		}
		else
		{
			$('#bunga_trans').attr('readonly', 'true');
		}
	});*/

	$('#tgl_transaksi, #TGL_REALISASI').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#NASABAH_ID_2').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#NASABAH_ID_2").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#NASABAH_ID_2').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);
					$('#NO_REK_TABUNGAN').val(ui.item.NO_REKENING);
					$('#simpanan_sukarela').val(ui.item.simpanan_sukarela);

					return false;
				},
				focus: function( event, ui ) {

			        $( "#NASABAH_ID_2" ).val( ui.item.nasabah_id );
			        
			        return false;
			   	},
			});
		}
	});

	$.ajax({
		url: $('#NO_REKENING').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#NO_REKENING").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#NASABAH_ID').val(ui.item.NASABAH_ID);
					$('#NASABAH').val(ui.item.NASABAH);

					$('#NO_REKENING').val(ui.item.NO_REKENING);
					$('#TGL_REALISASI').val(ui.item.TGL_REALISASI);
					$('#JML_PINJAMAN').val(ui.item.JML_PINJAMAN);
					$('#SALDO_NOMINATIF').val(ui.item.SALDO_NOMINATIF);

					var pelunasan_semua = $('#pelunasan_semua:checked').val();
					var pelunasan = (typeof(pelunasan_semua) == "undefined") ? 0 : 1;
					//alert(pelunasan);

					//if (pelunasan)
					//{
					//	$('#bunga_trans').removeAttr('readonly');
					//}
					//else
					//{
					//	$('#bunga_trans').attr('readonly', 'true');
					//}

					$.ajax({
						url : 'ajax_kretrans.php' ,
						data : {
							NO_REKENING : ui.item.NO_REKENING ,
							pelunasan_semua : pelunasan,
						},
						dataType : 'json' ,
						success : function(data){
							$('#cicilan_ke').val(data.pelunasan);
							$('#tgl_tagihan').val(data.tgl_tagihan);
							$('#pokok_trans').val(data.pokok_trans);
							$('#bunga_trans').val(data.bunga_trans);
							$('#admin_trans').val(data.admin_trans);
							$('#jumlah').val(data.jumlah);
						}
					})


					return false;
				},
				focus: function( event, ui ) {

			        $( "#NO_REKENING" ).val( ui.item.NO_REKENING );
			        
			        return false;
			      },
			});
		}
	});


	$('#bunga_trans').keyup(function(){
		var el = $(this);

		var bunga_trans = parseFloat(el.val());
		var pokok_trans = parseFloat($('#pokok_trans').val());
		var admin_trans = parseFloat($('#admin_trans').val());

		var jumlah = pokok_trans + bunga_trans + admin_trans;

		$('#jumlah').val(jumlah);
	});


	$('#pokok_trans').keyup(function(){
		var el = $(this);

		var pokok_trans = parseFloat(el.val());
		var bunga_trans = parseFloat($('#bunga_trans').val());
		var admin_trans = parseFloat($('#admin_trans').val());

		var jumlah = pokok_trans + bunga_trans + admin_trans;

		$('#jumlah').val(jumlah);
	});


	$('#admin_trans').keyup(function(){
		var el = $(this);

		var admin_trans = parseFloat(el.val());
		var bunga_trans = parseFloat($('#bunga_trans').val());
		var pokok_trans = parseFloat($('#pokok_trans').val());

		var jumlah = pokok_trans + bunga_trans + admin_trans;

		$('#jumlah').val(jumlah);
	});


	$('#cicilan_ke').keyup(function(){
		var el = $(this);

		var NO_REKENING = $( "#NO_REKENING" ).val();
		var angsuran_ke = el.val();

		$.ajax({
			url : 'ajax_kretrans_angsuran_ke.php' ,
			data : {
				NO_REKENING : NO_REKENING,
				angsuran_ke : angsuran_ke,
			},
			dataType : 'json' ,
			success : function(data){
				$('#cicilan_ke').val(data.pelunasan);
				$('#tgl_tagihan').val(data.tgl_tagihan);
				$('#pokok_trans').val(data.pokok_trans);
				$('#bunga_trans').val(data.bunga_trans);
				$('#admin_trans').val(data.admin_trans);
				$('#jumlah').val(data.jumlah);
			}
		});

		return false;
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>