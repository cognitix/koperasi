<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "ambil_bunga_deposito";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi = date('Y-m-d');

$message = 0;

if (isset($_POST['submit']))
{
	$nomor_rekening = $_POST['nomor_rekening'];
	$jml_deposito = $_POST['jml_deposito'];
	$bunga_hari_ini = $_POST['bunga_hari_ini'];
	$pajak_hari_ini = $_POST['pajak_hari_ini'];
	$no_bukti = $_POST['no_bukti'];

	$tgl_trans = date('Y-m-d');

	$userId = $user['USERID'];
	$cab = $kodecabang['kode_cab'];

	$sqlDeposito = "SELECT 
	deposito.*,
	nasabah.nama_nasabah
	FROM deposito 
	JOIN nasabah ON deposito.NASABAH_ID = nasabah.nasabah_id
	WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
	";
	$resultDeposito = mysql_fetch_array(mysql_query($sqlDeposito));

	$uraian = 'Pengambilan Titipan Bunga Deposito: #' . $resultDeposito['NO_REKENING'] . ' - ' . $resultDeposito['nama_nasabah'];

	$saldo_trans = $bunga_hari_ini - $pajak_hari_ini;

	$sqlInsertTellerTrans = "
	INSERT INTO tellertrans SET
	modul = 'DEP' ,
	tgl_trans = '".$tgl_trans."' ,
	NO_BUKTI = '".$no_bukti."' ,
	uraian = '".$uraian."' ,
	saldo_trans = '".$saldo_trans."' ,
	userid = '".$userId."' ,
	cab = '".$cab."'
	";
	mysql_query($sqlInsertTellerTrans);

	$sqlInsertDeptTrans = "
	INSERT INTO deptrans SET 
	TGL_TRANS = '".$tgl_trans."' ,
	NO_REKENING = '".$nomor_rekening."' ,
	KODE_TRANS = '03' ,
	MY_KODE_TRANS = '275' ,
	SALDO_TRANS = '".$saldo_trans."' ,
	kuitansi = '".$no_bukti."' ,
	USERID = '".$userId."' ,
	CAB = '".$cab."'
	";
	mysql_query($sqlInsertDeptTrans);

	$message = 1;
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Ambil Bunga Deposito</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<?php 
					if ($message == 1)
					{
						$linkBack = "ambil_bunga_deposito.php";
						require_once "layouts/message_success.php";
					}
					?>

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" value="" data-source="autosuggest_nomorrekening_deposito.php">
												<label for="nomor_rekening">Nomor Rekening Deposito</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="jml_deposito" name="jml_deposito" value="">
												<label for="jml_deposito">Jumlah Deposito</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="bunga_hari_ini" name="bunga_hari_ini" value="">
												<label for="bunga_hari_ini">Bunga Hari Ini</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="pajak_hari_ini" name="pajak_hari_ini" value="">
												<label for="pajak_hari_ini">Pajak Hari Ini</label>
											</div>

											<?php 
											$sqlNomorRekening = "SELECT kuitansi FROM deptrans WHERE 1 AND kuitansi != '' ORDER BY DEPTRANS_ID DESC LIMIT 1";
											$fetchNoRekening = mysql_fetch_array(mysql_query($sqlNomorRekening));
											$idMax = (int) str_replace("D-", "", $fetchNoRekening['kuitansi']);
											$idMax++;
											$kuitansi = sprintf("%08s", $idMax);
											$norekening1 = "D-";
											$norekening2 = substr($kuitansi, 3);
											?>

											<div class="form-group">
												<input type="text" class="form-control" id="no_bukti" name="no_bukti" value="<?php echo $norekening1.$norekening2; ?>">
												<label for="no_bukti">No Bukti</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

					</form>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#jml_deposito').val(ui.item.JML_DEPOSITO);
					$('#bunga_hari_ini').val(ui.item.BUNGA_BLN_INI);
					$('#pajak_hari_ini').val(ui.item.PAJAK_BLN_INI);
					$('#no_rekening_tabungan').val(ui.item.NO_REKENING_TABUNG);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>