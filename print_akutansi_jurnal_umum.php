<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$master_id = $_GET['master_id'];

$sql = "
SELECT 
trans_detail.* ,
perkiraan.kode_perk ,
perkiraan.nama_perk
FROM trans_detail 
JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
WHERE 1 
AND master_id = '".$master_id."'";
$fetch = mysql_fetch_array(mysql_query($sql));

$sqlTransMaster = "SELECT * FROM trans_master WHERE 1 AND trans_id = '".$master_id."'";
$fetchTransMaster = mysql_fetch_array(mysql_query($sqlTransMaster));
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: left;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
</style>

<div class="card body-print">
    <div class="card-body">

      <div>
        <div class="pull-left">
          Koperasi Simpan Pinjam<br />
          KSP ADIL MAKMUR FAJAR
        </div>
        <div class="pull-right" style="margin-right:50px;">
          <?php echo $fetchTransMaster['no_bukti']; ?>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="text-center" style="text-transform:uppercase;"><h4><b><u>Nota Debet/Kredit</u></b></h4></div>

      <div class="clearfix"></div>

      <table id="datatable1" class="table table-custom">
          <tr>
            <th>No. Perkiraan</th>
            <th>Keterangan</th>
            <th>Debet (Rp.)</th>
            <th>Kredit (Rp.)</th>
          </tr>
            
            <?php 
            $sqlTransDetail = "
            SELECT 
            trans_detail.* ,
            perkiraan.kode_perk ,
            perkiraan.nama_perk
            FROM trans_detail 
            JOIN perkiraan ON perkiraan.kode_perk = trans_detail.kode_perk
            WHERE 1 
            AND master_id = '".$master_id."'
            ";

            $queryTransDetail = mysql_query($sqlTransDetail);

            $totalDebet = $totalKredit = 0;

            $keterangan = null;

            while($fetchTransDetail = mysql_fetch_array($queryTransDetail))
            {
              $totalDebet += $fetchTransDetail['debet'];
              $totalKredit += $fetchTransDetail['kredit'];

              $keterangan = $fetchTransDetail['URAIAN'];
            ?>

            <tr>
              <td><?php echo $fetchTransDetail['kode_perk']; ?></td>
              <td><?php echo $fetchTransDetail['nama_perk']; ?></td>
              <td><?php echo number_format($fetchTransDetail['debet'],2,'.',','); ?></td>
              <td><?php echo number_format($fetchTransDetail['kredit'],2,'.',','); ?></td>
              <td></td>
            </tr>

            <?php
            }
            ?>

            <tr style="border-top:1px solid #000;">
              <td colspan="2">Keterangan: <?php echo $keterangan; ?></td>
              <td><b><?php echo number_format($totalDebet,2,'.',','); ?></b></td>
              <td><b><?php echo number_format($totalKredit,2,'.',','); ?></b></td>
            </tr>

      </table>

      <div class="clearfix"></div>

      <div>
        <div class="pull-right" style="margin-right:50px;">
          <table id="datatable1" class="table table-custom2">
            <tbody>
              <tr>
                <td>
                  Jakarta Utara, <?php echo date("d M Y"); ?>
                </td>
              </tr>
              <tr>
                <td>
                  <div style="text-align:center;">
                    Tanda Tangan
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                 
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>



<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>