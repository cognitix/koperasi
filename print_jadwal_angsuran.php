<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$nomor_rekening = $_GET['nomor_rekening'];
$sql = "SELECT * FROM kredit WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
$result = mysql_fetch_array(mysql_query($sql));

$JML_PINJAMAN = $result['JML_PINJAMAN'];
$JML_BUNGA_PINJAMAN = $result['JML_BUNGA_PINJAMAN'];
$JML_ANGSURAN = $result['JML_ANGSURAN'];
$PERIODE_ANGSURAN = $result['PERIODE_ANGSURAN'];
$ADM = $result['ADM'];
$TGL_REALISASI = $result['TGL_REALISASI'];	

$sqlKredit = "
SELECT
kredit.* ,
nasabah.NO_PASSPORT ,
nasabah.alamat
FROM kredit
JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id 
WHERE 1 
AND NO_REKENING = '".$nomor_rekening."'";
$resultKredit = mysql_query($sqlKredit);
$fetchKredit = mysql_fetch_array($resultKredit);

$sql = "SELECT * FROM kretrans WHERE 1 AND NO_REKENING = '".$nomor_rekening."' AND MY_KODE_TRANS = 200 ORDER BY ANGSURAN_KE ASC";
$query = mysql_query($sql);
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:12px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:2px;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
</style>

<body class="menubar-hoverable header-fixed menubar-pin ">

<div class="card body-print">
	<div class="card-body">

		<table id="datatable1" class="table table-custom">
			<tbody>
				<tr>
					<td colspan="2">
						<div>Bank Perkreditan Rakyat</div>
						<div style="font-size:20px;">PT. BPR TATA KARYA INDONESIA</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div style="text-align:center;font-size:15px;">JADWAL ANGSURAN</div>
					</td>
				</tr>
			</tbody>
		</table>

		<div>
			<div class="pull-left">
				<table id="datatable1" class="table table-custom2">
					<tbody>
						<tr>
							<td>
								<div>Nama</div>
							</td>
							<td>
								<div>:</div>
							</td>
							<td>
								<div><?php echo $fetchKredit['NASABAH']; ?></div>
							</td>
						</tr>
						<tr>
							<td>
								<div>Alamat</div>
							</td>
							<td>
								<div>:</div>
							</td>
							<td>
								<div><?php echo $fetchKredit['alamat']; ?></div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-right">
				<table id="datatable1" class="table table-custom2">
					<tbody>
						<tr>
							<td>
								<div>Jumlah Pinjaman</div>
							</td>
							<td>
								<div>:</div>
							</td>
							<td>
								<div><?php echo number_format($fetchKredit['JML_PINJAMAN'],0,'.',',') ?></div>
							</td>
						</tr>
						<tr>
							<td>
								<div>Bunga</div>
							</td>
							<td>
								<div>:</div>
							</td>
							<td>
								<?php 
								$totalBunga = ($fetchKredit['SUKU_BUNGA_PER_TAHUN'] != "0.00") ? $fetchKredit['SUKU_BUNGA_PER_TAHUN'] : $fetchKredit['SUKU_BUNGA_PER_ANGSURAN'];
								$jmlAngsuran = $fetchKredit['JML_ANGSURAN'];
								?>

								<div><?php echo round($totalBunga / $jmlAngsuran, 2); ?> % / Ang : <?php echo number_format($totalBunga,0,'.',',') ?> %</div>
							</td>
						</tr>
						<tr>
							<td>
								<div>Jangka Waktu</div>
							</td>
							<td>
								<div>:</div>
							</td>
							<td>
								<?php 
								$sqlkodeperiodepembayaran = mysql_query("SELECT * FROM kodeperiodepembayaran WHERE 1 AND kode_periode_pembayaran = '".$fetchKredit['PERIODE_ANGSURAN']."'");
								$rowkodeperiodepembayaran = mysql_fetch_array($sqlkodeperiodepembayaran);
								?>
								<div><?php echo $jmlAngsuran; ?> <?php echo $rowkodeperiodepembayaran['deskripsi_periode_pembayaran']; ?></div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<table id="datatable1" class="table table-custom">
			<thead>
				<tr>
					<th>Angs</th>
					<th>Angsuran Pokok</th>
					<th>Angsuran Bunga</th>
					<th>Administrasi</th>
					<th>Jumlah</th>
					<th>Saldo</th>
					<th>Paraf</th>
				</tr>
			</thead>
			<tbody>

				<?php 
				$sisaPinjaman = $totalPokok = $totalBunga = $totalAdmin = $totalAngsuran = $i = 0;

				while($result = mysql_fetch_array($query))
				{

				//for($i=0;$i<=$JML_ANGSURAN;$i++)
				//{

					if ($i == 0)
					{
						$sisaPinjaman += $JML_PINJAMAN;
					}
					//else
					//{
						$tgl = date('d/m/Y', strtotime($result['TGL_TRANS']));

						$pokok = $result['POKOK_TRANS'];
						$bunga = $result['BUNGA_TRANS'];
						$admin = $result['ADMIN_TRANS'];
						$angsuran = $pokok + $bunga + $admin;

						$sisaPinjaman -= $angsuran;

						$totalPokok += $pokok;
						$totalBunga += $bunga;
						$totalAdmin += $admin;
						$totalAngsuran += $angsuran;
				?>
				<tr>
					<td class="text-center"><?php echo ($i+1); ?></td>
					<td class="text-right"><?php echo number_format($pokok,0,'.',','); ?></td>
					<td class="text-right"><?php echo number_format($bunga,0,'.',','); ?></td>

					<td class="text-right"><?php echo number_format($admin,0,'.',','); ?></td>
					<td class="text-right"><?php echo number_format($angsuran,0,'.',','); ?></td>
					<td class="text-right"><?php echo ($sisaPinjaman > 0) ? number_format($sisaPinjaman,0,'.',',') : 0; ?></td>
				</tr>
				<?php
					//}
					$i++;
				}
				?>

				<tr class="total">
					<td class="text-left"><b>Total</b></td>
					<td class="text-right"><?php echo number_format($totalPokok,0,'.',','); ?></td>
					<td class="text-right"><?php echo number_format($totalBunga,0,'.',','); ?></td>

					<td class="text-right"><?php echo number_format($totalAdmin,0,'.',','); ?></td>
					<td class="text-right"><?php echo number_format($totalAngsuran,0,'.',','); ?></td>
					<td class="text-right"></td>
					<td class="text-right"></td>
				</tr>

			</tbody>
		</table>

		<div>
			<div class="pull-left">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Nasabah yang bersangkutan</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:120px;text-align:center;">
									(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-right" style="margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
								Jakarta Utara, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 20 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">
									PT. BPR TATA KARYA INDONESIA<br />
									Membuat
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
								<div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
									Staf Admin Kredit
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</div>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>



<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>