<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "overbook_tabungan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi = date('Y-m-d');

$message = 0;

if (isset($_POST['submit']))
{
	$nomor_rekening = $_POST['nomor_rekening'];
	$tgl_transaksi = $_POST['tgl_transaksi'];
	$jml_deposito = $_POST['jml_deposito'];
	$bunga_hari_ini = $_POST['bunga_hari_ini'];
	$pajak_hari_ini = $_POST['pajak_hari_ini'];
	$no_rekening_tabungan = $_POST['no_rekening_tabungan'];

	$sqlDeposito = "
	SELECT * FROM deposito WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
	";
	$resultDeposito = mysql_fetch_array(mysql_query($sqlDeposito));
	$TGL_REGISTRASI = date('Y-m-d', strtotime($resultDeposito['TGL_REGISTRASI'] . "+1 month"));

	$sqlTabTrans = "
	INSERT INTO tabtrans SET 
	SALDO_TRANS = '".$bunga_hari_ini."' ,
	TGL_TRANS = '".$tgl_transaksi."' ,
	NO_REKENING = '".$no_rekening_tabungan."' ,
	KODE_TRANS = '03' ,
	MY_KODE_TRANS = '175' ,
	keterangan = 'SYS-BNG AC. ".$no_rekening_tabungan."' ,
	LINK_MODUL = 'DEP' ,
	LINK_REKENING = '".$nomor_rekening."'
	";
	mysql_query($sqlTabTrans);

	$sqlTabTrans = "
	INSERT INTO tabtrans SET 
	SALDO_TRANS = '".$pajak_hari_ini."' ,
	TGL_TRANS = '".$tgl_transaksi."' ,
	NO_REKENING = '".$no_rekening_tabungan."' ,
	KODE_TRANS = '04' ,
	MY_KODE_TRANS = '276' ,
	keterangan = 'SYS-PJK AC. ".$no_rekening_tabungan."' ,
	LINK_MODUL = 'DEP' ,
	LINK_REKENING = '".$nomor_rekening."'
	";
	mysql_query($sqlTabTrans);

	$sqlTabung = "
	SELECT * FROM tabung WHERE 1 AND NO_REKENING = '".$no_rekening_tabungan."'
	";
	$resultTabung = mysql_fetch_array(mysql_query($sqlTabung));

	$saldo_akhir = $resultTabung['SALDO_AKHIR'] + $bunga_hari_ini - $pajak_hari_ini;
	$saldo_nominatif = $resultTabung['SALDO_NOMINATIF'] + $bunga_hari_ini - $pajak_hari_ini;
	$simpanan_sukarela = $resultTabung['simpanan_sukarela'] + $bunga_hari_ini - $pajak_hari_ini;

	$sqlUpdateTabung = "
	UPDATE tabung SET 
	SALDO_AKHIR = '".$saldo_akhir."' ,
	SALDO_NOMINATIF = '".$saldo_nominatif."' ,
	simpanan_sukarela = '".$simpanan_sukarela."'
	WHERE 1
	AND NO_REKENING = '".$no_rekening_tabungan."' 
	";
	mysql_query($sqlUpdateTabung);

	$sqlUpdateDeposito = "
	UPDATE deposito SET 
	TGL_REGISTRASI = '".$TGL_REGISTRASI."' ,
	BUNGA_BLN_INI = 0 ,
	PAJAK_BLN_INI = 0 
	WHERE 1
	AND NO_REKENING = '".$nomor_rekening."'
	";
	mysql_query($sqlUpdateDeposito);

	$message = 1;
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Overbook Tabungan</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<?php 
					if ($message == 1)
					{
						$linkBack = "overbook_tabungan.php";
						require_once "layouts/message_success.php";
					}
					?>

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" value="" data-source="autosuggest_nomorrekening_deposito.php">
												<label for="nomor_rekening">Nomor Rekening Deposito</label>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php echo $tgl_transaksi; ?>" required>
														<label for="tgl_transaksi">Tanggal Transaksi</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="jml_deposito" name="jml_deposito" value="">
												<label for="jml_deposito">Jumlah Deposito</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="bunga_hari_ini" name="bunga_hari_ini" value="">
												<label for="bunga_hari_ini">Bunga Hari Ini</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="pajak_hari_ini" name="pajak_hari_ini" value="">
												<label for="pajak_hari_ini">Pajak Hari Ini</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="no_rekening_tabungan" name="no_rekening_tabungan" value="">
												<label for="no_rekening_tabungan">No Rekening Tabungan</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

					</form>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tgl_transaksi').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#jml_deposito').val(ui.item.JML_DEPOSITO);
					$('#bunga_hari_ini').val(ui.item.BUNGA_BLN_INI);
					$('#pajak_hari_ini').val(ui.item.PAJAK_BLN_INI);
					$('#no_rekening_tabungan').val(ui.item.NO_REKENING_TABUNG);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>