<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "print_cover_buku_simpanan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Print Cover Buku Simpanan</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data" action="cetak_cover_buku_simpanan.php">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<?php 

											$nomor_rekening = (!empty($_GET['nomor_rekening'])) ? $_GET['nomor_rekening'] : "";
											$nasabah_id = $nama_nasabah = $TGL_REGISTRASI = "";

											if ($nomor_rekening)
											{

												$sql = "SELECT 
														tabung.*,
														nasabah.nama_nasabah ,
														nasabah.alamat
														FROM tabung 
														JOIN nasabah ON tabung.NASABAH_ID = nasabah.nasabah_id
														WHERE 1
														AND NO_REKENING = '".$nomor_rekening."'
														";

												$fetch = mysql_fetch_array( mysql_query($sql) );

												$nasabah_id = $fetch['NASABAH_ID'];
												$nama_nasabah = $fetch['nama_nasabah'];
												$TGL_REGISTRASI = $fetch['TGL_REGISTRASI'];
											}

											?>

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" data-source="autosuggest_nomorrekening_all.php" value="<?php echo $nomor_rekening; ?>">
												<label for="nomor_rekening">Nomor Rekening</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nasabah_id" name="nasabah_id" readonly value="<?php echo $nasabah_id; ?>">
												<label for="nasabah_id">Anggota ID</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly value="<?php echo $nama_nasabah; ?>">
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="TGL_REGISTRASI" name="TGL_REGISTRASI" readonly value="<?php echo $TGL_REGISTRASI; ?>">
												<label for="TGL_REGISTRASI">Tgl Buka Rekening / Tgl Registrasi</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Print</button>

					</form>
					
				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#nasabah_id').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);
					$('#TGL_REGISTRASI').val(ui.item.TGL_REGISTRASI);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>