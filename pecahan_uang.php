<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "pecahan_uang";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi = (!empty($_GET['tgl_transaksi'])) ? $_GET['tgl_transaksi'] : date('Y-m-d');
$userid = (!empty($_GET['userid'])) ? $_GET['userid'] : null;

$saldo_akhir = (!empty($_GET['saldo'])) ? $_GET['saldo'] : 0;

if (isset($_POST['print']))
{
	$jmls = $_POST['jml'];
	$totals = $_POST['total'];

	$jml_debet = $_POST['jml_debet'];
	$jml_kredit = $_POST['jml_kredit'];
	
	$saldo_awal = $_POST['saldo_awal'];
	$saldo_akhir = $_POST['saldo_akhir'];

	$_SESSION['jmls'] = $jmls;
	$_SESSION['totals'] = $totals;

	$_SESSION['jml_debet'] = $jml_debet;
	$_SESSION['jml_kredit'] = $jml_kredit;

	$_SESSION['saldo_awal'] = $saldo_awal;
	$_SESSION['saldo_akhir'] = $saldo_akhir;

	$_SESSION['tgl_transaksi'] = $tgl_transaksi;

	header('location:print_pecahan_uang.php');
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Pecahan Uang</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php echo $tgl_transaksi; ?>" required>
														<label for="tgl_transaksi">Tanggal Transaksi</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="userid" name="userid" data-source="autosuggest_userid.php" value="<?php echo $userid; ?>" />
												<label for="userid">User ID</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Search</button>

					</form>
					
					<hr />

					<?php 
					$saldoAkhir = 0;

					if (!empty($tgl_transaksi) && !empty($userid))
					{

						$sqlBefore = "
						SELECT 
						*
						FROM tellertrans 
						WHERE 1
						AND tgl_trans < '".date('Y-m-d', strtotime($tgl_transaksi))."'
						AND userid = '".$userid."'
						ORDER BY tgl_trans DESC
						";
						$queryBefore = mysql_query($sqlBefore);

						$saldoAwal = 0;
						$jml_obkredit_before = $jml_obdebet_before = $jml_kredit_before = $jml_debet_before = 0;
						while($resultBefore = mysql_fetch_array($queryBefore))
						{
							if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "300")
							{
								$jml_obkredit_before += $resultBefore['saldo_trans'];
							}
							if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "200")
							{
								$jml_obdebet_before += $resultBefore['saldo_trans'];
							}

							$kredit = 0;
							if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "300")
							{
								$jml_kredit_before += $resultBefore['saldo_trans'];
								//$saldoAwal -= $jml_kredit_before;

								$kredit = $resultBefore['saldo_trans'];
							}

							$debet = 0;
							if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "200")
							{
								$jml_debet_before += $resultBefore['saldo_trans'];
								//$saldoAwal += $jml_kredit_before;

								$debet = $resultBefore['saldo_trans'];
							}

							$saldoAwal += $debet - $kredit;
						}

						$sql = "
						SELECT 
						*
						FROM tellertrans 
						WHERE 1
						AND tgl_trans = '".$tgl_transaksi."'
						AND userid = '".$userid."'
						";

						$query = mysql_query($sql);

						$saldo = $saldoAwal;
						$jml_obkredit = $jml_obdebet = $jml_kredit = $jml_debet = 0;

						while($result = mysql_fetch_array($query))
						{
							$saldoTrans = 0;
							$kredit = 0;
							if ($result['tob'] == "T" && $result['my_kode_trans'] == "300")
							{
								$jml_kredit += $result['saldo_trans'];
								$saldoTrans = $result['saldo_trans'];

								$kredit = $result['saldo_trans'];
							}

							$saldoTrans = 0;
							$debet = 0;
							if ($result['tob'] == "T" && $result['my_kode_trans'] == "200")
							{
								$jml_debet += $result['saldo_trans'];
								$saldoTrans = $result['saldo_trans'];

								$debet = $result['saldo_trans'];
							}

							$saldo += $debet - $kredit;
						}

						$mutasi = $jml_debet - $jml_kredit;

						$saldo_akhir = $saldo;

					?>

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-7">

								<div class="card">
									<div class="card-body">


										<div class="table-responsive datatable1">
											<table id="datatable1" class="table table-striped table-hover">
												<tr>
													<th>Jenis Dominasi</th>
													<th>Nilai</th>
													<th>Jumlah</th>
													<th>Total</th>
												</tr>
												<tbody>
													<?php 
													$sqlDominasi = "SELECT * FROM kodednominasi WHERE 1 ORDER BY Jenis_D_Nominasi ASC";
													$queryDominasi = mysql_query($sqlDominasi);
													while($result = mysql_fetch_array($queryDominasi))
													{
													?>
													<tr>
														<td><?php echo $result['Jenis_D_Nominasi']; ?></td>
														<td id="nilai" data-nilai="<?php echo $result['Nilai_D_Nominasi']; ?>"><?php echo $result['Nilai_D_Nominasi']; ?></td>
														<td><input type="text" name="jml[]" id="jml" value="0" class="form-control jml" /></td>
														<td><input type="text" name="total[]" id="total" value="0" class="form-control total" readonly /></td>
													</tr>
													<?php
													}
													?>
													<tr>
														<td><b>Jumlah</b></td>
														<td></td>
														<td></td>
														<td id="total_all">0</td>
													</tr>
													<tr>
														<td><b>Saldo Awal</b></td>
														<td></td>
														<td></td>
														<td><input type="text" class="form-control" name="saldo_awal" id="saldo_awal" value="<?php echo $saldoAwal; ?>" /></td>
													</tr>
													<tr>
														<td><b>Mutasi Debet</b></td>
														<td></td>
														<td></td>
														<td><input type="text" class="form-control" name="jml_debet" id="jml_debet" value="<?php echo $jml_debet; ?>" /></td>
													</tr>
													<tr>
														<td><b>Mutasi Kredit</b></td>
														<td></td>
														<td></td>
														<td><input type="text" class="form-control" name="jml_kredit" id="jml_kredit" value="<?php echo $jml_kredit; ?>" /></td>
													</tr>
													<tr>
														<td><b>Saldo Akhir</b></td>
														<td></td>
														<td></td>
														<td><input type="text" class="form-control" name="saldo_akhir" id="saldo_akhir" value="<?php echo $saldo_akhir; ?>" /></td>
													</tr>
													<tr>
														<td><b>Status</b></td>
														<td></td>
														<td></td>
														<td id="status"></td>
													</tr>
												</tbody>	
											</table>
										</div>

										<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="print">Print</button>

									</div>
								</div>

							</div>


						</div>

					</form>
					
					<hr />

					<?php 
					}
					?>

				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){

	$('#tgl_transaksi').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#userid').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#userid").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$("#userid").val(ui.item.USERID);

					return false;
				},
				focus: function( event, ui ) {

			        $("#userid").val(ui.item.USERID);
			        
			        return false;
			    },
			});
		}
	});

	$('.datatable1 table').find('.jml').keyup(function(){

		var el = $(this);
		var jml = parseInt(el.val());
		var tr = el.closest('tr');
		var nilai = parseFloat(tr.find('#nilai').attr('data-nilai'));
		jml = (isNaN(jml)) ? 0 : jml;
		var total = jml * nilai;
		tr.find('#total').val(total);

		var total_all = 0;
		$('.datatable1 table').find('.jml').each(function(){
			var el = $(this);
			var tr = el.closest('tr');
			var total = tr.find('#total').val();
			total = (isNaN(total)) ? 0 : total;
			total_all += parseFloat(total);
		});

		$('#total_all').text(total_all);
		
		var saldo_akhir = parseFloat($('#saldo_akhir').val());

		var status = (saldo_akhir == total_all) ? 'Balance' : 'Not Balance';

		$('#status').text(status);
	});

	$('#saldo_akhir').keyup(function(){
		var el = $(this);
		var saldo_akhir = parseFloat(el.val());
		var total_all = parseFloat($('#total_all').text());
		var status = (saldo_akhir == total_all) ? 'Balance' : 'Not Balance';
		$('#status').text(status);
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>	