<?php
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl_transaksi = $_GET['tgl_transaksi'];
?>
<style>
    tbody > tr:nth-child(2n+1) > td, tbody > tr:nth-child(2n+1) > th {
        background-color: #00000;
    }
    table{
        width: 80%;
        margin: auto;
        border-collapse: collapse;
        box-shadow: white 3px;
    }
    thead tr {
        background-color: #00000;
    }
</style>
<LINK rel="stylesheet" type"text/css" href="layar.css" media="screen">  
<LINK rel="stylesheet" type"text/css" href="print.css" media="print">  

<table>
    <thead>
    <tr>
      <td align="center"><span lang="zh-cn"><font size="3" face="Arial"><b>LAPORAN TRANSAKSI KREDIT KSP ADIL AMKMUR FAJAR</b></font></span></td>
    </tr>
    </thead>
</table>
<?php 

$sql = "
SELECT  
kredit.JML_PINJAMAN ,
nasabah.nama_nasabah ,
nasabah.NO_PASSPORT ,
kredit.TGL_REALISASI,
kredit.NO_REKENING 
FROM kredit 
JOIN nasabah ON kredit.NASABAH_ID = nasabah.nasabah_id 
WHERE 1 
AND TGL_REALISASI ='".$tgl_transaksi."' ";
$result = mysql_fetch_array(mysql_query($sql));
{
$totalPinjaman += $result['JML_PINJAMAN'];
$no=1;
?>

<br /><br />

<table border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Nasabah</th>
            <th>No Passport</th>
            <th>No Rekening</th>
            <th>Tgl Transaksi</th>
            <th>Pokok Trans</th>
        </tr>
    </thead>
    
    <body>
    <?php
        echo("<tr><td><font face='Arial' size='2'>".$no++."</font></td>");
        echo("<td><font face='Arial' size='2'>".$result['nama_nasabah']."</font></td>");
        echo("<td><font face='Arial' size='2'>".$result['NO_PASSPORT']."</font></td>");
        echo("<td><font face='Arial' size='2'>".$result['NO_REKENING']."</font></td>");
        echo("<td><font face='Arial' size='2'>".$result['TGL_REALISASI']."</font></td>");
        echo("<td align='right'><font face='Arial' size='2'>".number_format($result['JML_PINJAMAN'],0,'',',')."</font></td>");
				echo("</tr>");
    }

    echo '<tr><td colspan="5" align="center">Total</td><td align="right"><font face="Arial" size="2">'.number_format($totalPinjaman,0,"",",").'</td></font></tr>';
?>
</tbody>
</table>