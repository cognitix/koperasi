<?php 
require_once "connect.php";

$sql = "SELECT 
		deposito.*,
		nasabah.nama_nasabah ,
		nasabah.alamat ,
		nasabah.PATH_TTANGAN ,
		tabung.NO_REKENING as NO_REKENING_TABUNG
		FROM deposito 
		JOIN nasabah ON deposito.NASABAH_ID = nasabah.nasabah_id
		JOIN tabung ON tabung.NASABAH_ID = nasabah.nasabah_id
		WHERE 1
		AND deposito.STATUS_AKTIF = 1
		";

$fetch = mysql_query($sql);

$datas = array();

while($row = mysql_fetch_array($fetch))
{
	$datas[] = array(
		'NO_REKENING' => $row['NO_REKENING'],
		'nasabah_id' => $row['NASABAH_ID'],
		'nama_nasabah' => $row['nama_nasabah'],
		'JML_DEPOSITO' => number_format($row['JML_DEPOSITO'],0,'',''),
		'BUNGA_BLN_INI' => number_format($row['BUNGA_BLN_INI'],0,'',''),
		'PAJAK_BLN_INI' => number_format($row['PAJAK_BLN_INI'],0,'',''),
		'NO_REKENING_TABUNG' => $row['NO_REKENING_TABUNG'],

		'value' => $row['NO_REKENING'] . ' - ' . $row['NASABAH_ID'] . ' - ' . $row['nama_nasabah']
	);
}

echo json_encode($datas);

?>