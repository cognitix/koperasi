<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "print_buku_simpanan";

$nomor_rekening = $_POST['nomor_rekening'];
$tabTransIds = $_POST['tabTransIds'];
//print_r($tabTransIds);die();

//$sql = "UPDATE tabtrans SET FLAG_CETAK = 'Y' WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
//mysql_query($sql);

$sql = "SELECT 
		tabtrans.*,

		nasabah.nama_nasabah ,
		
		IFNULL(passwd.USERNAME, 'ADM') as USERNAME,
		
		kodetranstabungan.DESKRIPSI_TRANS,
		kodetranstabungan.TYPE_TRANS,
		kodetranstabungan.GL_TRANS

		FROM 
		tabtrans 
		JOIN kodetranstabungan ON 
			kodetranstabungan.KODE_TRANS = tabtrans.KODE_TRANS
		JOIN tabung ON 
			tabung.NO_REKENING = tabtrans.NO_REKENING
		JOIN nasabah ON 
			nasabah.nasabah_id = tabung.NASABAH_ID
		LEFT JOIN passwd ON 
			passwd.USERID = tabtrans.NO_TELLER
		WHERE 1 
		AND tabtrans.NO_REKENING = '".$nomor_rekening."'
		AND TABTRANS_ID IN (".implode(",", $tabTransIds).")
		ORDER BY TABTRANS_ID ASC
		";

$query = mysql_query($sql);

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:10px;
	font-face:courier new;
}
.table > tbody > tr > td{
	border-top:0;
	padding:0;
}
.body-print h3{
	margin-top:0;
}
.card-body{
	padding:0;
}
</style>

<div class="card body-print">
	<div class="card-body">
		<table id="datatable1" class="table">
			<tbody>
<?php 

$no = 1;
$saldo = 0;
$jmlCetak = 0;
while($row = mysql_fetch_array($query))
{

	//if ($row['FLAG_CETAK'] == "Y") continue;
	
	//$style = ($row['FLAG_CETAK'] == "Y") ? 'display:none;' : 'display:block;';

	$jmlCetak++;

	$debet = ($row['TYPE_TRANS'] == "D") ? $row['SALDO_TRANS'] : 0;
	$kredit = ($row['TYPE_TRANS'] == "K") ? $row['SALDO_TRANS'] : 0;
	$saldo += $kredit - $debet;
  
  if ($row['FLAG_CETAK'] == "N"){
?>
<tr>
	<td width="3%" style="text-align:left;"><?php echo $row['TABTRANS_ID']; $no++; ?></td>
	<td width="5%" style="text-align:center;"><?php echo date('d/m/Y', strtotime($row['TGL_TRANS'])); ?></td>
	<td width="7%" style="text-align:left;"><?php echo $row['KODE_TRANS']; ?></td>
	<td width="5%"><?php echo number_format($debet,2,'.',','); ?></td>
	<td width="5%"><?php echo number_format($kredit,2,'.',','); ?></td>
	<td width="5%"><?php echo number_format($saldo,2,'.',','); ?></td>
	<td width="3%"><span style="text-align:center;"><?php echo substr(strtoupper($row['USERNAME']), 0, 3); ?></span></td>
</tr>

<?php
}else{
?>
<tr style="height:20px;">
<td></td>
<td></td>
<td></td>

<td></td>
<td></td>
<td></td>

<td></td>
</tr>
<?php
}
}
?>
			</tbody>
		</table>
	</div>
</div>

<?php 

if ($jmlCetak == 0)
{
?>

<div class="alert alert-callout alert-danger" role="alert">
	<strong>Oh snap!</strong> Tidak ada data yang dapat di cetak atau semua data telah tercetak pada halaman ini.
</div>

<?php
}
else
{

foreach($tabTransIds as $tabTransId)
{
	$sql = "UPDATE tabtrans SET FLAG_CETAK = 'Y' WHERE 1 AND NO_REKENING = '".$nomor_rekening."' AND TABTRANS_ID = '".$tabTransId."'";
	mysql_query($sql);
}
?>

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php 
}
?>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>