<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "laporan_transaksi_teller";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi1 = (!empty($_GET['tgl_transaksi1'])) ? $_GET['tgl_transaksi1'] : date('Y-m-d');
$tgl_transaksi2 = (!empty($_GET['tgl_transaksi2'])) ? $_GET['tgl_transaksi2'] : date('Y-m-d', strtotime("+7 day"));
$userid = (!empty($_GET['userid'])) ? $_GET['userid'] : null;

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Laporan Transaksi Teller</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="get" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="tgl_from_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi1" name="tgl_transaksi1" value="<?php echo $tgl_transaksi1; ?>" required>
														<label for="tgl_transaksi1">Tanggal Transaksi From</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="tgl_to_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl_transaksi2" name="tgl_transaksi2" value="<?php echo $tgl_transaksi2; ?>" required>
														<label for="tgl_transaksi2">Tanggal Transaksi To</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="userid" name="userid" data-source="autosuggest_userid.php" value="<?php echo $userid; ?>" />
												<label for="userid">User ID</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Search</button>

					</form>
					
					<hr />

					<?php 
					$saldoAkhir = 0;

					if (!empty($tgl_transaksi1) && !empty($tgl_transaksi2) && !empty($userid))
					{

						/*
						$sqlCheckTgl = "
						SELECT 
						*
						FROM tellertrans 
						WHERE 1
						AND tgl_trans < '".date('Y-m-d', strtotime($tgl_transaksi))."'
						AND userid = '".$userid."'
						ORDER BY tgl_trans DESC
						LIMIT 1
						";
						$fetchCheckTgl = mysql_fetch_array(mysql_query($sqlCheckTgl));
						$tgl_trans_saldo_awal = $fetchCheckTgl['tgl_trans'];

						$sqlBefore = "
						SELECT 
						*
						FROM tellertrans 
						WHERE 1
						AND tgl_trans = '".date('Y-m-d', strtotime($tgl_trans_saldo_awal))."'
						AND userid = '".$userid."'
						ORDER BY tgl_trans DESC
						";
						$queryBefore = mysql_query($sqlBefore);

						$saldoAwal = 0;
						$jml_obkredit_before = $jml_obdebet_before = $jml_kredit_before = $jml_debet_before = 0;
						while($resultBefore = mysql_fetch_array($queryBefore))
						{
							if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "300")
							{
								$jml_obkredit_before += $resultBefore['saldo_trans'];
							}
							if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "200")
							{
								$jml_obdebet_before += $resultBefore['saldo_trans'];
							}
							if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "300")
							{
								$jml_kredit_before += $resultBefore['saldo_trans'];
								//$saldoAwal -= $jml_kredit_before;
							}
							if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "200")
							{
								$jml_debet_before += $resultBefore['saldo_trans'];
								//$saldoAwal += $jml_kredit_before;
							}
						}

						$saldoAkhir = $saldoAwal = $jml_debet_before - $jml_kredit_before;
						*/
						
						$sqlBefore = "
						SELECT 
						*
						FROM tellertrans 
						WHERE 1
						AND tgl_trans < '".date('Y-m-d', strtotime($tgl_transaksi1))."'
						AND userid = '".$userid."'
						ORDER BY tgl_trans DESC
						";
						$queryBefore = mysql_query($sqlBefore);

						$saldoAwal = 0;
						$jml_obkredit_before = $jml_obdebet_before = $jml_kredit_before = $jml_debet_before = 0;
						while($resultBefore = mysql_fetch_array($queryBefore))
						{
							if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "300")
							{
								$jml_obkredit_before += $resultBefore['saldo_trans'];
							}
							if ($resultBefore['tob'] == "O" && $resultBefore['my_kode_trans'] == "200")
							{
								$jml_obdebet_before += $resultBefore['saldo_trans'];
							}

							$kredit = 0;
							if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "300")
							{
								$jml_kredit_before += $resultBefore['saldo_trans'];
								//$saldoAwal -= $jml_kredit_before;

								$kredit = $resultBefore['saldo_trans'];
							}

							$debet = 0;
							if ($resultBefore['tob'] == "T" && $resultBefore['my_kode_trans'] == "200")
							{
								$jml_debet_before += $resultBefore['saldo_trans'];
								//$saldoAwal += $jml_kredit_before;

								$debet = $resultBefore['saldo_trans'];
							}

							$saldoAwal += $debet - $kredit;
						}

						$sql = "
						SELECT 
						*
						FROM tellertrans 
						WHERE 1
						AND tgl_trans BETWEEN '".$tgl_transaksi1."' AND '".$tgl_transaksi2."'
						AND userid = '".$userid."'
						";

						$query = mysql_query($sql);
					?>

					<section class="style-default-bright">

						<div class="row">
							<div class="col-lg-12">

								<div class="table-responsive">
									<table id="datatable1" class="table table-striped table-hover">

										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td colspan="2"><b>Saldo Awal</b></td>
											<td class="text-center"></td>
											<td class="text-center"></td>
											<td colspan="2" class="text-center">
												<b><?php echo number_format($saldoAwal,2,'.',','); ?></b>
											</td>
										</tr>


										<tr>
											<th>Modul</th>
											<th>Tanggal</th>
											<th>No.Bukti</th>
											<th>Ket</th>
											<th>TOB</th>
											<th>OB Kredit</th>
											<th>OB Debet</th>
											<th>Kredit</th>
											<th>Debet</th>
										</tr>

										<tbody>

											<?php 
											$saldo = $saldoAwal;
											$jml_obkredit = $jml_obdebet = $jml_kredit = $jml_debet = 0;

											while($result = mysql_fetch_array($query))
											{
											?>

											<tr>
												<td><?php echo $result['modul']; ?></td>
												<td><?php echo date("d M Y", strtotime($result['tgl_trans'])); ?></td>
												<td><?php echo $result['NO_BUKTI']; ?></td>
												<td><?php echo $result['uraian']; ?></td>
												<td><?php echo $result['tob']; ?></td>
												<td>
												<?php 
												$saldoTrans = 0;
												if ($result['tob'] == "O" && $result['my_kode_trans'] == "300")
												{
													$jml_obkredit += $result['saldo_trans'];
													$saldoTrans = $result['saldo_trans'];
												}
												
												echo number_format($saldoTrans,2,'.',',');	

												?>
												</td>
												<td>
												<?php 
												$saldoTrans = 0;
												if ($result['tob'] == "O" && $result['my_kode_trans'] == "200")
												{
													$jml_obdebet += $result['saldo_trans'];
													$saldoTrans = $result['saldo_trans'];
												}

												echo number_format($saldoTrans,2,'.',',');
												?>
												</td>
												<td>
												<?php 
												$saldoTrans = 0;
												$kredit = 0;
												if ($result['tob'] == "T" && $result['my_kode_trans'] == "300")
												{
													$jml_kredit += $result['saldo_trans'];
													$saldoTrans = $result['saldo_trans'];

													$kredit = $result['saldo_trans'];
												}

												echo number_format($saldoTrans,2,'.',',');
												?>
												</td>
												<td>
												<?php 
												$saldoTrans = 0;
												$debet = 0;
												if ($result['tob'] == "T" && $result['my_kode_trans'] == "200")
												{
													$jml_debet += $result['saldo_trans'];
													$saldoTrans = $result['saldo_trans'];

													$debet = $result['saldo_trans'];
												}

												echo number_format($saldoTrans,2,'.',',');
												?>
												</td>
											</td>

											<?php

												$saldo += $debet - $kredit;

											}
											?>

											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td><b>Total</b></td>
												<td class="text-center"><b><?php echo number_format($jml_obkredit,2,'.',','); ?></b></td>
												<td class="text-center"><b><?php echo number_format($jml_obdebet,2,'.',','); ?></b></td>
												<td class="text-center"><b><?php echo number_format($jml_kredit,2,'.',','); ?></b></td>
												<td class="text-center"><b><?php echo number_format($jml_debet,2,'.',','); ?></b></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td colspan="2"><b>Mutasi Hari ini</b></td>
												<td class="text-center"></td>
												<td class="text-center"></td>
												<td colspan="2" class="text-center"><b>
													<?php 
													$mutasi = $jml_debet - $jml_kredit;
													//echo number_format($saldo,2,'.',','); 
													echo number_format($mutasi,2,'.',','); 
													?>
												</b>
												<?php /*<a href="pecahan_uang.php?saldo=<?php echo $saldo; ?>" class="btn btn-link">Cek Pecahan Uang</a>*/ ?>
												</td>
											</tr>

											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td colspan="2"><b>Saldo Akhir</b></td>
												<td class="text-center"></td>
												<td class="text-center"></td>
												<td colspan="2" class="text-center"><b>
													<?php 
													echo number_format($saldo,2,'.',','); 
													//echo number_format($jml_debet - $jml_kredit,2,'.',','); 
													?>
												</b></td>
											</tr>


										</tbody>
									</table>
								</div>

								<a href="print_laporan_transaksi_teller.php?tgl_transaksi1=<?php echo $tgl_transaksi1; ?>&tgl_transaksi2=<?php echo $tgl_transaksi2; ?>&userid=<?php echo $userid; ?>" class="btn ink-reaction btn-raised btn-primary">Print</a>

							</div>
						</div>	

					</section>	

					<?php 
					}
					?>

				</div>	
			</section>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tgl_from_container, #tgl_to_container').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$.ajax({
		url: $('#userid').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#userid").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$("#userid").val(ui.item.USERID);

					return false;
				},
				focus: function( event, ui ) {

			        $("#userid").val(ui.item.USERID);
			        
			        return false;
			    },
			});
		}
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>