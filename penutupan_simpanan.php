<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "penutupan_simpanan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	$nomor_rekening = $_POST['nomor_rekening'];

	$tgl_trans = date('Y-m-d');
	$userId = $user['USERID'];

	$nama_nasabah = $_POST['nama_nasabah'];
	$nasabah_id = $_POST['NASABAH_ID'];
	$kode_trans = 10;
	$jumlah = $_POST['ADM_PER_BLN'];
	$kuitansi = $_POST['kuitansi'];
	$keterangan = $_POST['keterangan'];
	$tanggal_input = $tglsystem;

	$sqlTabung = "SELECT * FROM tabung WHERE 1 AND NO_REKENING = '".$nomor_rekening."'";
	$fetchTabung = mysql_fetch_array(mysql_query($sqlTabung));

	if (!empty($fetchTabung['NO_REKENING']))
	{

		// insert tabtrans
		$sql = "INSERT INTO tabtrans SET 
				TGL_TRANS = '".$tgl_trans."' ,
				NO_REKENING = '".$nomor_rekening."' ,
				KODE_TRANS = '".$kode_trans."' ,
				SALDO_TRANS = '".$jumlah."' ,
				kuitansi = '".$kuitansi."' ,
				USERID = '".$userId."' ,
				NO_TELLER = '".$userId."' ,
				keterangan = '".$keterangan."' ,
				TGL_INPUT = '".$tanggal_input."' ,
				MY_KODE_TRANS = '300'
		";

		mysql_query($sql);

		$tabTransId = mysql_insert_id();

		$sqlKodeTrans = "SELECT * FROM kodetranstabungan WHERE 1 AND KODE_TRANS = '".$kode_trans."'";
		$fetchKodeTrans = mysql_fetch_array(mysql_query($sqlKodeTrans));
		$uraian = $fetchKodeTrans['DESKRIPSI_TRANS'] . " : " . $nomor_rekening . " - " . $nama_nasabah;

		// insert tellertrans
		$sql = "INSERT INTO tellertrans SET 
				modul = 'TAB' ,
				tgl_trans = '".$tgl_trans."' ,
				NO_BUKTI = '".$kuitansi."' ,
				uraian = '".$uraian."' ,
				saldo_trans = '".$jumlah."' ,
				userid = '".$nasabah_id."' ,
				my_kode_trans = '300'
		";

		mysql_query($sql);

		$SALDO_SETORAN = $fetchTabung['SALDO_SETORAN'] - $jumlah;
		$SALDO_SETORAN = ($SALDO_SETORAN <= 0) ? 0 : $SALDO_SETORAN;

		$simpanan_sukarela = $fetchTabung['simpanan_sukarela'] - $jumlah;
		$simpanan_sukarela = ($simpanan_sukarela <= 0) ? 0 : $simpanan_sukarela;

		// update tabung
		$sql = "UPDATE tabung SET 
					SALDO_SETORAN = '".$SALDO_SETORAN."' ,
					SALDO_NOMINATIF = 0 ,
					simpanan_sukarela = '".$simpanan_sukarela."' ,
					STATUS_AKTIF = 3
					WHERE 1 AND NO_REKENING = '".$nomor_rekening."'
			";

		mysql_query($sql);

		$message = 1;

	}
	else
	{
		$message = 2;
	}
}

?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php 
			if ($message == 0)
			{
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Penutupan Simpanan</li>
					</ol>
				</div>
				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" data-source="autosuggest_nomorrekening.php">
												<label for="nomor_rekening">Nomor Rekening</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="NASABAH_ID" name="NASABAH_ID" readonly>
												<label for="NASABAH_ID">Anggota Id</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="nama_nasabah" name="nama_nasabah" readonly>
												<label for="nama_nasabah">Nama Anggota</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="SUKU_BUNGA" name="SUKU_BUNGA" readonly>
												<label for="SUKU_BUNGA">Bunga</label>
												<p class="help-block">Default: 4%</p>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="PERSEN_PPH" name="PERSEN_PPH" readonly>
												<label for="PERSEN_PPH">Persen PPH</label>
												<p class="help-block">Default: 10%</p>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="TGL_REGISTRASI" name="TGL_REGISTRASI" readonly>
												<label for="TGL_REGISTRASI">Tgl Registrasi</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="ADM_PER_BLN" name="ADM_PER_BLN" readonly>
												<label for="ADM_PER_BLN">Adm Per Bulan</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="PERIODE_ADM" name="PERIODE_ADM" readonly>
												<label for="PERIODE_ADM">Periode Adm</label>
											</div>

									</div>
								</div>

							</div>

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group ">
												<input type="text" class="form-control" id="SALDO_AWAL" name="SALDO_AWAL" readonly>
												<label for="SALDO_AWAL">Saldo Awal</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="SALDO_SETORAN" name="SALDO_SETORAN" readonly>
												<label for="SALDO_SETORAN">Saldo Setoran</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="SALDO_NOMINATIF" name="SALDO_NOMINATIF" readonly>
												<label for="SALDO_NOMINATIF">Saldo Nominatif</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="simpanan_pokok" name="simpanan_pokok" readonly>
												<label for="simpanan_pokok">Simpanan Pokok</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="simpanan_sukarela" name="simpanan_sukarela" readonly>
												<label for="simpanan_sukarela">Simpanan Sukarela</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="simpanan_wajib" name="simpanan_wajib" readonly>
												<label for="simpanan_wajib">Simpanan Wajib</label>
											</div>

											<div class="form-group floating-label">
												<?php 
												$sqlTabTrans = "SELECT TABTRANS_ID FROM tabtrans WHERE 1 ORDER by TABTRANS_ID desc limit 1";
												$fetchTabTrans = mysql_fetch_array(mysql_query($sqlTabTrans));
												$idMax = (int) $fetchTabTrans['TABTRANS_ID'];
												$idMax++;
												$kuitansi = "T-" . sprintf("%06s", $idMax);
												?>
												<input type="text" class="form-control" id="kuitansi" name="kuitansi" value="<?php echo $kuitansi; ?>" readonly>
												<label for="kuitansi">Kuitansi</label>
											</div>

											<div class="form-group ">
												<input type="text" class="form-control" id="keterangan" name="keterangan">
												<label for="keterangan">Keterangan</label>
											</div>

									</div>
								</div>	

							</div>	

						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Submit</button>

					</form>
					
				</div>	
			</section>

			<?php 
			}
			else if ($message == 1)
			{
				$linkBack = "penutupan_simpanan.php";
				require_once "layouts/message_success.php";
			}
			else if ($message == 2)
			{
				$linkBack = "penutupan_simpanan.php";
				require_once "layouts/message_error.php";
			}
			?>

		</div>
		
		<?php require_once "layouts/home/menus.php"; ?>

	</div>

<?php require_once "layouts/foot.php"; ?>

<script type="text/javascript">
$(function(){
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#NASABAH_ID').val(ui.item.nasabah_id);
					$('#nama_nasabah').val(ui.item.nama_nasabah);
					$('#SUKU_BUNGA').val(ui.item.SUKU_BUNGA);
					$('#PERSEN_PPH').val(ui.item.PERSEN_PPH);
					$('#TGL_REGISTRASI').val(ui.item.TGL_REGISTRASI);
					$('#ADM_PER_BLN').val(ui.item.ADM_PER_BLN);
					$('#PERIODE_ADM').val(ui.item.PERIODE_ADM);
					$('#PERIODE_ADM').val(ui.item.PERIODE_ADM);

					$('#SALDO_AWAL').val(ui.item.SALDO_AWAL);
					$('#SALDO_SETORAN').val(ui.item.SALDO_SETORAN);
					$('#SALDO_NOMINATIF').val(ui.item.SALDO_NOMINATIF);
					$('#simpanan_pokok').val(ui.item.simpanan_pokok);
					$('#simpanan_sukarela').val(ui.item.simpanan_sukarela);
					$('#simpanan_wajib').val(ui.item.simpanan_wajib);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});
});
</script>

<?php require_once "layouts/footer.php"; ?>