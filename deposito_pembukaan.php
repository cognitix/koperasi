<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "deposito_pembukaan";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$message = 0;

if (isset($_POST['submit']))
{
	$nomor_rekening = $_POST['nomor_rekening'];
	$kuitansi = $_POST['kuitansi'];
	$saldo_trans = $_POST['saldo_trans'];

	$tgl_trans = date('Y-m-d'); //$tglsystem;

	$userId = $user['USERID'];
	$cab = $kodecabang['kode_cab'];

	// query deposito
	$sqlDeposito = "
	SELECT 
	deposito.*,
	nasabah.nama_nasabah ,
	nasabah.alamat ,
	nasabah.PATH_TTANGAN
	FROM deposito 
	JOIN nasabah ON deposito.NASABAH_ID = nasabah.nasabah_id
	WHERE 1
	AND NO_REKENING = '".$nomor_rekening."'
	";
	$fetchDeposito = mysql_fetch_array(mysql_query($sqlDeposito));

	// query deptrans
	$sql = "
	INSERT INTO deptrans SET 
	TGL_TRANS = '".$tgl_trans."' ,
	NO_REKENING = '".$nomor_rekening."' ,
	KODE_TRANS = '01' ,
	SALDO_TRANS = '".$saldo_trans."' ,
	MY_KODE_TRANS = 0 ,
	kuitansi = '".$kuitansi."' ,
	USERID = '".$userId."' ,
	TOB = 'T' ,
	POSTED = 1 ,
	VALIDATED = 1 ,
	KODE_PERK_GL = '10102' ,
	CAB = '".$cab."'
	";

	mysql_query($sql);

	$depTransId = mysql_insert_id();

	$uraian = "Penerimaan Deposito Tunai : #" . $fetchDeposito['NO_REKENING'] . " - " . $fetchDeposito['nama_nasabah'];
	$MY_KODE_TRANS = 200;

	// query teller trans
	$sql = "
	INSERT INTO tellertrans SET 
	modul = 'DEP' ,
	tgl_trans = '".$tgl_trans."' ,
	NO_BUKTI = '".$kuitansi."' ,
	uraian = '".$uraian."' ,
	saldo_trans = '".$saldo_trans."' ,
	userid = '".$userId."' ,
	my_kode_trans = '".$MY_KODE_TRANS."' ,
	TOB = 'T' ,
	cab = '".$cab."' ,
	modul_trans_id = '".$depTransId."'
	";

	mysql_query($sql);

	// query update status aktif di deposito
	$sql = "
	UPDATE deposito SET 
	STATUS_AKTIF = 2
	WHERE 1
	AND NO_REKENING = '".$nomor_rekening."' 
	";

	mysql_query($sql);

	$message = 1;
}
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
				$linkBack = "deposito_pembukaan.php";
				require_once "layouts/message_success.php";
			}
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Deposito Pembukaan</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group">
												<input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" value="" data-source="autosuggest_nomorrekening_deposito.php">
												<label for="nomor_rekening">Nomor Rekening Deposito</label>
											</div>

											<?php 
											$sqlNomorRekening = "SELECT kuitansi FROM deptrans WHERE 1 AND kuitansi != '' ORDER BY DEPTRANS_ID DESC LIMIT 1";
											$fetchNoRekening = mysql_fetch_array(mysql_query($sqlNomorRekening));
											$idMax = (int) str_replace("D-", "", $fetchNoRekening['kuitansi']);
											$idMax++;
											$kuitansi = sprintf("%08s", $idMax);
											$norekening1 = "D-";
											$norekening2 = substr($kuitansi, 3);
											?>

											<div class="form-group">
												<input type="text" class="form-control" id="kuitansi" name="kuitansi" value="<?php echo $norekening1.$norekening2; ?>">
												<label for="kuitansi">Kuitansi</label>
											</div>

											<div class="form-group">
												<input type="text" class="form-control" id="saldo_trans" name="saldo_trans" value="">
												<label for="saldo_trans">Saldo Transaksi</label>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

					</form>

				</div>	
			</section>

			<?php require_once "layouts/home/menus.php"; ?>

		</div>

	</div>
	
<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$.ajax({
		url: $('#nomor_rekening').data('source'),
		dataType: "json",
		success: function (datas) {
			$("#nomor_rekening").autocomplete({
				source: datas,
				select: function( event, ui ) {
					//var label = explode('-', ui.item.value);
					//alert(ui.item.value + " " + ui.item.label);
					//console.log(ui.item);

					$('#nomor_rekening').val(ui.item.NO_REKENING);
					$('#saldo_trans').val(ui.item.JML_DEPOSITO);

					return false;
				},
				focus: function( event, ui ) {

			        $('#nomor_rekening').val(ui.item.NO_REKENING);
			        
			        return false;
			    },
			});
		}
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>		