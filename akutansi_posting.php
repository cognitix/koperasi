<?php 
require_once "connect.php";

require_once "layouts/head.php"; 
require_once "library/Paginator.php";

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];
$menu = "akutansi_posting";

function findTanggalHariIniInMysysid()
{
	$sql = "SELECT * FROM mysysid WHERE 1 AND KeyName = 'TANGGALHARIINI'";
	$fetch = mysql_fetch_array(mysql_query($sql));
	return $fetch['Value'];
}

$tglsystem = findTanggalHariIniInMysysid();
$tglsystem = (!empty($tglsystem)) ? date("Y-m-d", strtotime(str_replace("/", "-", $tglsystem))) : null;

$tgl_transaksi = (!empty($_GET['tgl_transaksi'])) ? $_GET['tgl_transaksi'] : date('Y-m-d');

$message = 0;

if (isset($_POST['submit']))
{
	$tgl1 = $_POST['tgl1'];
	$tgl2 = $_POST['tgl2'];

	$sql = "
	SELECT 
	*
	FROM
	tellertrans
	WHERE 1
	AND tgl_trans BETWEEN '".$tgl1."' AND '".$tgl2."'
	ORDER BY trans_id ASC
	";	

	$query = mysql_query($sql);

	// looping data teller trans ============
	while($fetch = mysql_fetch_array($query))
	{
		//echo '<pre>';print_r($fetch);echo '</pre>';

		$modul = $fetch['modul'];
		$kuitansi = $fetch['NO_BUKTI'];
		$tgl_trans = $fetch['tgl_trans'];
		$kode_jurnal = $fetch['kode_jurnal'];
		$NOMINAL = $fetch['saldo_trans'];
		$uraian = $fetch['uraian'];
		$my_kode_trans = $fetch['my_kode_trans'];
		$GL_TRANS = $fetch['GL_TRANS'];

		// cek data posted ============
		if ($modul == "TAB")
		{
			$sqlModul = "
			SELECT 
			*
			FROM
			tabtrans 
			WHERE 1
			AND POSTED = 0
			AND kuitansi = '".$kuitansi."'
			";

			$queryModul = mysql_query($sqlModul);

			$fetchModul = mysql_fetch_array($queryModul);
		}
		else if ($modul == "KRE")
		{
			$sqlModul = "
			SELECT 
			*
			FROM
			kretrans 
			WHERE 1
			AND POSTED = 0
			AND KUITANSI = '".$kuitansi."'
			";

			$queryModul = mysql_query($sqlModul);

			$fetchModul = mysql_fetch_array($queryModul);
		}
		else if ($modul == "DEP")
		{
			$sqlModul = "
			SELECT 
			*
			FROM
			deptrans 
			WHERE 1
			AND POSTED = 0
			AND kuitansi = '".$kuitansi."'
			";

			$queryModul = mysql_query($sqlModul);

			$fetchModul = mysql_fetch_array($queryModul);
		}
		else if ($modul == "PC")
		{
			$sqlModul = "
			SELECT 
			*
			FROM
			tellertrans 
			WHERE 1
			AND POSTED = 0
			AND NO_BUKTI = '".$kuitansi."'
			";

			$queryModul = mysql_query($sqlModul);

			$fetchModul = mysql_fetch_array($queryModul);
		}

		if ($fetchModul)
		{

			//echo '<pre>';print_r($fetchModul);echo '</pre>';

			
			// insert trans master ============
			$sqlTransMaster = "
			INSERT INTO trans_master SET 
			tgl_trans = '".$tgl_trans."' ,
			kode_jurnal = '".$kode_jurnal."' ,
			NOMINAL = '".$NOMINAL."' ,
			KETERANGAN = '".$uraian."' ,
			no_bukti = '".$kuitansi."'
			";

			$queryTransMaster = mysql_query($sqlTransMaster);

			$master_id = mysql_insert_id();

			// kode perkiraan ============
			$kode_perk = ($kodecabang['kode_cab'] == "001") ? "10102" : ""; 

			if ($kode_perk)
			{
				$sqlPerkiraan = "
				SELECT
				*
				FROM
				perkiraan 
				WHERE 1
				AND kode_perk = '".$kode_perk."'
				";
			}
			else
			{
				$sqlPerkiraan = "
				SELECT
				*
				FROM
				perkiraan 
				WHERE 1
				AND CAB = '".$kodecabang['kode_cab']."'
				";
			}

			$queryPerkiraan = mysql_query($sqlPerkiraan);
			$fetchPerkiraan = mysql_fetch_array($queryPerkiraan);

			
			if ($my_kode_trans == "300")
			{
				$kode_perk = $fetchPerkiraan['kode_perk'];




				// 1. insert trans detail ============
				$debet = 0;
				$kredit = $NOMINAL;
				$saldo_akhir = $fetchPerkiraan['saldo_akhir'] + $debet - $kredit;

				$sqlTransDetail = "
				INSERT INTO trans_detail SET 
				kode_perk = '".$kode_perk."' ,
				master_id = '".$master_id."' ,
				URAIAN = '".$uraian."' ,
				debet = '".$debet."' ,
				kredit = '".$kredit."' ,
				saldo_akhir = '".$saldo_akhir."'
				";

				$queryTransDetail = mysql_query($sqlTransDetail);




				// 2. update perkiraan ============
				$saldo_debet = $fetchPerkiraan['saldo_debet'] + $debet;
				$saldo_kredit = $fetchPerkiraan['saldo_kredit'] + $kredit;
				$saldo_akhir = $saldo_debet - $saldo_kredit;

				$sqlUpdatePerkiraan = "
				UPDATE INTO perkiraan SET 
				saldo_debet = '".$saldo_debet."' ,
				saldo_kredit = '".$saldo_kredit."' ,
				saldo_akhir = '".$saldo_akhir."'
				WHERE 1
				AND kode_perk = '".$kode_perk."'
				";

				$queryUpdatePerkiraan = mysql_query($sqlUpdatePerkiraan);






				// 3. perkiraan gl trans ============
				$sqlPerkiraanGLTrans = "
				SELECT
				*
				FROM
				perkiraan 
				WHERE 1
				AND kode_perk = '".$GL_TRANS."'
				";
				
				$queryPerkiraanGLTrans = mysql_query($sqlPerkiraanGLTrans);

				$fetchPerkiraanGLTrans = mysql_fetch_array($queryPerkiraanGLTrans);




				// 4. insert trans detail ============
				$debet = $NOMINAL;
				$kredit = 0;
				$saldo_akhir = $fetchPerkiraanGLTrans['saldo_akhir'] + $debet - $kredit;

				$sqlTransDetail = "
				INSERT INTO trans_detail SET 
				kode_perk = '".$GL_TRANS."' ,
				master_id = '".$master_id."' ,
				URAIAN = '".$uraian."' ,
				debet = '".$debet."' ,
				kredit = '".$kredit."',
				saldo_akhir = '".$saldo_akhir."'
				";

				$queryTransDetail = mysql_query($sqlTransDetail);







				// 5. update perkiraan ============
				$saldo_debet = $fetchPerkiraanGLTrans['saldo_debet'] + $debet;
				$saldo_kredit = $fetchPerkiraanGLTrans['saldo_kredit'] + $kredit;
				$saldo_akhir = $saldo_debet - $saldo_kredit;

				$sqlPerkiraan = "
				UPDATE INTO perkiraan SET 
				saldo_debet = '".$saldo_debet."' ,
				saldo_kredit = '".$saldo_kredit."' ,
				saldo_akhir = '".$saldo_akhir."'
				WHERE 1
				AND kode_perk = '".$GL_TRANS."'
				";

				$queryPerkiraan = mysql_query($sqlPerkiraan);
			}
			else
			{
				$kode_perk = $fetchPerkiraan['kode_perk'];



				// 1. insert trans detail ============
				$debet = $NOMINAL;
				$kredit = 0;
				$saldo_akhir = $fetchPerkiraan['saldo_akhir'] + $debet - $kredit;

				$sqlTransDetail = "
				INSERT INTO trans_detail SET 
				kode_perk = '".$kode_perk."' ,
				master_id = '".$master_id."' ,
				URAIAN = '".$uraian."' ,
				debet = '".$debet."' ,
				kredit = '".$kredit."' ,
				saldo_akhir = '".$saldo_akhir."'
				";

				$queryTransDetail = mysql_query($sqlTransDetail);




				// 2. update perkiraan ============
				$saldo_debet = $fetchPerkiraan['saldo_debet'] + $debet;
				$saldo_kredit = $fetchPerkiraan['saldo_kredit'] + $kredit;
				$saldo_akhir = $saldo_debet - $saldo_kredit;

				$sqlUpdatePerkiraan = "
				UPDATE INTO perkiraan SET 
				saldo_debet = '".$saldo_debet."' ,
				saldo_kredit = '".$saldo_kredit."' ,
				saldo_akhir = '".$saldo_akhir."'
				WHERE 1
				AND kode_perk = '".$kode_perk."'
				";

				$queryUpdatePerkiraan = mysql_query($sqlUpdatePerkiraan);





				// 3. perkiraan gl trans ============
				$sqlPerkiraanGLTrans = "
				SELECT
				*
				FROM
				perkiraan 
				WHERE 1
				AND kode_perk = '".$GL_TRANS."'
				";
				
				$queryPerkiraanGLTrans = mysql_query($sqlPerkiraanGLTrans);

				$fetchPerkiraanGLTrans = mysql_fetch_array($queryPerkiraanGLTrans);





				// 4. insert trans detail ============
				$debet = 0;
				$kredit = $NOMINAL;
				$saldo_akhir = $fetchPerkiraanGLTrans['saldo_akhir'] + $debet - $kredit;

				$sqlTransDetail = "
				INSERT INTO trans_detail SET 
				kode_perk = '".$GL_TRANS."' ,
				master_id = '".$master_id."' ,
				URAIAN = '".$uraian."' ,
				debet = '".$debet."' ,
				kredit = '".$kredit."',
				saldo_akhir = '".$saldo_akhir."'
				";

				$queryTransDetail = mysql_query($sqlTransDetail);






				// 5. update perkiraan ============
				$saldo_debet = $fetchPerkiraanGLTrans['saldo_debet'] + $debet;
				$saldo_kredit = $fetchPerkiraanGLTrans['saldo_kredit'] + $kredit;
				$saldo_akhir = $saldo_debet - $saldo_kredit;

				$sqlPerkiraan = "
				UPDATE INTO perkiraan SET 
				saldo_debet = '".$saldo_debet."' ,
				saldo_kredit = '".$saldo_kredit."' ,
				saldo_akhir = '".$saldo_akhir."'
				WHERE 1
				AND kode_perk = '".$GL_TRANS."'
				";

				$queryPerkiraan = mysql_query($sqlPerkiraan);
			}




			// cek data posted ============
			if ($modul == "TAB")
			{
				$sqlUpdateModul = "
				UPDATE tabtrans SET 
				POSTED = 1
				WHERE 1
				AND kuitansi = '".$kuitansi."'
				";

				$queryUpdateModul = mysql_query($sqlUpdateModul);
			}
			else if ($modul == "KRE")
			{
				$sqlUpdateModul = "
				UPDATE kretrans SET
				POSTED = 1
				WHERE 1
				AND KUITANSI = '".$kuitansi."'
				";

				$queryUpdateModul = mysql_query($sqlUpdateModul);
			}
			else if ($modul == "DEP")
			{
				$sqlUpdateModul = "
				UPDATE deptrans SET 
				POSTED = 1
				WHERE 1
				AND kuitansi = '".$kuitansi."'
				";

				$queryUpdateModul = mysql_query($sqlUpdateModul);
			}
			else if ($modul == "PC")
			{
				$sqlUpdateModul = "
				UPDATE tellertrans SET 
				POSTED = 1
				WHERE 1
				AND NO_BUKTI = '".$kuitansi."'
				";

				$queryUpdateModul = mysql_query($sqlUpdateModul);
			}
			

		}

		$message = 1;

	}

	//die();
}


?>

<body class="menubar-hoverable header-fixed menubar-pin ">

	<?php require_once "layouts/home/header.php"; ?>

	<!-- BEGIN BASE-->
	<div id="base">

		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">

			<?php
			if ($message == 1)
			{
				$linkBack = "akutansi_posting.php";
				require_once "layouts/message_success.php";
			}
			?>

			<section>
				<div class="section-header">
					<ol class="breadcrumb">
						<li class="active">Posting</li>
					</ol>
				</div>

				<div class="section-body contain-lg">

					<form class="form" method="post" enctype="multipart/form-data">

						<div class="row">

							<div class="col-lg-6">

								<div class="card">
									<div class="card-body">

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl1" name="tgl1" value="" required>
														<label for="tgl1">Tanggal 1</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

											<div class="form-group floating-label">
												<div class="input-group date" id="TGL_REALISASI_container">
													<div class="input-group-content">
														<input type="text" class="form-control" id="tgl2" name="tgl2" value="" required>
														<label for="tgl2">Tanggal 2</label>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>

									</div>
								</div>

							</div>


						</div>

						<button type="submit" class="btn ink-reaction btn-raised btn-primary" name="submit">Proses</button>

					</form>

				</div>	
			</section>

			<?php require_once "layouts/home/menus.php"; ?>

		</div>

	</div>
	
<?php require_once "layouts/foot.php"; ?>

<script src="assets/js/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
	
	$('#tgl1').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

	$('#tgl2').datepicker({
		autoclose: true, 
		todayHighlight: true,
		format: "yyyy-mm-dd"
	});

});
</script>

<?php require_once "layouts/footer.php"; ?>		