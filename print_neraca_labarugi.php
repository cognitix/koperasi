<?php 
require_once "connect.php";

require_once "layouts/head.php"; 

if (empty($_SESSION['user'])) header('location:login.php');

$user = $_SESSION['user'];
$kodecabang = $_SESSION['kodecabang'];

$tgl1 = $_GET['tgl1'];
$tgl2 = $_GET['tgl2'];

$tgl_transaksi_from_string = date("d M Y", strtotime($tgl1));
$tgl_transaksi_to_string = date("d M Y", strtotime($tgl2));
?>

<body class="menubar-hoverable header-fixed menubar-pin ">

<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}    

@media all {
    .page-break { display: none; }
}

@media print {
    .page-break { display: block; page-break-before: always; }
}    

.body-print{
	font-size:11px;
	line-height: 1.2;
	width:800px;
}
.table-custom tbody tr td
{
	border:none;
	padding:4px;
	line-height: 1.5;
}
.table-custom2 tbody tr td
{
	border:none;
	padding:0 2px;
	line-height: 1.5;
}
.table-custom thead tr th{
	text-align: center;
}
.table-custom thead tr th,
.table-custom tbody tr.total td
{
	padding:4px;
	line-height: 1.5;
	border:1px solid #000;
	border-top:1px solid #000 !important;
}
.table-custom tr th{
	text-align: center;
	border-top:1px solid #000 !important;
	border-bottom:1px solid #000 !important;
	padding:2px !important;
}
.table-custom-border tr td{
	line-height: 1.5;
	padding:0px 18px !important;
}
.table-custom-border tr td.tanda-tangan{
	padding:10px !important;
}
.header-text h5, .header-text h3, .header-text h4{
    margin-top:0;
    margin-bottom:0;
}
</style>

<div class="card body-print">
	<div class="card-body">

		<div class="header-text">
			<div class="pull-left">
				<h5>Koperasi Simpan Pinjam</h5>
                <h4 style="margin-bottom:8px;">KSP ADIL MAKMUR FAJAR</h4>
			</div>
			<div class="pull-right">
				<h4>Trial Balance</h4>
                <h5 style="margin-bottom:8px;">Periode from <?php echo $tgl_transaksi_from_string; ?> to <?php echo $tgl_transaksi_to_string; ?></h5>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover table-bordered">
            	<tr>
                    <th style="text-align:left;">Pendapatan</th>
                    <th style="text-align:left;">Posisi Saldo <br /> Periode from <?php echo $tgl_transaksi_from_string; ?> to <?php echo $tgl_transaksi_to_string; ?></th>
                    <th style="text-align:left;">Posisi Saldo Akumalasi</th>
                </tr>

                		<?php

	                	// ======================== PENDAPATAN

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 2) = '40'
						AND LEFT(perkiraan.kode_perk, 1) = '4'
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$totalPendapatan = $totalPendapatanNow = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$sqlBefore = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								 trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";
							$queryBefore = mysql_query($sqlBefore);

							$saldoAwal = 0;
							while($resultBefore = mysql_fetch_array($queryBefore))
							{
								$debet = $resultBefore['debet'];

								$kredit = $resultBefore['kredit'];

								$saldoAwal += $debet - $kredit;
							}

							$sql = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";

							$query = mysql_query($sql);

							$saldo = $saldoAwal;
							$saldoNow = 0;
							while($result1 = mysql_fetch_array($query))
							{
								$debet = $result1['debet'];

								$kredit = $result1['kredit'];

								$saldoNow += $debet - $kredit;
								$saldo += $debet - $kredit;
							}

							if (! $saldo && $results['level'] >= 3) continue;

							$bold = ($results['level'] <= 2) ? 'font-weight:bold;' : '';

							$totalPendapatan += $saldo;
							$totalPendapatanNow += $saldoNow;
						?>

						<tr>
							<td>
								<?php echo $results['nama_perk']; ?></td>
							<td>
								<?php
									if ($saldoNow < 0)
									{
										echo "( ".number_format(abs($saldoNow),2,'.',',')." )";
									}
									else
									{
										echo number_format($saldoNow,2,'.',','); 
									}
								?>
							</td>
							<td>
								<?php
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo number_format($saldo,2,'.',','); 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Pendapatan</td>
							<td>
								<?php 
									if ($totalPendapatanNow < 0)
									{
										echo "( ".number_format(abs($totalPendapatanNow),2,'.',',')." )";
									}
									else
									{
										echo ((int)$totalPendapatanNow != 0) ? number_format($totalPendapatanNow,2,'.',',') : ''; 
									}
								?>
							</td>
							<td>
								<?php 
									if ($totalPendapatan < 0)
									{
										echo "( ".number_format(abs($totalPendapatan),2,'.',',')." )";
									}
									else
									{
										echo ((int)$totalPendapatan != 0) ? number_format($totalPendapatan,2,'.',',') : ''; 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== Pendapatan : END
						?>

            </table>
        </div>

        <div class="table-responsive">
            <table id="datatable1" class="table table-striped table-hover table-bordered">
            	<tr>
                    <th style="text-align:left;">Biaya</th>
                    <th style="text-align:left;">Posisi Saldo <br /> Periode from <?php echo $tgl_transaksi_from_string; ?> to <?php echo $tgl_transaksi_to_string; ?></th>
                    <th style="text-align:left;">Posisi Saldo Akumalasi</th>
                </tr>

                		<?php

	                	// ======================== Biaya

						$sqlPerkiraan = "
						SELECT 
						perkiraan.*
						FROM perkiraan 
						WHERE 1 
						AND LEFT(perkiraan.kode_perk, 2) = '50'
						AND LEFT(perkiraan.kode_perk, 1) = '5'
						ORDER BY perkiraan.kode_perk ASC
						";

						$queryPerkiraan = mysql_query($sqlPerkiraan);

						$totalBiaya = $totalBiayaNow = 0;
						$c = 0;

						while($results = mysql_fetch_array($queryPerkiraan))
						{
							if (! $results['kode_perk']) continue;

							$sqlBefore = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								 trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans < '".date('Y-m-d', strtotime($tgl1))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";
							$queryBefore = mysql_query($sqlBefore);

							$saldoAwal = 0;
							while($resultBefore = mysql_fetch_array($queryBefore))
							{
								$debet = $resultBefore['debet'];

								$kredit = $resultBefore['kredit'];

								$saldoAwal += $debet - $kredit;
							}

							$sql = "
							SELECT 
							*
							FROM trans_detail
							JOIN trans_master ON 
								trans_detail.master_id = trans_master.trans_id
							WHERE 1
							AND trans_master.tgl_trans BETWEEN '".date('Y-m-d', strtotime($tgl1))."' AND '".date('Y-m-d', strtotime($tgl2))."'
							AND trans_detail.kode_perk = '".$results['kode_perk']."'
							ORDER BY trans_master.tgl_trans DESC
							";

							$query = mysql_query($sql);

							$saldo = $saldoAwal;
							$saldoNow = 0;
							while($result1 = mysql_fetch_array($query))
							{
								$debet = $result1['debet'];

								$kredit = $result1['kredit'];

								$saldoNow += $debet - $kredit;
								$saldo += $debet - $kredit;
							}

							if (! $saldo && $results['level'] >= 3) continue;

							$bold = ($results['level'] <= 2) ? 'font-weight:bold;' : '';

							$totalBiaya += $saldo;
							$totalBiayaNow += $saldoNow;
						?>

						<tr>
							<td>
								<?php echo $results['nama_perk']; ?></td>
							<td>
								<?php
									if ($saldoNow < 0)
									{
										echo "( ".number_format(abs($saldoNow),2,'.',',')." )";
									}
									else
									{
										echo number_format($saldoNow,2,'.',','); 
									}
								?>
							</td>
							<td>
								<?php
									if ($saldo < 0)
									{
										echo "( ".number_format(abs($saldo),2,'.',',')." )";
									}
									else
									{
										echo number_format($saldo,2,'.',','); 
									}
								?>
							</td>
						</tr>

						<?php
							$c++;
						}

	                	?>

	                	<tr>
							<td style="font-weight:bold;">Total Biaya</td>
							<td>
								<?php 
									if ($totalBiayaNow < 0)
									{
										echo "( ".number_format(abs($totalBiayaNow),2,'.',',')." )";
									}
									else
									{
										echo number_format($totalBiayaNow,2,'.',','); 
									}
								?>
							</td>
							<td>
								<?php 
									if ($totalBiaya < 0)
									{
										echo "( ".number_format(abs($totalBiaya),2,'.',',')." )";
									}
									else
									{
										echo number_format($totalBiaya,2,'.',','); 
									}
								?>
							</td>
						</tr>

						<?php 
						// ======================== Biaya : END
						?>



						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="font-weight:bold;">Laba Rugi</td>
							<td>
								<?php 

									$total1 = ($totalPendapatanNow + $totalBiayaNow);

									if ($total1 < 0)
									{
										echo "( ".number_format(abs($total1),2,'.',',')." )";
									}
									else
									{
										echo number_format($total1,2,'.',','); 
									}
								?>
							</td>
							<td>
								<?php 
									//echo $totalPendapatan .' + '. $totalBiaya .' ==== ';
									$total2 = ($totalPendapatan + $totalBiaya);

									if ($total2 < 0)
									{
										echo "( ".number_format(abs($total2),2,'.',',')." )";
									}
									else
									{
										echo number_format($total2,2,'.',','); 
									}
								?>
							</td>
						</tr>
						<tr>
							<td style="font-weight:bold;">Taksiran Pajak Penghasilan</td>
							<td>
								
							</td>
							<td>
								<?php 
									//echo $totalPendapatan .' + '. $totalBiaya .' ==== ';
									$totalPajak = 0;

									if ($totalPajak < 0)
									{
										echo "( ".number_format(abs($totalPajak),2,'.',',')." )";
									}
									else
									{
										echo number_format($totalPajak,2,'.',','); 
									}
								?>
							</td>
						</tr>
						<tr>
							<td style="font-weight:bold;">Laba Rugi Setelah Pajak</td>
							<td>
								
							</td>
							<td>
								<?php 
									//echo $totalPendapatan .' + '. $totalBiaya .' ==== ';
									$labaRugiSetelahPajak = $total2 + $totalPajak;

									if ($labaRugiSetelahPajak < 0)
									{
										echo "( ".number_format(abs($labaRugiSetelahPajak),2,'.',',')." )";
									}
									else
									{
										echo number_format($labaRugiSetelahPajak,2,'.',','); 
									}
								?>
							</td>
						</tr>

            </table>
        </div>

        <div class="clearfix"></div>

		<div style="margin:0 auto;">
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Menyetujui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:98px;text-align:center;">Agustina</div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    KBO
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;margin-right:50px;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">Mengetahui</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:98px;text-align:center;">Lily Njomin</div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Pengawas
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="pull-left" style="width:25%;">
				<table id="datatable1" class="table table-custom">
					<tbody>
						<tr>
							<td>
							</td>
						</tr>
						<tr>
							<td>
								<div style="text-align:center;">
									<b>Jakarta Utara, <?php echo date("d M Y"); ?></b><br />
									KSP ADIL MAKMUR FAJAR<br />
									Dibuat
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div style="margin-top:80px;border-top:2px dashed #000;text-align:center;"></div>
                                <div style="margin-top:3px;border-top:2px solid #000;text-align:center;">
                                    Staff Akutansi
                                </div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>

	</div>
</div>	

<p><input type="button" value="Print" class="no-print" onClick="self.print()"/></p>

<?php require_once "layouts/foot.php"; ?>

<?php require_once "layouts/footer.php"; ?>